/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var moment = require('moment');
var _ = require('underscore');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
// import my Interfaces
var exchange_1 = require("../models/exchange");
var market_1 = require("../models/market");
var ticker_1 = require("../models/ticker");
var balance_1 = require("../models/balance");
var account_1 = require("../models/account");
var orderBook_1 = require("../models/orderBook");
var order_1 = require("../models/order");
var historicalAccountTrade_1 = require("../models/historicalAccountTrade");
// My packages
var BTCChina = require("btc-china"), logger = require('config-logger'), VError = require('verror'), config = require('config');
var defaultExchangeSettings = {
    name: 'BTCChina',
    fixedCurrencies: ['BTC', 'LTC'],
    variableCurrencies: ['CNY'],
    commissions: 0,
    currencyRounding: {
        BTC: 4,
        LTC: 3,
        CNY: 2 },
    priceRounding: {
        BTCCNY: 2,
        LTCCNY: 2,
        LTCBTC: 4
    },
    defaultMinAmount: 0.001,
    publicPollingInterval: 3000,
    privatePollingInterval: 3000,
    maxFailures: 10,
    feeStructure: 'buyCurrency' // all fees are in the fixed currency. eg sell BTC will have a BTC fee
};
var BTCChinaExchange = (function (_super) {
    __extends(BTCChinaExchange, _super);
    function BTCChinaExchange(exchangeConfig) {
        var functionName = 'BTCChina.constructor()';
        // override the default exchange settings from the config
        var exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);
        //Call the constructor of inherited Exchange object
        _super.call(this, exchangeSettings);
        this.getTransactionLimit = 1000;
        logger.trace('%s initialising BTC China client', functionName);
        this.client = new BTCChina(exchangeConfig.APIkey, exchangeConfig.APIsecret);
    }
    BTCChinaExchange.prototype.getCurrencyMovementsBetweenDates = function (callback, fromDate, toDate, currency, type) {
        var error = new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    };
    BTCChinaExchange.prototype.getOrder = function (exchangeId, callback) {
        var functionName = 'BTCChinaExchange.getOrder()', self = this;
        // TODO remove hard coding of symbol
        var symbol = 'BTCCNY';
        logger.trace('%s about to get order for market trade with id %s', functionName, exchangeId);
        self.client.getOrder(function getOrderDetail(err, json) {
            if (err) {
                if (err.name == 'Order not found') {
                    var error = VError('%s could not find order for id %s in the %s market', functionName, exchangeId, symbol);
                    error.name = exchange_1.default.ID_NOT_FOUND;
                    logger.error(error.stack);
                    return callback(error);
                }
                return self.errorHandler(err, true, 'getOrder', self.getOrder, callback, exchangeId);
            }
            self.failures = 0;
            logger.debug('%s successfully got order with id %s and symbol %s', functionName, exchangeId, symbol);
            logger.debug('%s json data: %s', functionName, JSON.stringify(json));
            var order = json.result.order;
            //convert from BTCChina type
            var side = convertSide(order.type);
            var amountTraded = new BigNumber(order.amount_original).
                minus(order.amount).
                toString();
            var priceBN = BTCChinaExchange.getPriceBN(order);
            var amountLastPartial = '0';
            var numberPartialFills = 0;
            if (order.details && order.details.length > 0) {
                numberPartialFills = order.details.length;
                var lastTrade = _.last(order.details);
                amountLastPartial = lastTrade.amount.toString();
                logger.debug("%s order with id %s has been partially filled by %s trades. Last trade had price %s, amount %s at %s", functionName, order.id, order.details.length, lastTrade.price, lastTrade.amount, moment(new Date(lastTrade.dateline + '000')).format('D MMM YY H:mm:ss'));
            }
            var state = 'unknown';
            if (order.status === 'closed')
                state = 'filled';
            else if (order.status === 'open' || order.status === 'pending')
                state = 'pending';
            else if (order.status === 'cancelled')
                state = 'cancelled';
            else if (order.status === 'error')
                state = 'error';
            else {
                var error = new VError('%s could not convert %s order status %s for id %s', functionName, self.name, order.status, exchangeId);
                logger.error(error.stack);
                return callback(error, null);
            }
            var exchangeOrder = new order_1.default({
                exchangeId: order.id.toString(),
                exchangeName: self.name,
                symbol: symbol,
                state: state,
                side: side,
                amount: order.amount_original.toString(),
                amountTraded: amountTraded,
                amountRemaining: order.amount.toString(),
                amountLastPartial: amountLastPartial,
                numberPartialFills: numberPartialFills,
                price: priceBN,
                timestamp: new Date(order.date * 1000)
            });
            logger.info('%s successfully got order:', functionName);
            exchangeOrder.log('info');
            callback(null, exchangeOrder);
            // id, market, trade details included
        }, parseInt(exchangeId), symbol, true);
    };
    BTCChinaExchange.getPriceBN = function (order) {
        if (order.avg_price) {
            return new BigNumber(order.avg_price);
        }
        else {
            return new BigNumber(order.price);
        }
    };
    // override abstract function with implementation for this exchange
    BTCChinaExchange.prototype.getTicker = function (symbol, callback) {
        var functionName = 'BTCChina.getTicker()', self = this;
        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);
        this.client.getTicker(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);
            self.failures = 0;
            var newTicker = new ticker_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(json.ticker.buy),
                ask: Number(json.ticker.sell),
                last: Number(json.ticker.last),
                timestamp: new Date()
            });
            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName, newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last, moment(newTicker.timestamp).format('D MMM YY H:mm:ss'));
            self.markets[symbol].addTicker(newTicker);
            callback(null, newTicker);
        }, symbol);
    };
    ;
    // override abstract function with implementation for this exchange
    BTCChinaExchange.prototype.getOrderBook = function (symbol, callback) {
        var functionName = 'BTCChina.getOrderBook()', self = this;
        logger.trace('%s about to get order book for market %s', functionName, symbol);
        this.client.getOrderBook(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);
            self.failures = 0;
            logger.trace('%s data: %s', functionName, JSON.stringify(json));
            //Convert bid and ask formats to a 2 dimentional array
            var bids = [], asks = [];
            json.bids.forEach(function (order) {
                bids.push([Number(order[0]), Number(order[1])]); //cast to Number as coming from JSON string
            });
            json.asks.forEach(function (order) {
                asks.unshift([Number(order[0]), Number(order[1])]); //cast to Number as coming from JSON string
            });
            var newOrderBook = new orderBook_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(Number(json.date) * 1000),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });
            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0) {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName, self.name, symbol, newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length, newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length, moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss'));
            }
            else {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks', functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }
            self.markets[symbol].addOrderBook(newOrderBook);
            callback(null, newOrderBook);
        }, symbol);
    };
    // Implementation of abstract function to get account balances on the exchange
    BTCChinaExchange.prototype.getAccountBalances = function (callback) {
        var functionName = 'BTCChina.getAccountBalances()', self = this;
        logger.trace('%s about to get account balances', functionName);
        this.client.getAccountInfo(function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);
            self.failures = 0;
            logger.trace('%s accounts returned from the %s exchange:', functionName, self.name);
            logger.trace(data);
            var returnedAccount = new account_1.default([], self.name, self.currencyRounding);
            var currencies = _.keys(data.result.balance);
            currencies.forEach(function (currency) {
                var totalBN = new BigNumber(data.result.balance[currency].amount);
                var availableBN = totalBN.
                    minus(data.result.frozen[currency].amount);
                logger.debug('%s %s balances: total %s, available %s = balance amount %s - frozen amount %s', functionName, currency, totalBN.toString(), availableBN.toString(), data.result.balance[currency].amount, data.result.frozen[currency].amount);
                returnedAccount.setBalance(new balance_1.default({
                    exchangeName: self.name,
                    currency: currency.toUpperCase(),
                    totalBalance: totalBN.toString(),
                    availableBalance: availableBN.toString()
                }));
            });
            // set the balances on the exchange
            self.account = returnedAccount;
            callback(null, returnedAccount);
        });
    };
    ;
    // gets my pending orders
    BTCChinaExchange.prototype.getPendingOrdersForSymbol = function (symbol, callback) {
        var functionName = 'BTCChina.getPendingOrdersForSymbol()', self = this;
        logger.trace('%s about to get pending orders from the %s exchange', functionName, this.name);
        var fixedCurrency = market_1.default.getFixedCurrency(symbol);
        self.client.getOrders(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getPendingOrdersForSymbol', self.getPendingOrdersForSymbol, callback, symbol);
            self.failures = 0;
            logger.debug('%s %s pending orders returned', self.name, json.result.order.length);
            var orders = [];
            json.result.order.forEach(function (order) {
                //convert from BTCChina type
                var side = convertSide(order.type);
                var amountBN = new BigNumber(order.amount_original).
                    round(self.currencyRounding[fixedCurrency], BigNumber.ROUND_DOWN);
                var amountRemainingBN = new BigNumber(order.amount).
                    round(self.currencyRounding[fixedCurrency], BigNumber.ROUND_DOWN);
                var amountTradedBN = amountBN.
                    minus(amountRemainingBN);
                var priceBN = new BigNumber(order.price).
                    round(self.priceRounding[symbol], BigNumber.ROUND_DOWN);
                var amountLastPartial = '0';
                var numberPartialFills = 0;
                if (order.details && order.details.length > 0) {
                    numberPartialFills = order.details.length;
                    var lastTrade = _.last(order.details);
                    amountLastPartial = lastTrade.amount.toString();
                    logger.debug("%s order with id %s has been partially filled by %s trades. Last trade had price %s, amount %s at %s", functionName, order.id, order.details.length, lastTrade.price, lastTrade.amount, moment(lastTrade.dateline).format('D MMM YY H:mm:ss'));
                }
                var exchangeOrder = new order_1.default({
                    exchangeId: order.id.toString(),
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: side,
                    amount: amountBN.toString(),
                    amountTraded: amountTradedBN.toString(),
                    amountRemaining: amountRemainingBN.toString(),
                    amountLastPartial: amountLastPartial,
                    numberPartialFills: numberPartialFills,
                    price: priceBN.toString(),
                    timestamp: new Date(order.date * 1000)
                });
                logger.debug('pending order:');
                exchangeOrder.log('debug');
                orders.push(exchangeOrder);
            });
            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);
            callback(null, orders);
            //openOnly, market, limit, offset, since, withDetail
            // withDetails returns an array of trades filled against the order
        }, true, symbol, 100, 0, 0, true);
    };
    // creates a buy or sell order on an exchange
    BTCChinaExchange.prototype.addOrder = function (newOrder, callback) {
        var functionName = 'BTCChina.addOrder()', self = this;
        logger.trace('%s about to add the following new order:', functionName);
        newOrder.log('trace');
        // validate the type parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy') {
            var error = new VError('%s add trade failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }
        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, newOrder.exchange.name, this.name);
            logger.error(error.stack);
            return callback(error);
        }
        var price = newOrder.price;
        if (newOrder.type === 'market') {
            price = null;
        }
        this.client.createOrder2(function (err, json) {
            var functionName = 'BTCChina.addOrder.createOrder()#' + newOrder.type;
            if (err) {
                var tradeDesc = util_1.format('%s order with price %s, remaining amount %s, sell amount remaining %s %s and tag %s', newOrder.side, newOrder.price, newOrder.amountRemaining, newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.tag);
                // no need to retry the API call with the following errors.
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.addOrderTimeout(err, newOrder, callback);
                }
                else if (err.name === 'Insufficient balance') {
                    var expectedAvailableBalanceBN = self.account.balances[newOrder.sell.currency].availableBalanceBN.
                        minus(newOrder.sellAmountRemainingBN);
                    var error = new VError('%s not enough funds on the %s exchange to add %s. Client %s total balance %s, available balance %s, expected available balance %s', functionName, self.name, tradeDesc, newOrder.sell.currency, self.account.balances[newOrder.sell.currency].totalBalance, self.account.balances[newOrder.sell.currency].availableBalance, expectedAvailableBalanceBN.toString());
                    error.name = exchange_1.default.NOT_ENOUGH_FUNDS;
                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.name === 'Invalid amount') {
                    var error = new VError(err, '%s trade amount remaining %s was too small for the %s exchange to add %s. Configured min amount for the %s market is %s', functionName, newOrder.amountRemaining, self.name, tradeDesc, newOrder.symbol, self.minAmount[newOrder.symbol]);
                    error.name = exchange_1.default.AMOUNT_TOO_SMALL;
                    logger.error(error.stack);
                    return callback(error);
                }
                return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(json));
            var data = {
                exchangeId: json.result,
                timestamp: new Date()
            };
            self.addOrderSuccess(newOrder, callback, data);
        }, newOrder.side, price, newOrder.amountRemaining, newOrder.symbol);
    };
    // cancels an pending order
    BTCChinaExchange.prototype.cancelOrder = function (cancelOrder, callback) {
        var functionName = 'BTCChina.cancelOrder()', self = this;
        logger.trace('%s about to cancel order with exchange id %s', functionName, cancelOrder.exchangeId);
        if (cancelOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            throw error;
        }
        this.client.cancelOrder(function (err, json) {
            if (err) {
                var errorTrade = util_1.format('%s order with id %s, tag %s, price %s and amount remaining %s', cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining);
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                else if (err.name === 'Order already cancelled') {
                    var error = new VError(err, '%s could not cancel %s as it has already been cancelled. Error message: %s', functionName, errorTrade, err.message);
                    error.name = exchange_1.default.ALREADY_CANCELLED;
                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.name === 'Order already completed') {
                    var error = new VError(err, '%s could not cancel %s as it has already been filled', functionName, errorTrade);
                    error.name = exchange_1.default.ALREADY_FILLED;
                    logger.error(error.stack);
                    return callback(error);
                }
                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }
            logger.trace(json);
            self.cancelOrderSuccess(cancelOrder, callback);
        }, parseInt(cancelOrder.exchangeId));
    };
    BTCChinaExchange.prototype.getMarketTrades = function (symbol, params, callback) {
        var functionName = 'BTCChina.getMarketTrades()', self = this;
        if (!params)
            params = {};
        logger.trace('%s about to get historical trades for the %s market', functionName, symbol);
        var trades = [];
        self.client.getTrades(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getTrades', self.getMarketTrades, callback, symbol, params);
            self.failures = 0;
            json.forEach(function (trade) {
                trades.push({
                    exchangeName: self.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: new Date(trade.date * 1000)
                });
            });
            logger.debug('%s %s historical trades returned', functionName, trades.length);
            callback(null, trades);
        }, symbol, params);
    };
    ;
    BTCChinaExchange.prototype.getAccountTradesBetweenDates = function (callback, fromDate, toDate) {
        if (toDate === void 0) { toDate = new Date(); }
        var functionName = 'BTCChina.getAccountTradesBetweenDates()', self = this;
        // TODO need to get both LTCBNY and BTCCNY with an async.parallel
        var symbol = 'BTCCNY';
        var since = moment(fromDate).unix();
        logger.trace('%s about to get account trades from %s (%s) to %s', functionName, moment(fromDate).format('DD MMM YYYY HH:mm:ss'), since, moment(toDate).format('DD MMM YYYY HH:mm:ss'));
        self.client.getOrders(function (err, json) {
            if (err)
                return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);
            self.failures = 0;
            var trades = [];
            json.result.order.forEach(function (order) {
                if (!order.details) {
                    return;
                }
                // is the returned trade type a key in the typeMap
                order.details.forEach(function (trade) {
                    var tradeDate = new Date(trade.dateline * 1000);
                    if (fromDate <= tradeDate &&
                        tradeDate <= toDate) {
                        var historicalAccountTrade = new historicalAccountTrade_1.default({
                            exchangeName: self.name,
                            symbol: symbol,
                            side: convertSide(order.type),
                            priceBN: new BigNumber(trade.price),
                            quantityBN: new BigNumber(trade.amount),
                            timestamp: tradeDate,
                            orderId: order.id,
                            feeAmountBN: BigNumber(0),
                            feeCurrency: 'AUD' // it doesn't matter what the currency is as there is no fee
                        });
                        trades.push(historicalAccountTrade);
                    }
                });
            });
            logger.debug('%s %s my trades returned', functionName, trades.length);
            callback(null, trades);
        }, false, symbol, this.getTransactionLimit, 0, since, true);
        // openOnly, market, limit, offset, since, withDetail
    };
    BTCChinaExchange.prototype.setTimeout = function (timeout) {
        this.client.timeout = timeout;
    };
    return BTCChinaExchange;
})(exchange_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BTCChinaExchange;
function convertSide(side) {
    var functionName = 'BTCChina.convertSide()';
    var convertedSide;
    if (side === 'bid') {
        convertedSide = 'buy';
    }
    else if (side === 'ask') {
        convertedSide = 'sell';
    }
    else if (side === 'buy') {
        convertedSide = 'bid';
    }
    else if (side === 'sell') {
        convertedSide = 'ask';
    }
    else {
        var error = new VError('%s side %s is not buy, sell, bid or ask', functionName, side);
        logger.error(error.stack);
        throw error;
    }
    logger.trace('%s side %s convert to %s', functionName, side, convertedSide);
    return convertedSide;
}
