/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var moment = require('moment');
var _ = require('underscore');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
// import my Interfaces
var exchange_1 = require("../models/exchange");
var market_1 = require("../models/market");
var ticker_1 = require("../models/ticker");
var balance_1 = require("../models/balance");
var account_1 = require("../models/account");
var orderBook_1 = require("../models/orderBook");
var order_1 = require("../models/order");
var historicalAccountTrade_1 = require("../models/historicalAccountTrade");
// My packages
var btce = require("btc-e"), logger = require('config-logger'), VError = require('verror'), config = require('config');
var defaultExchangeSettings = {
    name: 'BTCe',
    fixedCurrencies: ['BTC', 'LTC', 'PPC', 'NMC', 'NVC', 'TRC', 'FTC', 'XPM', 'NMC', 'NVC', 'USD', 'TRC', 'PPC', 'FTC', 'XPM'],
    variableCurrencies: ['USD', 'BTC', 'LTC', 'RUR', 'EUR'],
    commissions: 0.002,
    feeStructure: "buyCurrency",
    currencyRounding: {
        BTC: 8,
        LTC: 8,
        USD: 8
    },
    priceRounding: {
        BTCUSD: 3,
        BTCRUR: 5,
        BTCEUR: 5,
        BTCGBP: 5,
        BTCCNH: 2,
        LTCBTC: 5,
        LTCUSD: 6,
        LTCRUR: 5,
        LTCEUR: 3,
        LTCGBP: 3,
        LTCCNH: 2
    },
    defaultPriceRounding: 2,
    minAmount: {
        BTCUSD: 0.01002,
        BTCRUR: 0.01002,
        BTCEUR: 0.01002,
        BTCGBP: 0.01002,
        BTCCNH: 0.01002,
        LTCBTC: 0.10020,
        LTCUSD: 0.10020,
        LTCRUR: 0.10020,
        LTCEUR: 0.10020,
        LTCGBP: 0.10020,
        LTCCNH: 0.10020
    },
    defaultMinAmount: 0.01,
    publicPollingInterval: 5100,
    privatePollingInterval: 1100,
    maxFailures: 10
};
var BTCe = (function (_super) {
    __extends(BTCe, _super);
    function BTCe(exchangeConfig) {
        var functionName = 'BTCe.constructor()';
        // override the default exchange settings from the config
        var exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);
        //Call the constructor of inherited Exchange object
        _super.call(this, exchangeSettings);
        this.cancelTradeTimeouts = 0;
        this.getTransactionLimit = 10000;
        logger.trace('%s initialising BTCe clients', functionName);
        this.btcePublic = new btce();
        this.btceTrade = new btce(exchangeConfig.APIkey, exchangeConfig.APIsecret, BTCe.nonceGenerator);
        logger.debug('%s nonce = %d, nonce function returns %s', functionName, BTCe.nonce, BTCe.nonceGenerator());
    }
    // function to get new nonce by incrementing it by 1
    BTCe.nonceGenerator = function () {
        var functionName = 'BTCe.nonceGenerator()';
        // increment nonce before every request
        logger.debug('%s nonce = %d', functionName, BTCe.nonce + 1);
        return ++BTCe.nonce;
    };
    ;
    BTCe.prototype.getOrder = function (exchangeId, callback) {
        var error = new VError('Exchange.getOrder() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    };
    BTCe.prototype.getCurrencyMovementsBetweenDates = function (callback, fromDate, toDate, currency, type) {
        var error = new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    };
    BTCe.prototype.getTicker = function (symbol, callback) {
        var functionName = 'BTCe.getTicker()', self = this;
        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol);
        var pair = fixedCurrency.toLowerCase() + "_" + variableCurrency.toLowerCase();
        self.btcePublic.ticker(pair, function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);
            self.failures = 0;
            logger.trace('%s data returned: %s', functionName, JSON.stringify(data));
            var newTicker = new ticker_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bid: data.ticker.sell,
                ask: data.ticker.buy,
                last: data.ticker.last,
                timestamp: new Date(data.ticker.server_time * 1000)
            });
            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName, newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last, moment(newTicker.timestamp).format('D MMM YY H:mm:ss'));
            self.markets[symbol].addTicker(newTicker);
            callback(null, newTicker);
        });
    };
    BTCe.prototype.getOrderBook = function (symbol, callback) {
        var functionName = 'BTCe.getOrderBook()', self = this;
        logger.trace('%s about to get order book for market %s', functionName, symbol);
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol);
        var pair = fixedCurrency.toLowerCase() + "_" + variableCurrency.toLowerCase();
        logger.trace('%s pair: %s fixed: %s var: %s', functionName, pair, fixedCurrency, variableCurrency);
        self.btcePublic.depth(pair, function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);
            self.failures = 0;
            logger.trace('%s data returned: %s', functionName, JSON.stringify(data));
            logger.debug('%s returned %d bids, %d asks', functionName, data.bids.length, data.asks.length);
            var newOrderBook = new orderBook_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bids: data.bids,
                asks: data.asks,
                timestamp: new Date(),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });
            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0) {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName, self.name, symbol, newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length, newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length, moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss'));
            }
            else {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks', functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }
            self.markets[symbol].addOrderBook(newOrderBook);
            callback(null, newOrderBook);
        });
    };
    BTCe.prototype.getAccountBalances = function (callback) {
        var functionName = 'BTCe.getAccountBalances()', self = this;
        logger.trace('%s about to get account balances', functionName);
        self.btceTrade.getInfo(function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);
            self.failures = 0;
            logger.trace('%s wallets returned:', functionName);
            logger.trace(data.return.funds);
            var returnedAccount = new account_1.default([], self.name, self.currencyRounding);
            Object.keys(data.return.funds).forEach(function (currency) {
                logger.trace('%s balances %s %s total balance %d', functionName, self.name, currency.toUpperCase(), data.return.funds[currency]);
                var upperCurrency = currency.toUpperCase();
                self.account[upperCurrency] = new balance_1.default({
                    exchangeName: self.name,
                    currency: upperCurrency,
                    totalBalance: data.return.funds[currency]
                });
                //totalBalance: json.data.Wallets[currency].value});
                logger.debug('%s %s balances: total %s, available %s', functionName, currency, data.return.funds[currency], data.return.funds[currency]);
                returnedAccount.setBalance(new balance_1.default({
                    exchangeName: self.name,
                    currency: upperCurrency,
                    totalBalance: data.return.funds[currency],
                    availableBalance: data.return.funds[currency]
                }));
                // TODO need to get pending orders to work out totalBalance
            });
            // set the balances on the exchange
            self.account = returnedAccount;
            callback(null, returnedAccount);
        });
    };
    ;
    BTCe.prototype.getPendingOrders = function (callback) {
        var functionName = 'BTCe.getPendingOrders()', self = this;
        logger.trace('%s about to get pending orders', functionName);
        var orders = [];
        self.btceTrade.activeOrders(function (err, data) {
            if (err) {
                if (err.message === 'no orders') {
                    logger.debug('%s no orders in the order book so returning an empty array of orders', functionName);
                    return callback(null, orders);
                }
                else {
                    return self.errorHandler(err, true, 'getPendingOrders', self.getPendingOrders, callback);
                }
            }
            self.failures = 0;
            logger.debug('%s pending orders returned from the exchange:', functionName);
            logger.debug(data);
            Object.keys(data).forEach(function (orderId) {
                var fixedCurrency = data[orderId].pair.slice(0, 3).toUpperCase(), variableCurrency = data[orderId].pair.slice(4).toUpperCase(), symbol = fixedCurrency + variableCurrency;
                logger.debug('%s %s %s %d @ %s %s id:%s status %s', functionName, data[orderId].side, data[orderId].pair, data[orderId].amount, data[orderId].rate, moment(data[orderId].timestamp_created).format('D:MM:YY hh:mm:ss'), orderId, data[orderId].status);
                var exchangeOrder = new order_1.default({
                    exchangeId: orderId,
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: data[orderId].side,
                    amount: data[orderId].amount,
                    amountTraded: '0',
                    amountRemaining: data[orderId].amount,
                    price: data[orderId].rate,
                    timestamp: new Date(data[orderId].timestamp_created * 1000)
                });
                logger.debug('%s pending order:', functionName);
                exchangeOrder.log('debug');
                orders.push(exchangeOrder);
            });
            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);
            callback(null, orders);
        });
    };
    ;
    BTCe.prototype.addOrder = function (newOrder, callback) {
        var functionName = 'BTCe.addOrder()', self = this;
        logger.trace('%s about to add the following new order:', functionName);
        newOrder.log('trace');
        // validate the type parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy') {
            var error = new VError('%s trade failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }
        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, newOrder.exchangeName, this.name);
            logger.error(error.stack);
            return callback(error);
        }
        var pair = newOrder.fixedCurrency.toLowerCase() +
            "_" +
            newOrder.variableCurrency.toLowerCase();
        logger.trace('%s pair = %s, type %s, price %s and amount remaining %s', functionName, pair, newOrder.side, newOrder.price, newOrder.amountRemaining);
        self.btceTrade.trade(pair, newOrder.side, newOrder.price, newOrder.amountRemaining, function (err, data) {
            if (err) {
                var tradeDesc = util_1.format('%s order with price %s, remaining amount %s, sell amount remaining %s %s and tag %s', newOrder.side, newOrder.price, newOrder.amountRemaining, newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.tag);
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.addOrderTimeout(err, newOrder, callback);
                }
                else if (err.message === 'It is not enough BTC in the account for sale.' ||
                    err.message === 'It is not enough LTC in the account for sale.' ||
                    err.message === 'It is not enough USD for purchase') {
                    var expectedAvailableBalanceBN = self.account.balances[newOrder.sell.currency].availableBalanceBN.
                        minus(newOrder.sellAmountRemainingBN);
                    var error = new VError('%s not enough funds on the %s exchange to add %s. Client %s total balance %s, available balance %s, expected available balance %s', functionName, self.name, tradeDesc, newOrder.sell.currency, self.account.balances[newOrder.sell.currency].totalBalance, self.account.balances[newOrder.sell.currency].availableBalance, expectedAvailableBalanceBN.toString());
                    error.name = exchange_1.default.NOT_ENOUGH_FUNDS;
                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.message === 'Value BTC must be greater than 0.01 BTC.' ||
                    err.message === 'Value LTC must be greater than 0.01 LTC.') {
                    var error = new VError('%s trade amount remaining %s was too small for the %s exchange to add %s. Configured min amount for the %s market is %s', functionName, newOrder.amountRemaining, self.name, tradeDesc, newOrder.symbol, self.minAmount[newOrder.symbol]);
                    error.name = exchange_1.default.AMOUNT_TOO_SMALL;
                    logger.error(error.stack);
                    return callback(error);
                }
                else {
                    return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
                }
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(data));
            var addTradeSuccessData = {
                exchangeId: data.order_id,
                timestamp: new Date(data.timestamp * 1000)
            };
            self.addOrderSuccess(newOrder, callback, addTradeSuccessData);
        });
    };
    ;
    BTCe.prototype.cancelOrder = function (cancelOrder, callback) {
        var functionName = 'BTCe.cancelOrder()', self = this;
        logger.trace('%s about to cancel order with exchange id %s', functionName, cancelOrder.exchangeId);
        if (cancelOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            return callback(error);
        }
        self.btceTrade.cancelOrder(cancelOrder.exchangeId, function (err, data) {
            if (err) {
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                else if (err.message === "bad status") {
                    return self.cancelOrderInvalidState(err, cancelOrder, callback);
                }
                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(data));
            self.cancelOrderSuccess(cancelOrder, callback);
        });
    };
    BTCe.prototype.getMarketTrades = function (symbol, numberOfTrades, callback) {
        var error = new VError('Exchange.getMarketTrades() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    };
    BTCe.prototype.getAccountTradesBetweenDates = function (callback, fromDate, toDate) {
        if (toDate === void 0) { toDate = new Date(); }
        var functionName = 'BTCe.getAccountTradesBetweenDates()', self = this;
        logger.trace('%s about to my historical trades between %s (%s) and %s (%s)', functionName, fromDate.toDateString(), fromDate.getTime() / 1000, toDate.toString(), toDate.getTime() / 1000);
        var options = {
            //since: fromDate.getTime() / 1000,
            end: toDate.getTime() / 1000
        };
        self.btceTrade.tradeHistory(options, function (err, data) {
            if (err) {
                if (err.message === "no trades") {
                    logger.warn('%s did not return any trades between %s (%s) and %s (%s)', functionName, fromDate.toDateString(), fromDate.getTime() / 1000, toDate.toString(), toDate.getTime() / 1000);
                    return callback(null, []);
                }
                return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(data));
            var trades = [];
            _.forEach(_.keys(data.return), function (tradeId) {
                var trade = data.return[tradeId], tradeDate = new Date(trade.timestamp * 1000);
                if (fromDate <= tradeDate &&
                    tradeDate <= toDate) {
                    var symbol = BTCe.convertPairToSymbol(trade.pair);
                    var feeAmountBN, feeCurrency;
                    if (trade.side == 'buy') {
                        feeCurrency = market_1.default.getFixedCurrency(symbol);
                        feeAmountBN = new BigNumber(trade.amount).
                            times(self.commissions[feeCurrency].maker).
                            round(self.currencyRounding[feeCurrency]);
                    }
                    else {
                        feeCurrency = market_1.default.getVariableCurrency(symbol);
                        feeAmountBN = new BigNumber(trade.amount).
                            times(trade.amount).
                            times(self.commissions[feeCurrency].maker).
                            round(self.currencyRounding[feeCurrency]);
                    }
                    var historicalAccountTrade = new historicalAccountTrade_1.default({
                        exchangeName: self.name,
                        symbol: symbol,
                        side: trade.type,
                        priceBN: new BigNumber(trade.rate),
                        quantityBN: new BigNumber(trade.amount),
                        timestamp: tradeDate,
                        orderId: trade.order_id,
                        tradeId: tradeId,
                        feeAmountBN: feeAmountBN,
                        feeCurrency: feeCurrency
                    });
                    trades.push(historicalAccountTrade);
                }
            });
            callback(null, trades);
        });
    };
    BTCe.prototype.setTimeout = function (timeout) {
        this.btcePublic.timeout = timeout;
        this.btceTrade.timeout = timeout;
    };
    /**
     * converts the exchange pair to a symbol. eg btc_usd to BTCUSD
     * @param pair
     * @returns {string}
     */
    BTCe.convertPairToSymbol = function (pair) {
        return pair.slice(0, 3).toUpperCase() +
            pair.slice(4).toUpperCase();
    };
    // set nonce to current unix date time in seconds
    BTCe.nonce = Math.round((new Date()).getTime() / 1000);
    return BTCe;
})(exchange_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BTCe;
