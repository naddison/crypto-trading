/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

// load third party packages
import * as _ from 'underscore';

import {default as Exchange, IExchangeSettings, OrderCallback, OrdersCallback,
    HistoricalTradesCallback,
    IHistoricalMarketTrade, HistoricalMarketTradesCallback}
    from "../models/exchange";
import Ticker from "../models/ticker";
import OrderBook from "../models/orderBook";
import Order from "../models/order";
import Account from "../models/account";
import {default as Movement, MovementType} from "../models/movement";

// My packages
var VError = require('verror');

/**
 * A test implementation of the abstract Exchange class.
 * All abstract methods are implemented to return an error in the callback
 */
export default class TestExchange extends Exchange
{
    constructor(settings: IExchangeSettings)
    {
        settings = _.extend(settings, {
            APIkey: 'testAPPKey',
            APIsecret: 'testAPISecret'
        });

        super(settings);
    }

    initOrderBook(symbol: string, bids: number[][], asks: number[][])
    {
        this.markets[symbol].latestOrderBook = new OrderBook({
            exchangeName: this.name,
            symbol: symbol,
            bids: bids,
            asks: asks
        });
    };

    // get latest ticker data from the exchange for a specified market
    getTicker(symbol: string, callback: (error?: Error, ticker?: Ticker) => void ): void {
        callback(new VError('Exchange.getTicker() on the %s exchange has not been implemented', this.name)); }

    getOrderBook(symbol: string, callback: (error?: Error, orderBook?: OrderBook) => void ): void {
        callback(new VError('Exchange.getOrderBook() on the %s exchange has not been implemented', this.name)); }

    getMarketTrades(symbol: string, numberOfTrades: number, callback: HistoricalMarketTradesCallback ): void {
        callback(new VError('Exchange.getMarketTrades() on the %s exchange has not been implemented', this.name)); }

    getAccountTradesBetweenDates(callback: HistoricalTradesCallback, fromDate: Date, toDate: Date = new Date() ): void {
        callback(new VError('Exchange.getAccountTradesBetweenDates() on the %s exchange has not been implemented', this.name)); }

    getPendingOrders(callback: OrdersCallback): void {
        callback(new VError('Exchange.getPendingOrders() on the %s exchange has not been implemented', this.name)); }

    getOrderState(exchangeId: string, callback: (error?: Error, state?: string) => void): void {
        callback(new VError('Exchange.getOrderState() on the %s exchange has not been implemented', this.name)); }

    getOrder(exchangeId: string, callback: OrderCallback): void {
        callback(new VError('Exchange.getOrder() on the %s exchange has not been implemented', this.name)); }

    getAccountBalances(callback: (error?: Error, account?: Account) => void): void {
        callback(new VError('Exchange.getAccountBalances() on the %s exchange has not been implemented', this.name)); }

    addOrder(newOrder: Order, callback: OrderCallback): void {
        callback(new VError('Exchange.addOrder() on the %s exchange has not been implemented', this.name)); }

    cancelOrder(cancelOrder, callback: OrderCallback): void {
        callback(new VError('Exchange.cancelOrder() on the %s exchange has not been implemented', this.name)); }

    getCurrencyMovementsBetweenDates(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate?: Date, currency?: string, type?: MovementType): void {
        callback(new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name)); }

    setTimeout(timeout: number): void {}
}