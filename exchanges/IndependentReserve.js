/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var moment = require('moment');
var _ = require('underscore');
var async = require('async');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
// import my Interfaces
var exchange_1 = require("../models/exchange");
var market_1 = require("../models/market");
var ticker_1 = require("../models/ticker");
var balance_1 = require("../models/balance");
var account_1 = require("../models/account");
var orderBook_1 = require("../models/orderBook");
var order_1 = require("../models/order");
var historicalAccountTrade_1 = require("../models/historicalAccountTrade");
var movement_1 = require("../models/movement");
// My packages
var logger = require('config-logger'), VError = require('verror'), IndependentReserve = require("independentreserve");
// default exchange config
var defaultExchangeSettings = {
    name: 'IndependentReserve',
    fixedCurrencies: ['BTC'],
    variableCurrencies: ['USD', 'AUD', 'NZD'],
    commissions: 0,
    currencyRounding: {
        BTC: 8,
        USD: 2,
        AUD: 2 },
    priceRounding: {
        BTCUSD: 2,
        BTCAUD: 2,
        BTCNZD: 2
    },
    defaultMinAmount: 0.01,
    publicPollingInterval: 5000,
    privatePollingInterval: 5000,
    maxFailures: 10,
    feeStructure: 'variableCurrency' // all fees are in USD. eg buy BTC will have a USD fee
};
var IRExchange = (function (_super) {
    __extends(IRExchange, _super);
    function IRExchange(exchangeConfig) {
        var functionName = 'IRExchange.constructor()';
        // override the default exchange settings from the config
        var exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);
        //Call the constructor of inherited Exchange object
        _super.call(this, exchangeSettings);
        this.currenciesToAcountGuidsMap = {};
        if (exchangeConfig.testInstance) {
            this.server = 'http://dev.api.independentreserve.com';
            logger.info('%s using test server %s', functionName, this.server);
        }
        logger.trace('%s initialising Independent Reserve client', functionName);
        this.client = new IndependentReserve(exchangeConfig.APIkey, exchangeConfig.APIsecret, this.server, // undefined defaults to production: https://api.independentreserve.com
        20000 // timeout in milliseconds
        );
    }
    // override abstract function with implementation for this exchange
    IRExchange.prototype.getTicker = function (symbol, callback) {
        var functionName = 'IndependentReserve.getTicker()', self = this;
        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);
        // call static Market functions to get the fixed and variable currencies
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol);
        var primaryCurrencyCode = getIRCurrencyCode(fixedCurrency);
        var secondaryCurrencyCode = getIRCurrencyCode(variableCurrency);
        this.client.getMarketSummary(primaryCurrencyCode, secondaryCurrencyCode, function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);
            self.failures = 0;
            var newTicker = new ticker_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(json.CurrentHighestBidPrice),
                ask: Number(json.CurrentLowestOfferPrice),
                last: Number(json.LastPrice),
                timestamp: new Date(json.CreatedTimestampUtc)
            });
            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName, newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last, moment(newTicker.timestamp).format('D MMM YY H:mm:ss'));
            self.markets[symbol].addTicker(newTicker);
            callback(null, newTicker);
        });
    };
    // override abstract function with implementation for this exchange
    IRExchange.prototype.getOrderBook = function (symbol, callback) {
        var functionName = 'IndependentReserve.getOrderBook()', self = this;
        logger.trace('%s about to get order book for market %s', functionName, symbol);
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol);
        var primaryCurrencyCode = getIRCurrencyCode(fixedCurrency);
        var secondaryCurrencyCode = getIRCurrencyCode(variableCurrency);
        this.client.getOrderBook(primaryCurrencyCode, secondaryCurrencyCode, function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);
            self.failures = 0;
            logger.trace('%s data: %s', functionName, JSON.stringify(json));
            //Convert Cryptsy bid and ask formats to a 2 dimentional array
            var bids = [], asks = [];
            json.BuyOrders.forEach(function (order, index) {
                bids[index] = [Number(order.Price), Number(order.Volume)]; //cast to Number as coming from JSON string
            });
            json.SellOrders.forEach(function (order, index) {
                asks[index] = [Number(order.Price), Number(order.Volume)]; //cast to Number as coming from JSON string
            });
            var newOrderBook = new orderBook_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(json.CreatedTimestampUtc),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });
            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0) {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName, self.name, symbol, newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length, newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length, moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss'));
            }
            else {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks', functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }
            self.markets[symbol].addOrderBook(newOrderBook);
            callback(null, newOrderBook);
        });
    };
    IRExchange.prototype.getMarketTrades = function (symbol, params, callback) {
        var functionName = 'IndependentReserve.getMarketTrades()', self = this;
        logger.trace('%s about to get historical trades for market %s', functionName, symbol);
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol);
        var primaryCurrencyCode = getIRCurrencyCode(fixedCurrency);
        var secondaryCurrencyCode = getIRCurrencyCode(variableCurrency);
        var trades = [];
        // TODO need to loop over the recent trades pages
        var numberTrades = 50;
        self.client.getRecentTrades(primaryCurrencyCode, secondaryCurrencyCode, numberTrades, function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getMarketTrades', self.getMarketTrades, callback);
            self.failures = 0;
            var trades = [];
            json.Trades.forEach(function (trade, i) {
                trades.push({
                    exchangeName: self.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.SecondaryCurrencyTradePrice),
                    quantityBN: new BigNumber(trade.PrimaryCurrencyAmount),
                    timestamp: new Date(trade.TradeTimestampUtc)
                });
            });
            logger.debug('%s %s historical trades returned', functionName, trades.length);
            callback(null, trades);
        });
    };
    // Implementation of abstract function to get account balances on the exchange
    IRExchange.prototype.getAccountBalances = function (callback) {
        var functionName = 'IndependentReserve.getAccountBalances()', self = this;
        logger.trace('%s about to get account balances', functionName);
        this.client.getAccounts(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getBalances', self.getAccountBalances, callback);
            self.failures = 0;
            logger.trace('%s accounts returned from the %s exchange:', functionName, self.name);
            logger.trace(json);
            var returnedAccount = new account_1.default([], self.name, self.currencyRounding);
            json.forEach(function (account) {
                var total = account.TotalBalance;
                var available = account.AvailableBalance;
                var currency = 'BTC';
                if (account.CurrencyCode != 'Xbt') {
                    currency = account.CurrencyCode.toUpperCase();
                }
                logger.debug('%s %s balances: total %s, available %s', functionName, currency, total, available);
                returnedAccount.setBalance(new balance_1.default({
                    exchangeName: self.name,
                    currency: currency,
                    totalBalance: total,
                    availableBalance: available
                }));
            });
            // set the balances on the exchange
            self.account = returnedAccount;
            callback(null, returnedAccount);
        });
    };
    // gets my pending orders - not the market orders/depth - for a given symbol
    IRExchange.prototype.getPendingOrdersForSymbol = function (symbol, callback) {
        var functionName = 'IndependentReserve.getPendingOrdersForSymbol()', self = this;
        logger.trace('%s about to get pending orders on the %s exchange for the %s market', functionName, this.name, symbol);
        var primaryCurrencyCode = getIRCurrencyCode(market_1.default.getFixedCurrency(symbol)), secondaryCurrencyCode = getIRCurrencyCode(market_1.default.getVariableCurrency(symbol));
        //can use any client to get the pending orders
        //TODO there could be more than 50 open orders so need to page through the results
        self.client.getOpenOrders(primaryCurrencyCode, secondaryCurrencyCode, 1, 50, function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getPendingOrdersForSymbol', self.getPendingOrdersForSymbol, callback, symbol);
            self.failures = 0;
            logger.debug('%s %s pending orders returned', self.name, json.Data.length);
            var orders = [];
            json.Data.forEach(function (order) {
                //convert from IndependentReserve side
                var side = convertSide(order.OrderType);
                var symbol = convertSymbol(order.PrimaryCurrencyCode, order.SecondaryCurrencyCode);
                var amountTradedBN = new BigNumber(order.Volume).
                    minus(order.Outstanding).
                    round(self.currencyRounding["BTC"]);
                var amountLastPartial = '0', numberPartialFills = 0;
                if (amountTradedBN.greaterThan(0)) {
                    amountLastPartial = amountTradedBN.toString();
                    numberPartialFills = 1; // there may have been more than 1 partial fill but we don't have the information here to work it out
                }
                var exchangeOrder = new order_1.default({
                    exchangeId: order.OrderGuid,
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: side,
                    amount: order.Volume,
                    amountTraded: amountTradedBN.toString(),
                    amountRemaining: order.Outstanding,
                    price: order.Price,
                    amountLastPartial: amountLastPartial,
                    numberPartialFills: numberPartialFills,
                    timestamp: new Date(order.CreatedTimestampUtc * 1000)
                });
                logger.debug('%s pending order:', functionName);
                exchangeOrder.log('debug');
                orders.push(exchangeOrder);
            });
            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);
            callback(null, orders);
        });
    };
    // creates a buy or sell order on an exchange
    IRExchange.prototype.addOrder = function (newOrder, callback) {
        var functionName = 'IndependentReserve.addOrder()', self = this;
        logger.trace('%s about to add the following new order:', functionName);
        newOrder.log('trace');
        // validate the side parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy') {
            var error = new VError('%s add order failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }
        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, newOrder.name, this.name);
            logger.error(error.stack);
            return callback(error);
        }
        var variableCurrency = newOrder.symbol.slice(3);
        var primaryCurrencyCode = 'Xbt'; // hard coding for now instead of mapping BTC -> Xbt
        var secondaryCurrencyCode = variableCurrency[0].toUpperCase() + variableCurrency.slice(1).toLocaleLowerCase();
        var orderType = getIROrderType(newOrder.type, newOrder.side);
        this.client.placeOrder(primaryCurrencyCode, secondaryCurrencyCode, orderType, newOrder.price, newOrder.amountRemaining, function (err, json) {
            var functionName = 'IndependentReserve.addOrder.placeOrder()';
            if (err) {
                var orderDesc = util_1.format('%s %s order with price %s, remaining amount %s, sell amount remaining %s %s and tag %s', newOrder.side, newOrder.type, newOrder.price, newOrder.amountRemaining, newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.tag);
                // no need to retry the API call with the following errors.
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.addOrderTimeout(err, newOrder, callback);
                }
                else if (err.name === 'Available balance too small.') {
                    var expectedAvailableBalanceBN = self.account.balances[newOrder.sell.currency].availableBalanceBN.
                        minus(newOrder.sellAmountRemainingBN);
                    var error = new VError('%s not enough funds on the %s exchange to add %s. Client %s total balance %s, available balance %s, expected available balance %s', functionName, self.name, orderDesc, newOrder.sell.currency, self.account.balances[newOrder.sell.currency].totalBalance, self.account.balances[newOrder.sell.currency].availableBalance, expectedAvailableBalanceBN.toString());
                    error.name = exchange_1.default.NOT_ENOUGH_FUNDS;
                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.name === 'Volume must be greater or equal to XBT 0.01.') {
                    var error = new VError('%s order amount remaining %s was too small for the %s exchange to add %s. Configured min amount for the %s market is %s', functionName, newOrder.amountRemaining, self.name, orderDesc, newOrder.symbol, self.minAmount[newOrder.symbol]);
                    error.name = exchange_1.default.AMOUNT_TOO_SMALL;
                    logger.error(error.stack);
                    return callback(error);
                }
                return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
            }
            logger.trace('%s data: %s', functionName, JSON.stringify(json));
            var data = {
                exchangeId: json.OrderGuid,
                timestamp: new Date(json.CreatedTimestampUtc)
            };
            self.addOrderSuccess(newOrder, callback, data);
        });
    };
    // cancels an pending order
    IRExchange.prototype.cancelOrder = function (cancelOrder, callback) {
        var functionName = 'IndependentReserve.cancelOrder()', self = this;
        logger.trace('%s about to cancel order with exchange id %s', functionName, cancelOrder.exchangeId);
        if (cancelOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, cancelOrder.name, this.name);
            logger.error(error.stack);
            throw error;
        }
        this.client.cancelOrder(cancelOrder.exchangeId, function (err, json) {
            if (err) {
                var errorTrade = util_1.format('%s order with id %s, tag %s, price %s and amount remaining %s', cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining);
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                else if (err.name === 'Order is in an invalid state to be cancelled.') {
                    var invalidStateError = new VError(err, '%s could not cancel %s as it is in an invalid state. Will now check what state the order is in.', functionName, errorTrade);
                    invalidStateError.name = err.name;
                    logger.warn(invalidStateError.stack);
                    return self.getOrder(cancelOrder.exchangeId, function (getOrderStateError, cancelledOrder) {
                        logger.warn('%s exchange state is %s for failed %s', functionName, cancelledOrder.state, errorTrade);
                        if (getOrderStateError) {
                            var error = new VError(getOrderStateError, '%s could not get order state of failed cancelled order with id %s', functionName, cancelOrder.exchangeId);
                            logger.error(error.stack);
                            return callback(error);
                        }
                        else if (cancelledOrder.state === 'filled') {
                            var error = new VError(invalidStateError, '%s order has already been filled so mark as ALREADY_FILLED', functionName);
                            error.name = exchange_1.default.ALREADY_FILLED;
                            logger.error(error.stack);
                            return callback(error);
                        }
                        else if (cancelledOrder.state === 'cancelled') {
                            var error = new VError(invalidStateError, '%s order has already been cancelled', functionName);
                            error.name = exchange_1.default.ALREADY_CANCELLED;
                            logger.error(error.stack);
                            return callback(error);
                        }
                        else {
                            var error = new VError(err, '%s could not cancel %s. getOrderState says it is in a %s state. Error: %s', functionName, errorTrade, cancelledOrder.state);
                            error.name = err.name;
                            return callback(error);
                        }
                    });
                }
                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(json));
            self.cancelOrderSuccess(cancelOrder, callback);
        });
    };
    IRExchange.prototype.getOrder = function (exchangeId, callback) {
        var functionName = 'IndependentReserve.getOrder()', self = this;
        logger.trace('%s about to get order for id %s', functionName, exchangeId);
        self.client.getOrderDetails(exchangeId, function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getOrder', self.getOrder, callback, exchangeId);
            self.failures = 0;
            logger.trace('%s order details for id %s: %s', functionName, exchangeId, JSON.stringify(data));
            var state;
            if (data.Status === 'Filled')
                state = 'filled';
            else if (data.Status === 'Open' || data.Status === 'PartiallyFilled' || data.Status === 'Open')
                state = 'pending';
            else if (data.Status === 'Cancelled' || data.Status === 'PartiallyFilledAndCancelled')
                state = 'cancelled';
            else {
                var error = new VError('%s could not convert Independent Reserve order Status %s so returning unknown order state', functionName, data.Status);
                logger.error(error.stack);
                return callback(error, null);
            }
            //convert from IndependentReserve side
            var side = convertSide(data.Type);
            var symbol = convertSymbol(data.PrimaryCurrencyCode, data.SecondaryCurrencyCode);
            // for some reason the order.ReservedAmount always = 0 hence we have to calculate the the remaining amount outselves
            var amountRemainingBN = new BigNumber(data.VolumeOrdered).
                minus(data.VolumeFilled).
                round(self.currencyRounding["BTC"]);
            var numberPartialFills = 0;
            if (data.VolumeFilled > 0) {
                numberPartialFills = 1; // there may have been more than 1 partial fill but we don't have the information here to work it out
            }
            // default will assume the order is of type limit
            var price = data.Price;
            var type = 'limit';
            // if a market order
            if (data.Type == "MarketOffer" || data.Type == "MarketBid") {
                price = data.AvgPrice;
                type = 'market';
            }
            var exchangeOrder = new order_1.default({
                exchangeId: exchangeId,
                exchangeName: self.name,
                symbol: symbol,
                state: state,
                side: side,
                type: type,
                amount: data.VolumeOrdered,
                amountTraded: data.VolumeFilled,
                amountRemaining: amountRemainingBN.toString(),
                price: price,
                amountLastPartial: data.VolumeFilled,
                numberPartialFills: numberPartialFills,
                timestamp: new Date(data.CreatedTimestampUtc * 1000)
            });
            logger.info('%s successfully got order:', functionName);
            exchangeOrder.log('info');
            callback(null, exchangeOrder);
        });
    };
    IRExchange.prototype.getAccountTradesBetweenDates = function (callback, fromDate, toDate) {
        var functionName = 'IndependentReserve.getAccountTradesBetweenDates()', self = this, maxTradesPerPage = 50, trades = [];
        var page = 1, tradesInPage, lastTradeTimestamp;
        logger.trace('%s about to get my historical trades', functionName);
        async.doUntil(function (callback) {
            self.client.getTrades(page++, maxTradesPerPage, function (err, json) {
                if (err)
                    return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);
                self.failures = 0;
                json.Data.forEach(function (trade, i) {
                    lastTradeTimestamp = new Date(trade.TradeTimestampUtc);
                    // just get the trades in the specified date range
                    if (fromDate <= lastTradeTimestamp &&
                        lastTradeTimestamp <= toDate) {
                        var historicalAccountTrade = new historicalAccountTrade_1.default({
                            exchangeName: self.name,
                            symbol: convertSymbol(trade.PrimaryCurrencyCode, trade.SecondaryCurrencyCode),
                            tradeId: trade.TradeGuid,
                            orderId: trade.OrderGuid,
                            side: convertSide(trade.OrderType),
                            priceBN: new BigNumber(trade.Price),
                            quantityBN: new BigNumber(trade.VolumeTraded),
                            timestamp: lastTradeTimestamp,
                            feeAmountBN: BigNumber(0),
                            feeCurrency: 'AUD'
                        });
                        trades.push(historicalAccountTrade);
                    }
                });
                tradesInPage = json.Data.length;
                logger.debug('%s %s historical trades returned in the page for a total of %s trades in the specified range', functionName, tradesInPage, trades.length);
                callback(null);
            });
        }, 
        // do until this function returns true
        function () {
            // stop if trades are older than the fromDate OR
            // the last page did not equal to the max number of trades in a page
            return lastTradeTimestamp < fromDate || tradesInPage != maxTradesPerPage;
        }, function (err) {
            callback(err, trades);
        });
    };
    IRExchange.prototype.getCurrencyMovementsBetweenDates = function (callback, fromDate, toDate, currency, type) {
        var functionName = "IndependentReserve.getCurrencyMovementsBetweenDates()", self = this, movements = [];
        logger.trace('%s about to get movements between %s (%s) and %s (%s)', functionName, fromDate.toString(), fromDate.getTime(), toDate.toString(), fromDate.getTime());
        if (!toDate) {
            toDate = new Date();
        }
        this.mapCurrenciesToAccountGuids(function (err) {
            if (err)
                return self.errorHandler(err, false, 'getCurrencyMovementsBetweenDates', self.getCurrencyMovementsBetweenDates, callback, fromDate, toDate, currency, type);
            self.failures = 0;
            var irCurrencies;
            if (currency) {
                irCurrencies = [getIRCurrencyCode(currency)];
            }
            else {
                irCurrencies = _.keys(self.currenciesToAcountGuidsMap);
            }
            var returnedMovements = [];
            async.eachSeries(irCurrencies, function (irCurrency, callback) {
                self.getSingleCurrencyMovementsBetweenDates(function (error, movements) {
                    returnedMovements = returnedMovements.concat(movements);
                    callback(null);
                }, fromDate, toDate, self.currenciesToAcountGuidsMap[irCurrency], type);
            }, function (error) {
                callback(error, returnedMovements);
            });
        });
    };
    IRExchange.prototype.mapCurrenciesToAccountGuids = function (callback) {
        var functionName = "IndependentReserve.mapCurrenciesToAccountGuids()", self = this;
        // return no error if the map of currencies to accounts already exists
        if (_.keys(this.currenciesToAcountGuidsMap).length > 0) {
            return callback(null);
        }
        logger.trace('%s about to get map of IR currencies to accounts', functionName);
        self.client.getAccounts(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'mapCurrenciesToAccountGuids', self.mapCurrenciesToAccountGuids, callback);
            self.failures = 0;
            logger.trace('%s accounts returned:\n%s', functionName, JSON.stringify(json));
            json.forEach(function (account) {
                self.currenciesToAcountGuidsMap[account.CurrencyCode] = account.AccountGuid;
            });
            return callback(null);
        });
    };
    IRExchange.prototype.getSingleCurrencyMovementsBetweenDates = function (callback, fromDate, toDate, accountGuid, type) {
        var functionName = "IndependentReserve.getSingleCurrencyMovementsBetweenDates()", self = this, maxMovementsPerPage = 50, movements = [];
        logger.trace('%s about to get movements between %s (%s) and %s (%s) for account GUID %s', functionName, fromDate.toString(), fromDate.getTime(), toDate.toString(), fromDate.getTime(), accountGuid);
        var transactionTypes = convertMovementType(type);
        var lastTradeTimestamp;
        var page = 1, movementsInPage;
        // loop through
        async.doUntil(function (callback) {
            logger.trace('%s about to get page %s of movements between %s (%s) and %s (%s) for account GUID %s', functionName, page, fromDate.toString(), fromDate.getTime(), toDate.toString(), fromDate.getTime(), accountGuid);
            self.client.getTransactions(accountGuid, fromDate, toDate, page++, maxMovementsPerPage, transactionTypes, function (err, json) {
                if (err)
                    return self.errorHandler(err, false, 'getSingleCurrencyMovementsBetweenDates', self.getSingleCurrencyMovementsBetweenDates, callback, fromDate, toDate, accountGuid, type);
                self.failures = 0;
                logger.trace('%s movements returned:\n%s', functionName, JSON.stringify(json));
                json.Data.forEach(function (movement, i) {
                    lastTradeTimestamp = new Date(movement.SettleTimestampUtc);
                    // just get the movements in the specified date range
                    if (fromDate <= lastTradeTimestamp &&
                        lastTradeTimestamp <= toDate) {
                        var amount;
                        if (movement.Debit != null) {
                            amount = movement.Debit;
                        }
                        else if (movement.Credit != null) {
                            amount = movement.Credit;
                        }
                        var newMovement = new movement_1.default({
                            exchangeName: self.name,
                            currency: convertIRCurrencyCode(movement.CurrencyCode),
                            type: movement.Type.toLowerCase(),
                            amount: amount,
                            timestamp: lastTradeTimestamp
                        });
                        movements.push(newMovement);
                    }
                });
                movementsInPage = json.Data.length;
                logger.debug('%s %s movements returned in the page', functionName, movements.length);
                callback(null);
            });
        }, 
        // do until this function returns true
        function () {
            // stop if the number of movements did not equal to the max number of movements in a page
            return movementsInPage != maxMovementsPerPage;
        }, function (err) {
            callback(err, movements);
        });
    };
    IRExchange.prototype.setTimeout = function (timeout) {
        this.client.timeout = timeout;
    };
    return IRExchange;
})(exchange_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = IRExchange;
;
function getIROrderType(orderType, side) {
    if (orderType == 'limit' && side == 'sell') {
        return 'LimitOffer';
    }
    else if (orderType == 'limit' && side == 'buy') {
        return 'LimitBid';
    }
    else if (orderType == 'market' && side == 'sell') {
        return 'MarketOffer';
    }
    else if (orderType == 'market' && side == 'buy') {
        return 'MarketBid';
    }
}
function convertMovementType(type) {
    if (!type) {
        return ['Deposit', 'Withdrawal'];
    }
    else if (type == movement_1.MovementType.deposit) {
        return ['Deposit'];
    }
    else if (type == movement_1.MovementType.withdrawal) {
        return ['Withdrawal'];
    }
    else {
        return [];
    }
}
function convertIRCurrencyCode(irCurrency) {
    if (irCurrency == 'Xbt') {
        return 'BTC';
    }
    return irCurrency.toUpperCase();
}
function getIRCurrencyCode(currency) {
    if (currency == 'BTC') {
        return 'Xbt';
    }
    return currency[0].toUpperCase() + currency.slice(1).toLocaleLowerCase();
}
function convertSymbol(primaryCurrencyCode, secondaryCurrencyCode) {
    var functionName = 'convertSymbol';
    var fixedCurrency = "BTC", variableCurrency = secondaryCurrencyCode.toUpperCase();
    if (primaryCurrencyCode !== 'Xbt') {
        var error = new VError('%s primary currency code %s not equal to Xbt', functionName, primaryCurrencyCode);
        logger.error(error);
        return error;
    }
    return fixedCurrency.concat(variableCurrency);
}
function convertSide(side) {
    var functionName = 'IndependentReserve.convertSide()';
    var convertedSide;
    if (side === 'LimitBid' || side === 'MarketBid') {
        convertedSide = 'buy';
    }
    else if (side === 'LimitOffer' || side === 'MarketOffer') {
        convertedSide = 'sell';
    }
    else if (side === 'buy') {
        convertedSide = 'LimitBid';
    }
    else if (side === 'sell') {
        convertedSide = 'LimitOffer';
    }
    else {
        var error = new VError('%s side %s is not buy, sell, bid or ask', functionName, side);
        logger.error(error.stack);
        throw error;
    }
    logger.trace('%s side %s convert to %s', functionName, side, convertedSide);
    return convertedSide;
}
