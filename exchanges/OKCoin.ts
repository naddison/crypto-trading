/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />

import * as moment from 'moment';
import * as _ from 'underscore';
import * as async from 'async';
import {format} from 'util';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

// import my Interfaces
import {default as Exchange, IExchangeSettings, OrderCallback, OrdersCallback,
    HistoricalTradesCallback,
    IHistoricalMarketTrade, HistoricalMarketTradesCallback}
    from "../models/exchange";
import Market from "../models/market";
import Ticker from "../models/ticker";
import Balance from "../models/balance";
import Account from "../models/account";
import OrderBook from "../models/orderBook";
import Order from "../models/order";
import {default as HistoricalAccountTrade, IHistoricalAccountTrade} from "../models/historicalAccountTrade";
import {default as Movement, MovementType} from "../models/movement";

const logger = require('config-logger'),
    VError = require('verror'),
    OkCoin = require("okcoin"),
    config = require('config');

const defaultExchangeSettings: IExchangeSettings =
{
	name: 'OKCoin',
	fixedCurrencies: ['BTC', 'LTC'],
	variableCurrencies: ['USD'],
	commissions: 0.002,				// default commission rather than defining for each currency
	currencyRounding: {
	    BTC: 8,
	    LTC: 8,
	    AUD: 2},
    priceRounding: {
        BTCUSD: 2,
        LTCUSD: 4,
        LTCBTC: 8
    },
    defaultMinAmount: 0.01,
	publicPollingInterval: 15000,	// milliseconds
	privatePollingInterval: 5000,    //polling interval of private methods
	maxFailures: 10,
    feeStructure: 'buyCurrency'
};

export default class OKCoinExchange extends Exchange
{
    client: any;
    server: string;

    getTransactionLimit: number;

    constructor(exchangeConfig: IExchangeSettings)
    {
        const functionName = 'OKCoin.constructor()';

        // override the default exchange settings from the config
        const exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);

        //Call the constructor of inherited Exchange object
        super(exchangeSettings);

        this.getTransactionLimit = 10000;

        let server;
        if (exchangeSettings.testInstance)
        {
            server = 'http://dev.okcoin.net';

            logger.info('%s using test server %s', functionName, server);
        }

        logger.trace('%s initialising OKCoin client', functionName);

        this.client = new OkCoin(
            exchangeSettings.APIkey,
            exchangeSettings.APIsecret,
            server
        );
    }

    // TODO need to implement
    getCurrencyMovementsBetweenDates(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate?: Date, currency?: string, type?: MovementType): void
    {
        const error = new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    }

    // TODO need to implement
    getOrder(exchangeId:string, callback:(error?:Error, trade?:Order) => void):void {
        const error = new VError('Exchange.getOrder() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    }

    getTicker(symbol: string, callback: (error: Error, ticker?: Ticker) => void): void
    {
        const functionName = 'OKCoin.getTicker()',
            self = this;

        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);

        const okCoinSymbol = OKCoinExchange.getSymbol(symbol);

        this.client.getTicker(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);

            self.failures = 0;

            const newTicker = new Ticker({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(json.ticker.buy),
                ask: Number(json.ticker.sell),
                last: Number(json.ticker.last),
                timestamp: new Date(json.date * 1000)
            });

            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName,
                newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last,
                moment(newTicker.timestamp).format('D MMM YY H:mm:ss') );

            self.markets[symbol].addTicker(newTicker);

            callback(null, newTicker);

        }, okCoinSymbol);
    }

    getOrderBook(symbol: string, callback: (error: Error, orderBook?: OrderBook) => void): void
    {
        const functionName = 'OKCoin.getOrderBook()',
            self = this;

        logger.trace('%s about to get order book for market %s', functionName, symbol);

        const fixedCurrency = Market.getFixedCurrency(symbol),
            variableCurrency = Market.getVariableCurrency(symbol),
            okCoinSymbol = OKCoinExchange.getSymbol(symbol);

        logger.trace('%s fixed currency %s, variable currency %s, symbol %s', functionName,
            fixedCurrency, variableCurrency, okCoinSymbol);

        this.client.getDepth(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);

            self.failures = 0;

            logger.trace('%s data: %s', functionName, JSON.stringify(json) );

            //Convert bid and ask formats to a 2 dimensional array
            const bids = [], asks = [];
            json.bids.forEach(function(order) {
                bids.push([Number(order[0]),Number(order[1])]);	//cast to Number as coming from JSON string
            });
            json.asks.forEach(function(order) {
                asks.unshift([Number(order[0]),Number(order[1])]);	//cast to Number as coming from JSON string
            });

            const newOrderBook = new OrderBook({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });

            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0)
            {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName,
                    self.name, symbol,
                    newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length,
                    newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length,
                    moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss') );
            }
            else
            {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks',
                    functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }

            self.markets[symbol].addOrderBook(newOrderBook);

            if (_.isFunction(callback) ) callback(null, newOrderBook);
        }, okCoinSymbol);
    };

    getAccountBalances(callback: (error: Error, account?: Account) => void): void
    {
        const functionName = 'OKCoin.getAccountBalances()',
            self = this;

        logger.trace('%s about to get account balances', functionName);

        this.client.getUserInfo(function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);

            self.failures = 0;

            logger.trace('%s balances returned from the %s exchange:', functionName, self.name);
            logger.trace(data);

            const returnedAccount = new Account([], self.name, self.currencyRounding);

            ['btc','usd','ltc'].forEach(function(currency)
            {
                const totalBN = new BigNumber(data.info.funds.free[currency]).
                plus(data.info.funds.freezed[currency]).
                round(self.currencyRounding[currency.toUpperCase()], BigNumber.ROUND_DOWN);

                logger.debug('%s %s total %s = free %s + freezed %s', functionName,
                    currency, totalBN.toString(),
                    data.info.funds.free[currency],
                    data.info.funds.freezed[currency]);

                returnedAccount.setBalance(
                    new Balance({
                        exchangeName: self.name,
                        currency: currency.toUpperCase(),
                        totalBalance: totalBN.toString(),
                        availableBalance: data.info.funds.free[currency]
                    })
                );
            });

            // set the balances on the exchange
            self.account = returnedAccount;

            callback(null, returnedAccount);
        });
    };

    // gets my pending orders - not the market orders/depth
    getPendingOrdersForSymbol(symbol: string, callback: OrdersCallback): void
    {
        const functionName = 'OKCoin.getPendingOrdersForSymbol()',
            self = this;

        logger.trace('%s about to get pending orders on the %s exchange for the %s market', functionName,
            this.name, symbol);

        const orders: Order[] = [];
        const okCoinSymbol = OKCoinExchange.getSymbol(symbol);

        self.client.getOrderInfo(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getPendingOrdersForSymbol', self.getPendingOrdersForSymbol, callback, symbol);

            self.failures = 0;

            logger.debug('%s %s pending orders returned', self.name, json.orders.length);

            json.orders.forEach(function(order)
            {
                const amountRemainingBN = new BigNumber(order.amount).
                minus(order.deal_amount);

                const exchangeOrder = new Order({
                    exchangeId: order.order_id,
                    exchangeName: self.name,
                    symbol: symbol,
                    state: OKCoinExchange.getState(order.status),
                    side: order.side,
                    amount: order.amount,
                    amountTraded: order.deal_amount,
                    amountRemaining: amountRemainingBN.toString(),
                    price: order.priceBN.toString(),
                    timestamp: new Date()
                });

                logger.debug('pending order:');
                exchangeOrder.log('debug');

                orders.push(exchangeOrder);
            });

            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);

            callback(null, orders);

        }, okCoinSymbol, '-1');
    };

    // creates a buy or sell order on an exchange
    addOrder(newOrder, callback: OrderCallback): void
    {
        const functionName = 'OKCoin.addOrder()',
            self = this;

        logger.trace('%s about to add the following new order:', functionName );
        newOrder.log('trace');

        // validate the side parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy')
        {
            const error = new VError('%s add trade failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }

        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                newOrder.name, this.name);

            logger.error(error.stack);
            return callback(error);
        }

        const symbol = OKCoinExchange.getSymbol(newOrder.symbol);

        this.client.addOrder(
            function(err, json)
            {
                if (err)
                {
                    const orderDesc = format('%s trade with price %s, remaining amount %s, sell amount remaining %s %s and tag %s on %s[%s]', functionName,
                        newOrder.side, newOrder.priceBN,
                        newOrder.amountRemaining, newOrder.sell.amountRemaining, newOrder.sell.currency,
                        newOrder.tag, newOrder.exchangeName, newOrder.symbol);

                    if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                    {
                        return self.addOrderTimeout(err, newOrder, callback);
                    }
                    else if (err.name === 10010 || err.name === 10016 || err.name === 10035)
                    {
                        const error = new VError(err, '%s not enough funds on add %s', functionName,
                            self.name, orderDesc);

                        error.name = Exchange.NOT_ENOUGH_FUNDS;

                        logger.error(error.stack);
                        return callback(error);
                    }
                    // Order is less than minimum trade amount
                    else if (err.name === 10011)
                    {
                        const error = new VError(err, '%s trade amount %s too small to add %s', functionName,
                            newOrder.amountRemaining, orderDesc);

                        error.name = Exchange.AMOUNT_TOO_SMALL;

                        logger.error(error.stack);
                        return callback(error);
                    }

                    return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
                }

                logger.trace('%s json data: %s', functionName, JSON.stringify(json) );

                const data = {
                    exchangeId: json.order_id,
                    timestamp: new Date()
                };

                self.addOrderSuccess(newOrder, callback, data);

            }, symbol, newOrder.side, newOrder.amountRemaining, newOrder.price);
    }

    // cancels an pending order
    cancelOrder(cancelOrder: Order, callback: OrderCallback): void
    {
        const functionName = 'OKCoin.cancelOrder()',
            self = this;

        logger.trace('%s about to cancel order with exchange id %s', functionName,
            cancelOrder.exchangeId);

        if (cancelOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            throw error;
        }

        const symbol = OKCoinExchange.getSymbol(cancelOrder.symbol);

        this.client.cancelOrder(function(err, json)
        {
            if (err)
            {
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                // if order does not exist
                else if (err.name === 10009)
                {
                    return self.cancelOrderInvalidState(err, cancelOrder, callback);
                }

                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }

            logger.trace('%s json data: %s', functionName, JSON.stringify(json) );

            self.cancelOrderSuccess(cancelOrder, callback);

        }, symbol, cancelOrder.exchangeId);
    }

    getMarketTrades(symbol: string, numberOfTrades: number, callback: HistoricalMarketTradesCallback): void
    {
        const functionName = 'OKCoin.getMarketTrades()',
            self = this;

        logger.trace('%s about to get historical trades for market %s', functionName, symbol);

        const fixedCurrency = Market.getFixedCurrency(symbol),
            variableCurrency = Market.getVariableCurrency(symbol),
            okCoinSymbol = fixedCurrency + '_' + variableCurrency;

        logger.trace('%s fixed currency %s, variable currency %s, symbol %s', functionName,
            fixedCurrency, variableCurrency, okCoinSymbol);

        this.client.getTrades(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getMarketTrades', self.getMarketTrades, callback, symbol);

            self.failures = 0;

            const trades: IHistoricalMarketTrade[] = [];

            json.forEach(function(trade)
            {
                trades.push({
                    exchangeName: this.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: new Date(trade.date_ms)
                });
            });

            logger.debug('%s %s historical trades returned', functionName, trades.length);

            callback(null, trades);
        }, okCoinSymbol);
    }

    getAccountTradesBetweenDates(callback: HistoricalTradesCallback, fromDate: Date, toDate: Date = new Date() ): void
    {
        const functionName = 'OKCoin.getAccountTradesBetweenDates()',
            self = this,
            maxTradesPerCall = 600,
            returnedTrades: HistoricalAccountTrade[] = [];

        let sinceTid = 1,
            tradesInCall: number,
            lastTradeTimestamp: Date;

        logger.trace('%s about to get historical account trades from %s (%s) to %s (%s)', functionName,
            fromDate.toString(), fromDate.getTime(),
            toDate.toString(), toDate.getTime() );

        async.doUntil(
            function (callback)
            {
                self.getAccountTradesSinceTid(sinceTid, function (err, trades: HistoricalAccountTrade[])
                {
                    if (err) return callback(err);

                    trades.forEach(function(trade)
                    {
                        if (fromDate <= trade.timestamp &&
                            trade.timestamp <= toDate)
                        {
                            returnedTrades.push(trade);
                        }
                    });

                    const lastTradeId = _.last(trades).tradeId;
                    sinceTid = Number(lastTradeId) + 1;

                    logger.trace('%s last trade id = %s. Next since tid = %s', functionName, lastTradeId, sinceTid);

                    tradesInCall = trades.length;

                    callback(null);
                });
            },
            // do until this function returns true
            function()
            {
                // stop if the last page did not equal to the max number of trades in a page
                return tradesInCall != maxTradesPerCall || lastTradeTimestamp > toDate;
            },
            function(err)
            {
                callback(err, returnedTrades)
            }
        );
    }

    getAccountTradesSinceTid(sinceTid: number, callback: HistoricalTradesCallback): void
    {
        const functionName = 'OKCoin.getAccountTradesSinceTid()',
            self = this;

        // hard coding for now. other option is LTCUSD
        const symbol = 'BTCUSD';

        logger.trace('%s about to get historical account trades since trade id %s for market ', functionName,
            sinceTid, symbol );

        const okCoinSymbol = OKCoinExchange.getSymbol(symbol);

        this.client.getTradeHistory(function(err, trades)
        {
            if (err) return self.errorHandler(err, true, 'getSingleAccountTradesSinceDate', self.getAccountTradesSinceTid, callback, sinceTid);

            self.failures = 0;

            logger.trace('%s data returned: %s', functionName, JSON.stringify(trades) );

            let returnedTrades: HistoricalAccountTrade[] = [];

            trades.forEach(function(trade)
            {
                const tradeDate = new Date(trade.date_ms);

                const historicalAccountTrade = new HistoricalAccountTrade({
                    exchangeName: self.name,
                    symbol: symbol,
                    side: trade.side,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: tradeDate,
                    tradeId: trade.tid
                });

                returnedTrades.push(historicalAccountTrade);

                logger.debug('%s, %s', functionName, trade.tid, tradeDate.toDateString() );
            });

            logger.debug('%s %s historical trades on my account', functionName, returnedTrades.length );

            callback(null, returnedTrades);

        }, okCoinSymbol, sinceTid);
    }

    setTimeout(timeout: number)
    {
        this.client.timeout = timeout;
    }

    static getState(status: number): string
    {
        if (status == -1) return "cancelled";
        else if (status == 0) return "pending";
        else if (status == 1) return "pendingPartial";
        else if (status == 2) return "filled";
        else if (status == 4) return "cancelled";
        else return "unknown";
    }

    static getSymbol(symbol: string): string
    {
        const fixedCurrency = Market.getFixedCurrency(symbol),
            variableCurrency = Market.getVariableCurrency(symbol);

        return fixedCurrency.toLowerCase() + '_' + variableCurrency.toLowerCase();
    }
}
