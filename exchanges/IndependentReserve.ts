/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />

import * as moment from 'moment';
import * as _ from 'underscore';
import * as async from 'async';
import {format} from 'util';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

// import my Interfaces
import {default as Exchange, IExchangeSettings, OrderCallback, OrdersCallback,
    HistoricalTradesCallback,
    IHistoricalMarketTrade, HistoricalMarketTradesCallback}
    from "../models/exchange";
import Market from "../models/market";
import Ticker from "../models/ticker";
import Balance from "../models/balance";
import Account from "../models/account";
import OrderBook from "../models/orderBook";
import Order from "../models/order";
import HistoricalAccountTrade from "../models/historicalAccountTrade";
import {default as Movement, MovementType} from "../models/movement";

// My packages
const logger = require('config-logger'),
    VError = require('verror'),
	IndependentReserve = require("independentreserve");

// default exchange config
const defaultExchangeSettings: IExchangeSettings = {
	name: 'IndependentReserve',
	fixedCurrencies: ['BTC'],
	variableCurrencies: ['USD', 'AUD', 'NZD'],
	commissions: 0,
	currencyRounding: {
	    BTC:8,
	    USD:2,
	    AUD:2},
    priceRounding: {
        BTCUSD: 2,
        BTCAUD: 2,
        BTCNZD: 2
    },
    defaultMinAmount: 0.01,
	publicPollingInterval: 5000,	// milliseconds
	privatePollingInterval: 5000,    //polling interval of private methods
	maxFailures: 10,
    feeStructure: 'variableCurrency'    // all fees are in USD. eg buy BTC will have a USD fee
};

export default class IRExchange extends Exchange
{
    client;
    server: string;

    // maps each currency to an account globally unique identifier. eg {'Usd', '49994921-60ec-411e-8a78-d0eba078d5e9'}
    // note the currency is in IR format. eg Usd, Aud, Nzd and Xbt
    currenciesToAcountGuidsMap: {[irCurrency: string]: string};

    constructor(exchangeConfig: IExchangeSettings)
    {
        const functionName = 'IRExchange.constructor()';

        // override the default exchange settings from the config
        const exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);

        //Call the constructor of inherited Exchange object
        super(exchangeSettings);

        this.currenciesToAcountGuidsMap = {};

        if (exchangeConfig.testInstance)
        {
            this.server = 'http://dev.api.independentreserve.com';

            logger.info('%s using test server %s', functionName, this.server);
        }

        logger.trace('%s initialising Independent Reserve client', functionName);

        this.client = new IndependentReserve(
            exchangeConfig.APIkey,
            exchangeConfig.APIsecret,
            this.server,  // undefined defaults to production: https://api.independentreserve.com
            20000   // timeout in milliseconds
        );
    }

    // override abstract function with implementation for this exchange
    getTicker(symbol: string, callback: (error: Error, ticker: Ticker) => void): void
    {
        const functionName = 'IndependentReserve.getTicker()',
            self = this;

        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);

        // call static Market functions to get the fixed and variable currencies
        const	fixedCurrency = Market.getFixedCurrency(symbol),
            variableCurrency = Market.getVariableCurrency(symbol);

        const primaryCurrencyCode = getIRCurrencyCode(fixedCurrency);
        const secondaryCurrencyCode = getIRCurrencyCode(variableCurrency);

        this.client.getMarketSummary(primaryCurrencyCode, secondaryCurrencyCode, function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);

            self.failures = 0;

            const newTicker = new Ticker({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(json.CurrentHighestBidPrice),
                ask: Number(json.CurrentLowestOfferPrice),
                last: Number(json.LastPrice),
                timestamp: new Date(json.CreatedTimestampUtc)
            });

            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName,
                newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last,
                moment(newTicker.timestamp).format('D MMM YY H:mm:ss') );

            self.markets[symbol].addTicker(newTicker);

            callback(null, newTicker);
        });
    }

    // override abstract function with implementation for this exchange
    getOrderBook(symbol: string, callback: (error: Error, orderBook: OrderBook) => void): void
    {
        const functionName = 'IndependentReserve.getOrderBook()',
            self = this;

        logger.trace('%s about to get order book for market %s', functionName, symbol);

        var	fixedCurrency = Market.getFixedCurrency(symbol),
            variableCurrency = Market.getVariableCurrency(symbol);

        const primaryCurrencyCode = getIRCurrencyCode(fixedCurrency);
        const secondaryCurrencyCode = getIRCurrencyCode(variableCurrency);

        this.client.getOrderBook(primaryCurrencyCode, secondaryCurrencyCode, function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);

            self.failures = 0;

            logger.trace('%s data: %s', functionName, JSON.stringify(json) );

            //Convert Cryptsy bid and ask formats to a 2 dimentional array
            const bids = [], asks = [];
            json.BuyOrders.forEach(function(order, index) {
                bids[index] = [Number(order.Price),Number(order.Volume)];	//cast to Number as coming from JSON string
            });
            json.SellOrders.forEach(function(order, index) {
                asks[index] = [Number(order.Price),Number(order.Volume)];	//cast to Number as coming from JSON string
            });

            const newOrderBook = new OrderBook({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(json.CreatedTimestampUtc),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });

            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0)
            {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName,
                    self.name, symbol,
                    newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length,
                    newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length,
                    moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss') );
            }
            else
            {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks',
                    functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }

            self.markets[symbol].addOrderBook(newOrderBook);

            callback(null, newOrderBook);
        });
    }

    getMarketTrades(symbol: string, params, callback: HistoricalMarketTradesCallback): void
    {
        const functionName = 'IndependentReserve.getMarketTrades()',
            self = this;

        logger.trace('%s about to get historical trades for market %s', functionName, symbol);

        var	fixedCurrency = Market.getFixedCurrency(symbol),
            variableCurrency = Market.getVariableCurrency(symbol);

        const primaryCurrencyCode = getIRCurrencyCode(fixedCurrency);
        const secondaryCurrencyCode = getIRCurrencyCode(variableCurrency);

        const trades: IHistoricalMarketTrade[] = [];

        // TODO need to loop over the recent trades pages
        const numberTrades = 50;

        self.client.getRecentTrades(primaryCurrencyCode, secondaryCurrencyCode, numberTrades, function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getMarketTrades', self.getMarketTrades, callback);

            self.failures = 0;

            const trades: IHistoricalMarketTrade[] = [];

            json.Trades.forEach(function(trade, i)
            {
                trades.push({
                    exchangeName: self.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.SecondaryCurrencyTradePrice),
                    quantityBN: new BigNumber(trade.PrimaryCurrencyAmount),
                    timestamp: new Date(trade.TradeTimestampUtc)
                });
            });

            logger.debug('%s %s historical trades returned', functionName, trades.length);

            callback(null, trades);
        });
    }

    // Implementation of abstract function to get account balances on the exchange
    getAccountBalances(callback: (error: Error, account?: Account) => void): void
    {
        const functionName = 'IndependentReserve.getAccountBalances()',
            self = this;

        logger.trace('%s about to get account balances', functionName);

        this.client.getAccounts(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getBalances', self.getAccountBalances, callback);

            self.failures = 0;

            logger.trace('%s accounts returned from the %s exchange:', functionName, self.name);
            logger.trace(json);

            const returnedAccount = new Account([], self.name, self.currencyRounding);

            json.forEach(function(account)
            {
                const total = account.TotalBalance;
                const available = account.AvailableBalance;

                let currency = 'BTC';
                if (account.CurrencyCode != 'Xbt')
                {
                    currency = account.CurrencyCode.toUpperCase();
                }

                logger.debug('%s %s balances: total %s, available %s', functionName,
                    currency, total, available);

                returnedAccount.setBalance(
                    new Balance({
                        exchangeName: self.name,
                        currency: currency,
                        totalBalance: total,
                        availableBalance: available
                    })
                );
            });

            // set the balances on the exchange
            self.account = returnedAccount;

            callback(null, returnedAccount);
        });
    }

    // gets my pending orders - not the market orders/depth - for a given symbol
    getPendingOrdersForSymbol(symbol: string, callback: OrdersCallback): void
    {
        const functionName = 'IndependentReserve.getPendingOrdersForSymbol()',
            self = this;

        logger.trace('%s about to get pending orders on the %s exchange for the %s market', functionName,
            this.name, symbol);

        const primaryCurrencyCode = getIRCurrencyCode(Market.getFixedCurrency(symbol) ),
            secondaryCurrencyCode = getIRCurrencyCode(Market.getVariableCurrency(symbol) );

        //can use any client to get the pending orders
        //TODO there could be more than 50 open orders so need to page through the results
        self.client.getOpenOrders(primaryCurrencyCode, secondaryCurrencyCode, 1, 50, function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getPendingOrdersForSymbol', self.getPendingOrdersForSymbol, callback, symbol);

            self.failures = 0;

            logger.debug('%s %s pending orders returned', self.name, json.Data.length);

            const orders: Order[] = [];

            json.Data.forEach(function(order)
            {
                //convert from IndependentReserve side
                const side = convertSide(order.OrderType);

                const symbol = convertSymbol(order.PrimaryCurrencyCode, order.SecondaryCurrencyCode);

                const amountTradedBN = new BigNumber(order.Volume).
                    minus(order.Outstanding).
                    round(self.currencyRounding["BTC"]);

                let amountLastPartial = '0',
                    numberPartialFills = 0;

                if (amountTradedBN.greaterThan(0))
                {
                    amountLastPartial = amountTradedBN.toString();
                    numberPartialFills = 1;     // there may have been more than 1 partial fill but we don't have the information here to work it out
                }

                const exchangeOrder = new Order({
                    exchangeId: order.OrderGuid,
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: side,
                    amount: order.Volume,
                    amountTraded: amountTradedBN.toString(),
                    amountRemaining: order.Outstanding,
                    price: order.Price,
                    amountLastPartial: amountLastPartial,
                    numberPartialFills: numberPartialFills,
                    timestamp: new Date(order.CreatedTimestampUtc * 1000)
                });

                logger.debug('%s pending order:', functionName);
                exchangeOrder.log('debug');

                orders.push(exchangeOrder);
            });

            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);

            callback(null, orders);
        });
    }

    // creates a buy or sell order on an exchange
    addOrder(newOrder, callback: OrderCallback): void
    {
        const functionName = 'IndependentReserve.addOrder()',
            self = this;

        logger.trace('%s about to add the following new order:', functionName );
        newOrder.log('trace');

        // validate the side parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy')
        {
            const error = new VError('%s add order failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }

        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                newOrder.name, this.name);

            logger.error(error.stack);
            return callback(error);
        }

        var	variableCurrency = newOrder.symbol.slice(3);

        const primaryCurrencyCode = 'Xbt';    // hard coding for now instead of mapping BTC -> Xbt
        const secondaryCurrencyCode = variableCurrency[0].toUpperCase() + variableCurrency.slice(1).toLocaleLowerCase();

        const orderType = getIROrderType(newOrder.type, newOrder.side);

        this.client.placeOrder(
            primaryCurrencyCode,
            secondaryCurrencyCode,
            orderType,
            newOrder.price,
            newOrder.amountRemaining,
            function(err, json)
            {
                const functionName = 'IndependentReserve.addOrder.placeOrder()';

                if (err)
                {
                    const orderDesc = format('%s %s order with price %s, remaining amount %s, sell amount remaining %s %s and tag %s',
                        newOrder.side, newOrder.type, newOrder.price, newOrder.amountRemaining,
                        newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.tag);

                    // no need to retry the API call with the following errors.
                    if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                    {
                        return self.addOrderTimeout(err, newOrder, callback);
                    }
                    else if (err.name === 'Available balance too small.')
                    {
                        const expectedAvailableBalanceBN = self.account.balances[newOrder.sell.currency].availableBalanceBN.
                        minus(newOrder.sellAmountRemainingBN);

                        const error = new VError('%s not enough funds on the %s exchange to add %s. Client %s total balance %s, available balance %s, expected available balance %s', functionName,
                            self.name, orderDesc,
                            newOrder.sell.currency, self.account.balances[newOrder.sell.currency].totalBalance,
                            self.account.balances[newOrder.sell.currency].availableBalance,
                            expectedAvailableBalanceBN.toString() );

                        error.name = Exchange.NOT_ENOUGH_FUNDS;

                        logger.error(error.stack);
                        return callback(error);
                    }
                    else if (err.name === 'Volume must be greater or equal to XBT 0.01.')
                    {
                        const error = new VError('%s order amount remaining %s was too small for the %s exchange to add %s. Configured min amount for the %s market is %s', functionName,
                            newOrder.amountRemaining, self.name, orderDesc,
                            newOrder.symbol, self.minAmount[newOrder.symbol]);

                        error.name = Exchange.AMOUNT_TOO_SMALL;

                        logger.error(error.stack);
                        return callback(error);
                    }

                    return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
                }

                logger.trace('%s data: %s', functionName, JSON.stringify(json));

                const data = {
                    exchangeId: json.OrderGuid,
                    timestamp: new Date(json.CreatedTimestampUtc)
                };

                self.addOrderSuccess(newOrder, callback, data);
            }
        );
    }

    // cancels an pending order
    cancelOrder(cancelOrder, callback: OrderCallback): void
    {
        const functionName = 'IndependentReserve.cancelOrder()',
            self = this;

        logger.trace('%s about to cancel order with exchange id %s', functionName,
            cancelOrder.exchangeId);

        if (cancelOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                cancelOrder.name, this.name);
            logger.error(error.stack);
            throw error;
        }

        this.client.cancelOrder(cancelOrder.exchangeId, function(err, json)
        {
            if (err)
            {
                const errorTrade = format('%s order with id %s, tag %s, price %s and amount remaining %s',
                    cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining);

                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                else if (err.name === 'Order is in an invalid state to be cancelled.')
                {
                    const invalidStateError = new VError(err, '%s could not cancel %s as it is in an invalid state. Will now check what state the order is in.', functionName,
                        errorTrade);

                    invalidStateError.name = err.name;
                    logger.warn(invalidStateError.stack);

                    return self.getOrder(cancelOrder.exchangeId, function(getOrderStateError, cancelledOrder)
                    {
                        logger.warn('%s exchange state is %s for failed %s', functionName,
                            cancelledOrder.state, errorTrade);

                        if (getOrderStateError)
                        {
                            const error = new VError(getOrderStateError, '%s could not get order state of failed cancelled order with id %s', functionName,
                                cancelOrder.exchangeId);

                            logger.error(error.stack);
                            return callback(error);
                        }
                        else if (cancelledOrder.state === 'filled')
                        {
                            const error = new VError(invalidStateError, '%s order has already been filled so mark as ALREADY_FILLED', functionName);
                            error.name = Exchange.ALREADY_FILLED;

                            logger.error(error.stack);
                            return callback(error);
                        }
                        else if (cancelledOrder.state === 'cancelled')
                        {
                            const error = new VError(invalidStateError, '%s order has already been cancelled', functionName);
                            error.name = Exchange.ALREADY_CANCELLED;

                            logger.error(error.stack);
                            return callback(error);
                        }
                        else
                        {
                            const error = new VError(err, '%s could not cancel %s. getOrderState says it is in a %s state. Error: %s', functionName,
                                errorTrade, cancelledOrder.state);

                            error.name = err.name;
                            return callback(error);
                        }
                    });
                }

                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }

            logger.trace('%s json data: %s', functionName, JSON.stringify(json) );

            self.cancelOrderSuccess(cancelOrder, callback);
        });
    }

    getOrder(exchangeId: string, callback: OrderCallback): void
    {
        const functionName = 'IndependentReserve.getOrder()',
            self = this;

        logger.trace('%s about to get order for id %s', functionName, exchangeId);

        self.client.getOrderDetails(exchangeId, function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'getOrder', self.getOrder, callback, exchangeId);

            self.failures = 0;

            logger.trace('%s order details for id %s: %s', functionName,
                exchangeId, JSON.stringify(data));

            let state;
            if (data.Status === 'Filled') state = 'filled';
            else if (data.Status === 'Open' || data.Status === 'PartiallyFilled' || data.Status === 'Open') state = 'pending';
            else if (data.Status === 'Cancelled' || data.Status === 'PartiallyFilledAndCancelled') state = 'cancelled';
            else
            {
                const error = new VError('%s could not convert Independent Reserve order Status %s so returning unknown order state', functionName, data.Status);

                logger.error(error.stack);
                return callback(error, null);
            }

            //convert from IndependentReserve side
            const side = convertSide(data.Type);

            const symbol = convertSymbol(data.PrimaryCurrencyCode, data.SecondaryCurrencyCode);

            // for some reason the order.ReservedAmount always = 0 hence we have to calculate the the remaining amount outselves
            const amountRemainingBN = new BigNumber(data.VolumeOrdered).
            minus(data.VolumeFilled).
            round(self.currencyRounding["BTC"]);

            let numberPartialFills = 0;

            if (data.VolumeFilled > 0)
            {
                numberPartialFills = 1;     // there may have been more than 1 partial fill but we don't have the information here to work it out
            }

            // default will assume the order is of type limit
            let price: number = data.Price;
            let type = 'limit';

            // if a market order
            if (data.Type == "MarketOffer" || data.Type == "MarketBid")
            {
                price = data.AvgPrice;
                type = 'market';
            }

            const exchangeOrder = new Order({
                exchangeId: exchangeId,
                exchangeName: self.name,
                symbol: symbol,
                state: state,
                side: side,
                type: type,
                amount: data.VolumeOrdered,
                amountTraded: data.VolumeFilled,
                amountRemaining: amountRemainingBN.toString(),
                price: price,
                amountLastPartial: data.VolumeFilled,
                numberPartialFills: numberPartialFills,
                timestamp: new Date(data.CreatedTimestampUtc * 1000)
            });

            logger.info('%s successfully got order:', functionName);
            exchangeOrder.log('info');

            callback(null, exchangeOrder);
        });
    }

    getAccountTradesBetweenDates(callback: HistoricalTradesCallback, fromDate: Date, toDate?: Date): void
    {
        const functionName = 'IndependentReserve.getAccountTradesBetweenDates()',
            self = this,
            maxTradesPerPage = 50,
            trades: HistoricalAccountTrade[] = [];

        let page = 1,
            tradesInPage: number,
            lastTradeTimestamp: Date;

        logger.trace('%s about to get my historical trades', functionName);

        async.doUntil(
            function(callback)
            {
                self.client.getTrades(page++, maxTradesPerPage, function(err, json)
                {
                    if (err) return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);

                    self.failures = 0;

                    json.Data.forEach(function(trade, i)
                    {
                        lastTradeTimestamp = new Date(trade.TradeTimestampUtc);

                        // just get the trades in the specified date range
                        if (fromDate <= lastTradeTimestamp &&
                            lastTradeTimestamp <= toDate )
                        {
                            const historicalAccountTrade = new HistoricalAccountTrade({
                                exchangeName: self.name,
                                symbol: convertSymbol(trade.PrimaryCurrencyCode, trade.SecondaryCurrencyCode),
                                tradeId: trade.TradeGuid,
                                orderId: trade.OrderGuid,
                                side: convertSide(trade.OrderType),
                                priceBN: new BigNumber(trade.Price),
                                quantityBN: new BigNumber(trade.VolumeTraded),
                                timestamp: lastTradeTimestamp,
                                feeAmountBN: BigNumber(0),
                                feeCurrency: 'AUD'
                            });

                            trades.push(historicalAccountTrade);
                        }
                    });

                    tradesInPage = json.Data.length;

                    logger.debug('%s %s historical trades returned in the page for a total of %s trades in the specified range', functionName,
                        tradesInPage, trades.length);

                    callback(null);
                });
            },
            // do until this function returns true
            function()
            {
                // stop if trades are older than the fromDate OR
                // the last page did not equal to the max number of trades in a page
                return lastTradeTimestamp < fromDate || tradesInPage != maxTradesPerPage;
            },
            function(err)
            {
                callback(err, trades)
            }
        );
    }

    getCurrencyMovementsBetweenDates(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate?: Date, currency?: string, type?: MovementType): void
    {
        const functionName = "IndependentReserve.getCurrencyMovementsBetweenDates()",
            self = this,
            movements: Movement[] = [];

        logger.trace('%s about to get movements between %s (%s) and %s (%s)', functionName,
            fromDate.toString(), fromDate.getTime(),
            toDate.toString(), fromDate.getTime() );

        if (!toDate) { toDate = new Date(); }

        this.mapCurrenciesToAccountGuids(function(err)
        {
            if (err) return self.errorHandler(err, false, 'getCurrencyMovementsBetweenDates', self.getCurrencyMovementsBetweenDates, callback, fromDate, toDate, currency, type);

            self.failures = 0;

            let irCurrencies: string[];

            if (currency)
            {
                irCurrencies = [getIRCurrencyCode(currency)];
            }
            else
            {
                irCurrencies = _.keys(self.currenciesToAcountGuidsMap)
            }

            let returnedMovements: Movement[] = [];

            async.eachSeries
            (
                irCurrencies,

                function(irCurrency: string, callback)
                {
                    self.getSingleCurrencyMovementsBetweenDates(function(error, movements)
                    {
                        returnedMovements = returnedMovements.concat(movements);

                        callback(null);

                    }, fromDate, toDate, self.currenciesToAcountGuidsMap[irCurrency], type)
                },

                function(error: Error)
                {
                    callback(error, returnedMovements);
                }
            )
        });
    }

    private mapCurrenciesToAccountGuids(callback: (err: Error)=> void): void
    {
        const functionName = "IndependentReserve.mapCurrenciesToAccountGuids()",
            self = this;

        // return no error if the map of currencies to accounts already exists
        if (_.keys(this.currenciesToAcountGuidsMap).length > 0)
        {
            return callback(null);
        }

        logger.trace('%s about to get map of IR currencies to accounts', functionName );

        self.client.getAccounts(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'mapCurrenciesToAccountGuids', self.mapCurrenciesToAccountGuids, callback);

            self.failures = 0;

            logger.trace('%s accounts returned:\n%s', functionName,
                JSON.stringify(json));

            json.forEach(function(account)
            {
                self.currenciesToAcountGuidsMap[account.CurrencyCode] = account.AccountGuid;
            });

            return callback(null);
        });
    }

    private getSingleCurrencyMovementsBetweenDates(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate: Date, accountGuid: string, type?: MovementType): void
    {
        const functionName = "IndependentReserve.getSingleCurrencyMovementsBetweenDates()",
            self = this,
            maxMovementsPerPage = 50,
            movements: Movement[] = [];

        logger.trace('%s about to get movements between %s (%s) and %s (%s) for account GUID %s', functionName,
            fromDate.toString(), fromDate.getTime(),
            toDate.toString(), fromDate.getTime(),
            accountGuid);

        const transactionTypes = convertMovementType(type);

        let lastTradeTimestamp: Date;

        let page = 1,
            movementsInPage: number;

        // loop through
        async.doUntil(
            function(callback)
            {
                logger.trace('%s about to get page %s of movements between %s (%s) and %s (%s) for account GUID %s', functionName,
                    page,
                    fromDate.toString(), fromDate.getTime(),
                    toDate.toString(), fromDate.getTime(),
                    accountGuid);

                self.client.getTransactions(accountGuid, fromDate, toDate, page++, maxMovementsPerPage, transactionTypes, function(err, json)
                {
                    if (err) return self.errorHandler(err, false, 'getSingleCurrencyMovementsBetweenDates', self.getSingleCurrencyMovementsBetweenDates, callback, fromDate, toDate, accountGuid, type);

                    self.failures = 0;

                    logger.trace('%s movements returned:\n%s', functionName,
                        JSON.stringify(json));

                    json.Data.forEach(function(movement, i)
                    {
                        lastTradeTimestamp = new Date(movement.SettleTimestampUtc);

                        // just get the movements in the specified date range
                        if (fromDate <= lastTradeTimestamp &&
                            lastTradeTimestamp <= toDate )
                        {
                            let amount: string;
                            if (movement.Debit != null)
                            {
                                amount = movement.Debit;
                            }
                            else if (movement.Credit != null)
                            {
                                amount = movement.Credit;
                            }

                            const newMovement = new Movement({
                                exchangeName: self.name,
                                currency: convertIRCurrencyCode(movement.CurrencyCode),
                                type: movement.Type.toLowerCase(),
                                amount: amount,
                                timestamp: lastTradeTimestamp
                            });

                            movements.push(newMovement);
                        }
                    });

                    movementsInPage = json.Data.length;

                    logger.debug('%s %s movements returned in the page', functionName,
                        movements.length);

                    callback(null);
                });
            },
            // do until this function returns true
            function()
            {
                // stop if the number of movements did not equal to the max number of movements in a page
                return movementsInPage != maxMovementsPerPage;
            },
            function(err)
            {
                callback(err, movements)
            }
        );
    }

    setTimeout(timeout: number)
    {
        this.client.timeout = timeout;
    }
};

function getIROrderType(orderType: string, side: string): string
{
    if (orderType == 'limit' && side == 'sell')
    {
        return 'LimitOffer';
    }
    else if (orderType == 'limit' && side == 'buy')
    {
        return 'LimitBid';
    }
    else if (orderType == 'market' && side == 'sell')
    {
        return 'MarketOffer';
    }
    else if (orderType == 'market' && side == 'buy')
    {
        return 'MarketBid';
    }
}

function convertMovementType(type): string[]
{
    if (!type)
    {
        return ['Deposit', 'Withdrawal'];
    }
    else if (type == MovementType.deposit)
    {
        return ['Deposit'];
    }
    else if (type == MovementType.withdrawal)
    {
        return ['Withdrawal'];
    }
    else
    {
        return [];
    }
}

function convertIRCurrencyCode(irCurrency): string
{
    if (irCurrency == 'Xbt')
    {
        return 'BTC';
    }

    return irCurrency.toUpperCase();
}

function getIRCurrencyCode(currency): string
{
    if (currency == 'BTC')
    {
        return 'Xbt';
    }

    return currency[0].toUpperCase() + currency.slice(1).toLocaleLowerCase();
}

function convertSymbol(primaryCurrencyCode, secondaryCurrencyCode)
{
    const functionName = 'convertSymbol';

    const fixedCurrency = "BTC",
        variableCurrency = secondaryCurrencyCode.toUpperCase();

    if (primaryCurrencyCode !== 'Xbt')
    {
        const error = new VError('%s primary currency code %s not equal to Xbt', functionName, primaryCurrencyCode);
        logger.error(error);
        return error;
    }

    return fixedCurrency.concat(variableCurrency);
}

function convertSide(side: string): string
{
    const functionName = 'IndependentReserve.convertSide()';

    let convertedSide: string;

    if (side === 'LimitBid' || side === 'MarketBid')
    {
        convertedSide = 'buy';
    }
    else if (side === 'LimitOffer' || side === 'MarketOffer')
    {
        convertedSide = 'sell';
    }
    else if (side === 'buy')
    {
        convertedSide = 'LimitBid';
    }
    else if (side === 'sell')
    {
        convertedSide = 'LimitOffer';
    }
    else
    {
        const error = new VError('%s side %s is not buy, sell, bid or ask', functionName, side);
        logger.error(error.stack);
        throw error;
    }

    logger.trace('%s side %s convert to %s', functionName, side, convertedSide);

    return convertedSide;
}