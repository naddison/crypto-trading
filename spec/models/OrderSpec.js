/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
// My packages
var order_1 = require("../../models/order");
var TestExchange_1 = require('../../exchanges/TestExchange');
var exchange_1 = require('../../models/exchange');
var market_1 = require('../../models/market');
var BigNumber = require('bignumber.js');
describe("Order: ", function () {
    var testExchange1, testExchange2, variableCurrencyFeeExchange, fixedCurrencyFeeExchange;
    beforeEach(function () {
        // Define test exchanges data rather than using live config that can change
        testExchange1 = new TestExchange_1.default({
            name: 'testExchange1',
            fixedCurrencies: ["LTC", "BTC"],
            variableCurrencies: ["AUD", "EUR", "USD"],
            commissions: { LTC: 0.1, BTC: 0.02, AUD: 0.01, USD: 0.06, EUR: 0.03 },
            defaultPriceRounding: 2,
            defaultMinAmount: 0.1,
            confirmationTime: 1,
            currencyRounding: { BTC: 6, LTC: 8, USD: 4, AUD: 4, EUR: 2 }
        });
        testExchange2 = new TestExchange_1.default({
            name: 'testExchange2',
            fixedCurrencies: ["BTC"],
            variableCurrencies: ["EUR", "USD"],
            commissions: { BTC: 0.01, AUD: 0.03, USD: 0.04 },
            defaultPriceRounding: 3,
            defaultMinAmount: 0.001,
            confirmationTime: 20,
            currencyRounding: { BTC: 4, USD: 3, EUR: 2 }
        });
        variableCurrencyFeeExchange = new TestExchange_1.default({
            name: 'variableCurrencyFeeExchange',
            fixedCurrencies: ["LTC", "BTC"],
            variableCurrencies: ["AUD", "EUR", "USD"],
            commissions: { LTC: 0.1, BTC: 0.02, AUD: 0.01, USD: 0.06, EUR: 0.03 },
            feeStructure: 'variableCurrency',
            defaultPriceRounding: 2,
            defaultMinAmount: 0.1,
            confirmationTime: 1,
            currencyRounding: { BTC: 6, LTC: 8, USD: 4, AUD: 4, EUR: 2 }
        });
        fixedCurrencyFeeExchange = new TestExchange_1.default({
            name: 'fixedCurrencyFeeExchange',
            fixedCurrencies: ["LTC", "BTC"],
            variableCurrencies: ["AUD", "EUR", "USD"],
            commissions: { LTC: 0.1, BTC: 0.02, AUD: 0.01, USD: 0.06, EUR: 0.03 },
            feeStructure: 'fixedCurrency',
            defaultPriceRounding: 2,
            defaultMinAmount: 0.1,
            confirmationTime: 1,
            currencyRounding: { BTC: 6, LTC: 8, USD: 4, AUD: 4, EUR: 2 }
        });
        // override the global exchanges object
        exchange_1.default.exchanges = {
            'testExchange1': testExchange1,
            'testExchange2': testExchange2,
            'variableCurrencyFeeExchange': variableCurrencyFeeExchange,
            'fixedCurrencyFeeExchange': fixedCurrencyFeeExchange };
    });
    //define test exchanges rather than using live 
    //var exchangesStub = {testExchange1: testExchange1, testExchange2: testExchange2};
    //var exchanges = proxyquire('../exchanges', {'../exchanges': exchangesStub });
    //console.log('In OrderSpec ' + exchanges[testExchange1]);
    //TODO need to get this working as Order is still using the read exchange
    describe("Instantiate", function () {
        describe("on an exchange with buyCurrency fee structure", function () {
            describe("a quoted buy order", function () {
                var testOrderOptions = {
                    exchangeId: '12345',
                    originalExchangeId: '6789',
                    exchangeName: 'testExchange1',
                    symbol: 'BTCAUD',
                    side: 'buy',
                    state: 'quoted',
                    amount: '0.1',
                    amountTraded: '0',
                    amountRemaining: '0.1',
                    amountLastPartial: '0',
                    numberPartialFills: 0,
                    price: '1000'
                };
                it("with invalid exchangeName", function () {
                    testOrderOptions.exchangeName = "INVALID";
                    var testOrder = new order_1.default(testOrderOptions);
                    expect(testOrder.exchangeId).toEqual('12345');
                    expect(testOrder.exchangeName).toEqual('INVALID');
                });
                it("with invalid type", function () {
                    testOrderOptions.side = "invalid";
                    var testOrder = new order_1.default(testOrderOptions);
                    expect(testOrder.exchangeId).toEqual('12345');
                    expect(testOrder.side).toEqual('invalid');
                });
            });
            describe("a quoted buy order", function () {
                var testOrder = new order_1.default({
                    exchangeId: '12345',
                    originalExchangeId: '6789',
                    exchangeName: 'testExchange1',
                    symbol: 'BTCAUD',
                    side: 'buy',
                    state: 'quoted',
                    amount: '0.1',
                    amountTraded: '0',
                    amountRemaining: '0.1',
                    amountLastPartial: '0',
                    numberPartialFills: 0,
                    price: '1000'
                });
                it("all properties are set", function () {
                    expect(testOrder.exchangeId).toEqual('12345');
                    expect(testOrder.originalExchangeId).toEqual('6789');
                    expect(testOrder.exchangeName).toEqual('testExchange1');
                    expect(testOrder.symbol).toEqual('BTCAUD');
                    expect(testOrder.side).toEqual('buy');
                    expect(testOrder.state).toEqual('quoted');
                    expect(testOrder.amount).toEqual('0.1');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountRemaining).toEqual('0.1');
                    expect(testOrder.amountLastPartial).toEqual('0');
                    expect(testOrder.numberPartialFills).toEqual(0);
                    expect(testOrder.price).toEqual('1000');
                });
                it("exchange virtual properties are set", function () {
                    expect(testOrder.exchange).toBeDefined();
                    expect(testOrder.exchange instanceof exchange_1.default).toBe(true, "of type Exchange");
                    expect(testOrder.exchange.name).toEqual('testExchange1');
                });
                it("market virtual properties are set", function () {
                    expect(testOrder.market).toBeDefined();
                    expect(testOrder.market instanceof market_1.default).toBe(true, "of type Market");
                    expect(testOrder.market.symbol).toEqual('BTCAUD');
                });
                it("amount ratios are set", function () {
                    expect(testOrder.tradedRatioBN).toEqual(BigNumber(0));
                    expect(testOrder.remainingRatioBN).toEqual(BigNumber(1));
                    expect(testOrder.lastPartialRatioBN).toEqual(BigNumber(0));
                });
                it("currency virtual properties are set", function () {
                    expect(testOrder.fixedCurrency).toEqual('BTC');
                    expect(testOrder.variableCurrency).toEqual('AUD');
                });
                it("buy virtual properties are set", function () {
                    expect(testOrder.buy.currency).toEqual('BTC');
                    // 0.1 * (1 - 0.002)
                    expect(testOrder.buy.amount).toEqual('0.098');
                    // 0.05 * (1 - 0.002)
                    expect(testOrder.buy.amountRemaining).toEqual('0.098');
                    // 0.95 * (1 - 0.002)
                    expect(testOrder.buy.amountTraded).toEqual('0');
                    expect(testOrder.buy.amountLastPartial).toEqual('0');
                });
                it("gross buy virtual properties are set", function () {
                    expect(testOrder.buy.amountGross).toEqual('0.1');
                    expect(testOrder.buy.amountRemainingGross).toEqual('0.1');
                    expect(testOrder.buy.amountTradedGross).toEqual('0');
                    expect(testOrder.buy.amountLastPartialGross).toEqual('0');
                });
                it("sell virtual properties are set", function () {
                    expect(testOrder.sell.currency).toEqual('AUD');
                    expect(testOrder.sell.amount).toEqual('100');
                    expect(testOrder.sell.amountRemaining).toEqual('100');
                    expect(testOrder.sell.amountTraded).toEqual('0');
                    expect(testOrder.sell.amountLastPartial).toEqual('0');
                });
                it("fee virtual properties are set", function () {
                    expect(testOrder.fee.currency).toEqual('BTC');
                    // 0.1 * (1- 0.002)
                    expect(testOrder.fee.amount).toEqual('0.002');
                    // 0.5 * (1- 0.002)
                    expect(testOrder.fee.amountRemaining).toEqual('0.002');
                    expect(testOrder.fee.amountTraded).toEqual('0');
                    expect(testOrder.fee.amountLastPartial).toEqual('0');
                });
                it("fixed currency virtual properties are set", function () {
                    // 0.1 * (1- 0.002)
                    expect(testOrder.fixedAmount).toEqual('0.098');
                    // 0.5 * (1- 0.002)
                    expect(testOrder.fixedAmountRemaining).toEqual('0.098');
                    expect(testOrder.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fixedAmountLastPartial).toEqual('0');
                    expect(testOrder.fixedAmountGross).toEqual('0.1');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('0.1');
                    expect(testOrder.fixedAmountTradedGross).toEqual('0');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('0');
                    expect(testOrder.fee.fixedAmount).toEqual('0.002');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0.002');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0');
                });
                it("variable currency virtual properties are set", function () {
                    expect(testOrder.variableAmount).toEqual('100');
                    expect(testOrder.variableAmountRemaining).toEqual('100');
                    expect(testOrder.variableAmountTraded).toEqual('0');
                    expect(testOrder.variableAmountLastPartial).toEqual('0');
                    expect(testOrder.variableAmountGross).toEqual('100');
                    expect(testOrder.variableAmountRemainingGross).toEqual('100');
                    expect(testOrder.variableAmountTradedGross).toEqual('0');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('0');
                    expect(testOrder.fee.variableAmount).toEqual('0');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('0');
                    expect(testOrder.fee.variableAmountTraded).toEqual('0');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('0');
                });
            });
            describe("a pending sell order", function () {
                var testOrder = new order_1.default({ exchangeId: '12346',
                    exchangeName: 'testExchange1',
                    symbol: 'BTCAUD',
                    side: 'sell',
                    state: 'pending',
                    amount: '0.2',
                    //amountTraded: '0',
                    amountRemaining: '0.1',
                    //amountLastPartial: '0',
                    //numberPartialFills: 0,
                    price: '1000'
                });
                it("properties are set", function () {
                    expect(testOrder.exchangeId).toEqual('12346');
                    expect(testOrder.exchangeName).toEqual('testExchange1');
                    expect(testOrder.symbol).toEqual('BTCAUD');
                    expect(testOrder.side).toEqual('sell');
                    expect(testOrder.state).toEqual('pending');
                    expect(testOrder.amount).toEqual('0.2');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountRemaining).toEqual('0.1');
                    expect(testOrder.amountLastPartial).toEqual('0');
                    expect(testOrder.numberPartialFills).toEqual(0);
                    expect(testOrder.price).toEqual('1000');
                });
                it("buy virtual properties is set", function () {
                    expect(testOrder.buy.currency).toEqual('AUD');
                    // 1000 * 0.2 * (1 - 0.01)
                    expect(testOrder.buy.amount).toEqual('198');
                    // 1000 * 0.1 * (1 - 0.01)
                    expect(testOrder.buy.amountRemaining).toEqual('99');
                    expect(testOrder.buy.amountTraded).toEqual('0');
                    expect(testOrder.buy.amountLastPartial).toEqual('0');
                });
                it("gross buy virtual properties are set", function () {
                    expect(testOrder.buy.amountGross).toEqual('200');
                    expect(testOrder.buy.amountRemainingGross).toEqual('100');
                    expect(testOrder.buy.amountTradedGross).toEqual('0');
                    expect(testOrder.buy.amountLastPartialGross).toEqual('0');
                });
                it("sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('BTC');
                    expect(testOrder.sell.amount).toEqual('0.2');
                    expect(testOrder.sell.amountRemaining).toEqual('0.1');
                    expect(testOrder.sell.amountTraded).toEqual('0');
                    expect(testOrder.sell.amountLastPartial).toEqual('0');
                });
                it("fee virtual properties is set", function () {
                    expect(testOrder.fee.currency).toEqual('AUD');
                    expect(testOrder.fee.amount).toEqual('2');
                    expect(testOrder.fee.amountRemaining).toEqual('1');
                    expect(testOrder.fee.amountTraded).toEqual('0');
                    expect(testOrder.fee.amountLastPartial).toEqual('0');
                });
                it("fixed currency virtual properties are set", function () {
                    expect(testOrder.fixedAmount).toEqual('0.2');
                    expect(testOrder.fixedAmountRemaining).toEqual('0.1');
                    expect(testOrder.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fixedAmountLastPartial).toEqual('0');
                    expect(testOrder.fixedAmountGross).toEqual('0.2');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('0.1');
                    expect(testOrder.fixedAmountTradedGross).toEqual('0');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('0');
                    expect(testOrder.fee.fixedAmount).toEqual('0');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0');
                });
                it("variable currency virtual properties are set", function () {
                    expect(testOrder.variableAmount).toEqual('198');
                    expect(testOrder.variableAmountRemaining).toEqual('99');
                    expect(testOrder.variableAmountTraded).toEqual('0');
                    expect(testOrder.variableAmountLastPartial).toEqual('0');
                    expect(testOrder.variableAmountGross).toEqual('200');
                    expect(testOrder.variableAmountRemainingGross).toEqual('100');
                    expect(testOrder.variableAmountTradedGross).toEqual('0');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('0');
                    expect(testOrder.fee.variableAmount).toEqual('2');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('1');
                    expect(testOrder.fee.variableAmountTraded).toEqual('0');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('0');
                });
            });
            describe("a partially filled buy trade", function () {
                var testOrder = new order_1.default({
                    exchangeId: '12346',
                    exchangeName: 'testExchange1',
                    symbol: 'BTCAUD',
                    side: 'buy',
                    state: 'partiallyFilled',
                    amount: '100',
                    amountTraded: '80',
                    amountRemaining: '20',
                    amountLastPartial: '30',
                    numberPartialFills: 4,
                    price: '500'
                });
                it("properties are set", function () {
                    expect(testOrder.side).toEqual('buy');
                    expect(testOrder.state).toEqual('partiallyFilled');
                    expect(testOrder.amount).toEqual('100');
                    expect(testOrder.amountTraded).toEqual('80');
                    expect(testOrder.amountRemaining).toEqual('20');
                    expect(testOrder.amountLastPartial).toEqual('30');
                    expect(testOrder.numberPartialFills).toEqual(4);
                    expect(testOrder.price).toEqual('500');
                });
                it("properties are set", function () {
                    expect(testOrder.tradedRatioBN).toEqual(BigNumber(0.8));
                    expect(testOrder.remainingRatioBN).toEqual(BigNumber(0.2));
                    expect(testOrder.lastPartialRatioBN).toEqual(BigNumber(0.3));
                });
                it("buy virtual properties is set", function () {
                    expect(testOrder.buy.currency).toEqual('BTC');
                    // 1 * (1 - 0.02)
                    expect(testOrder.buy.amount).toEqual('98');
                    // 0.8 * (1 - 0.02)
                    expect(testOrder.buy.amountTraded).toEqual('78.4');
                    // 0.2 * (1 - 0.02)
                    expect(testOrder.buy.amountRemaining).toEqual('19.6');
                    // 0.3 * (1 - 0.02)
                    expect(testOrder.buy.amountLastPartial).toEqual('29.4');
                });
                it("gross buy virtual properties are set", function () {
                    expect(testOrder.buy.amountGross).toEqual('100');
                    expect(testOrder.buy.amountTradedGross).toEqual('80');
                    expect(testOrder.buy.amountRemainingGross).toEqual('20');
                    expect(testOrder.buy.amountLastPartialGross).toEqual('30');
                });
                it("sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('AUD');
                    // 100 * 500
                    expect(testOrder.sell.amount).toEqual('50000');
                    // 80 * 500
                    expect(testOrder.sell.amountTraded).toEqual('40000');
                    // 20 * 500
                    expect(testOrder.sell.amountRemaining).toEqual('10000');
                    // 30 * 500
                    expect(testOrder.sell.amountLastPartial).toEqual('15000');
                });
                it("gross sell virtual properties is set", function () {
                    // are the same as the net sell amount as the fee is in the buy currency
                    expect(testOrder.sell.amountGross).toEqual('50000');
                    expect(testOrder.sell.amountTradedGross).toEqual('40000');
                    expect(testOrder.sell.amountRemainingGross).toEqual('10000');
                    expect(testOrder.sell.amountLastPartialGross).toEqual('15000');
                });
                it("fee virtual properties is set", function () {
                    expect(testOrder.fee.currency).toEqual('BTC');
                    // 100 * 0.002
                    expect(testOrder.fee.amount).toEqual('2');
                    expect(testOrder.fee.amountTraded).toEqual('1.6');
                    expect(testOrder.fee.amountRemaining).toEqual('0.4');
                    expect(testOrder.fee.amountLastPartial).toEqual('0.6');
                });
                it("fixed currency virtual properties are set", function () {
                    expect(testOrder.fixedAmount).toEqual('98');
                    expect(testOrder.fixedAmountTraded).toEqual('78.4');
                    expect(testOrder.fixedAmountRemaining).toEqual('19.6');
                    expect(testOrder.fixedAmountLastPartial).toEqual('29.4');
                    expect(testOrder.fixedAmountGross).toEqual('100');
                    expect(testOrder.fixedAmountTradedGross).toEqual('80');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('20');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('30');
                    expect(testOrder.fee.fixedAmount).toEqual('2');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('1.6');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0.4');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0.6');
                });
                it("variable currency virtual properties are set", function () {
                    // 100 * 500
                    expect(testOrder.variableAmount).toEqual('50000');
                    expect(testOrder.variableAmountTraded).toEqual('40000');
                    expect(testOrder.variableAmountRemaining).toEqual('10000');
                    expect(testOrder.variableAmountLastPartial).toEqual('15000');
                    expect(testOrder.variableAmountGross).toEqual('50000');
                    expect(testOrder.variableAmountTradedGross).toEqual('40000');
                    expect(testOrder.variableAmountRemainingGross).toEqual('10000');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('15000');
                    // 2 / 500
                    expect(testOrder.fee.variableAmount).toEqual('0');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('0');
                    expect(testOrder.fee.variableAmountTraded).toEqual('0');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('0');
                });
            });
            describe("a new filled sell trade", function () {
                var testOrder = new order_1.default({ exchangeId: '444',
                    exchangeName: 'testExchange1',
                    symbol: 'BTCEUR',
                    side: 'sell',
                    state: 'filled',
                    amount: '2.1',
                    amountTraded: '2.1',
                    amountRemaining: '0',
                    amountLastPartial: '0.3',
                    numberPartialFills: 5,
                    price: '1000'
                });
                it("properties are set", function () {
                    expect(testOrder.exchangeId).toEqual('444');
                    expect(testOrder.exchangeName).toEqual('testExchange1');
                    expect(testOrder.symbol).toEqual('BTCEUR');
                    expect(testOrder.side).toEqual('sell');
                    expect(testOrder.state).toEqual('filled');
                    expect(testOrder.amount).toEqual('2.1');
                    expect(testOrder.amountTraded).toEqual('2.1');
                    expect(testOrder.amountRemaining).toEqual('0');
                    expect(testOrder.amountLastPartial).toEqual('0.3');
                    expect(testOrder.numberPartialFills).toEqual(5);
                    expect(testOrder.price).toEqual('1000');
                });
                it("buy virtual properties is set", function () {
                    expect(testOrder.buy.currency).toEqual('EUR');
                    // 1000 * 2.1 * (1 - 0.03)
                    expect(testOrder.buy.amount).toEqual('2037');
                    expect(testOrder.buy.amountRemaining).toEqual('0');
                    expect(testOrder.buy.amountTraded).toEqual('2037');
                    // 1000 * 0.3 * (1 - 0.03)
                    expect(testOrder.buy.amountLastPartial).toEqual('291');
                });
                it("gross buy virtual properties are set", function () {
                    // 1000 * 2.1
                    expect(testOrder.buy.amountGross).toEqual('2100');
                    expect(testOrder.buy.amountRemainingGross).toEqual('0');
                    expect(testOrder.buy.amountTradedGross).toEqual('2100');
                    // 1000 * 0.3
                    expect(testOrder.buy.amountLastPartialGross).toEqual('300');
                });
                it("sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('BTC');
                    expect(testOrder.sell.amount).toEqual('2.1');
                    expect(testOrder.sell.amountRemaining).toEqual('0');
                    expect(testOrder.sell.amountTraded).toEqual('2.1');
                    expect(testOrder.sell.amountLastPartial).toEqual('0.3');
                });
                it("gross sell virtual properties is set", function () {
                    expect(testOrder.sell.amountGross).toEqual('2.1');
                    expect(testOrder.sell.amountRemainingGross).toEqual('0');
                    expect(testOrder.sell.amountTradedGross).toEqual('2.1');
                    expect(testOrder.sell.amountLastPartialGross).toEqual('0.3');
                });
                it("fee virtual properties is set", function () {
                    expect(testOrder.fee.currency).toEqual('EUR');
                    // 1000 * 2.1 * 0.03)
                    expect(testOrder.fee.amount).toEqual('63');
                    expect(testOrder.fee.amountRemaining).toEqual('0');
                    expect(testOrder.fee.amountTraded).toEqual('63');
                    // 1000 * 0.3 * 0.03)
                    expect(testOrder.fee.amountLastPartial).toEqual('9');
                });
                it("fixed currency virtual properties are set", function () {
                    expect(testOrder.fixedAmount).toEqual('2.1');
                    expect(testOrder.fixedAmountRemaining).toEqual('0');
                    expect(testOrder.fixedAmountTraded).toEqual('2.1');
                    expect(testOrder.fixedAmountLastPartial).toEqual('0.3');
                    expect(testOrder.fixedAmountGross).toEqual('2.1');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('0');
                    expect(testOrder.fixedAmountTradedGross).toEqual('2.1');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('0.3');
                    expect(testOrder.fee.fixedAmount).toEqual('0');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0');
                });
                it("variable currency virtual properties are set", function () {
                    expect(testOrder.variableAmount).toEqual('2037');
                    expect(testOrder.variableAmountRemaining).toEqual('0');
                    expect(testOrder.variableAmountTraded).toEqual('2037');
                    expect(testOrder.variableAmountLastPartial).toEqual('291');
                    expect(testOrder.variableAmountGross).toEqual('2100');
                    expect(testOrder.variableAmountRemainingGross).toEqual('0');
                    expect(testOrder.variableAmountTradedGross).toEqual('2100');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('300');
                    expect(testOrder.fee.variableAmount).toEqual('63');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('0');
                    expect(testOrder.fee.variableAmountTraded).toEqual('63');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('9');
                });
            });
            describe("a sell order with only mandatory fields set of the defaults are used", function () {
                var testOrder = new order_1.default({ exchangeName: 'testExchange1',
                    symbol: 'BTCAUD',
                    side: 'sell',
                    amount: '0.1',
                    price: '1000'
                });
                it("default values are set", function () {
                    expect(testOrder.state).toEqual('quoted');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountLastPartial).toEqual('0');
                    expect(testOrder.minRate).toEqual('0');
                    expect(testOrder.maxRate).toEqual('Infinity');
                });
                it("timestamp should be set", function () {
                    expect(testOrder.timestamp).not.toBeNull();
                    expect(testOrder.timestamp instanceof Date).toBeTruthy();
                    // the following assumes the save time and the validation time will be done in the same minute. There's a small chance this may not happen
                    expect(testOrder.timestamp.getMinutes()).toEqual(new Date().getMinutes());
                    expect(testOrder.timestamp.getSeconds()).toEqual(new Date().getSeconds());
                });
            });
            describe("using net sell amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'testExchange1',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetSellAmount(BigNumber(800));
                    expect(testOrder.sell.amount).toEqual('800');
                    expect(testOrder.amount).toEqual('0.8');
                    expect(testOrder.amountRemaining).toEqual('0.8');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountLastPartial).toEqual('0');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'testExchange1',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetSellAmount(BigNumber(1.2));
                    expect(testOrder.sell.amount).toEqual('1.2');
                    expect(testOrder.amount).toEqual('1.2');
                    expect(testOrder.amountRemaining).toEqual('1.2');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountLastPartial).toEqual('0');
                });
            });
            describe("using net buy amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'testExchange1',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetBuyAmount(BigNumber(1));
                    // TODO should be 1 but with rounding comes out at 0.999999
                    expect(testOrder.buy.amount).toEqual('0.999999');
                    // 1 / (1 - 0.02) = 1.02040816326531
                    expect(testOrder.amount).toEqual('1.020408');
                    expect(testOrder.amountRemaining).toEqual('1.020408');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'testExchange1',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetBuyAmount(BigNumber(700));
                    // TODO should be 700 but with rounding comes out at 700.0002
                    expect(testOrder.buy.amount).toEqual('700.0002');
                    // 700 / (1000 * (1 - 0.01)) = 0.707070707070707
                    expect(testOrder.amount).toEqual('0.707071');
                    expect(testOrder.amountRemaining).toEqual('0.707071');
                });
            });
            describe("using net fixed amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'testExchange1',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetFixedAmount(BigNumber(1));
                    // 1 / (1 - 0.02) = 1.02040816326531
                    expect(testOrder.amount).toEqual('1.020408');
                    expect(testOrder.amountRemaining).toEqual('1.020408');
                    // 1.020408 * (1 - 0.02) = 0.99999984 rounded down
                    expect(testOrder.fixedAmount).toEqual('0.999999');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'testExchange1',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetFixedAmount(BigNumber(1));
                    expect(testOrder.amount).toEqual('1');
                    expect(testOrder.amountRemaining).toEqual('1');
                    expect(testOrder.fixedAmount).toEqual('1');
                });
            });
        });
        describe("on an exchange with a variableFee structure", function () {
            describe("a quoted buy order", function () {
                var testOrder = new order_1.default({ exchangeId: '12345',
                    exchangeName: 'variableCurrencyFeeExchange',
                    symbol: 'BTCAUD',
                    side: 'buy',
                    state: 'quoted',
                    amount: '0.1',
                    amountRemaining: '0.1',
                    price: '1000'
                });
                it("all properties are set", function () {
                    expect(testOrder.exchangeId).toEqual('12345');
                    expect(testOrder.exchangeName).toEqual('variableCurrencyFeeExchange');
                    expect(testOrder.symbol).toEqual('BTCAUD');
                    expect(testOrder.side).toEqual('buy');
                    expect(testOrder.state).toEqual('quoted');
                    expect(testOrder.amount).toEqual('0.1');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountRemaining).toEqual('0.1');
                    expect(testOrder.amountLastPartial).toEqual('0');
                    expect(testOrder.numberPartialFills).toEqual(0);
                    expect(testOrder.price).toEqual('1000');
                });
                it("exchange virtual properties are set", function () {
                    expect(testOrder.exchange).toBeDefined();
                    expect(testOrder.exchange.name).toEqual('variableCurrencyFeeExchange');
                });
                it("market virtual properties are set", function () {
                    expect(testOrder.market).toBeDefined();
                    expect(testOrder.market.symbol).toEqual('BTCAUD');
                });
                it("amount ratios are set", function () {
                    expect(testOrder.tradedRatioBN).toEqual(BigNumber(0));
                    expect(testOrder.remainingRatioBN).toEqual(BigNumber(1));
                    expect(testOrder.lastPartialRatioBN).toEqual(BigNumber(0));
                });
                it("currency virtual properties are set", function () {
                    expect(testOrder.fixedCurrency).toEqual('BTC');
                    expect(testOrder.variableCurrency).toEqual('AUD');
                });
                it("buy virtual properties are set", function () {
                    expect(testOrder.buy.currency).toEqual('BTC');
                    // 0.1 * (1 - 0.002)
                    expect(testOrder.buy.amount).toEqual('0.1');
                    // 0.05 * (1 - 0.002)
                    expect(testOrder.buy.amountRemaining).toEqual('0.1');
                    // 0.95 * (1 - 0.002)
                    expect(testOrder.buy.amountTraded).toEqual('0');
                    expect(testOrder.buy.amountLastPartial).toEqual('0');
                });
                it("gross buy virtual properties are set", function () {
                    expect(testOrder.buy.amountGross).toEqual('0.1');
                    expect(testOrder.buy.amountRemainingGross).toEqual('0.1');
                    expect(testOrder.buy.amountTradedGross).toEqual('0');
                    expect(testOrder.buy.amountLastPartialGross).toEqual('0');
                });
                it("sell virtual properties are set", function () {
                    expect(testOrder.sell.currency).toEqual('AUD');
                    expect(testOrder.sell.amount).toEqual('101');
                    expect(testOrder.sell.amountRemaining).toEqual('101');
                    expect(testOrder.sell.amountTraded).toEqual('0');
                    expect(testOrder.sell.amountLastPartial).toEqual('0');
                });
                it("gross sell virtual properties are set", function () {
                    expect(testOrder.sell.currency).toEqual('AUD');
                    expect(testOrder.sell.amountGross).toEqual('100');
                    expect(testOrder.sell.amountRemainingGross).toEqual('100');
                    expect(testOrder.sell.amountTradedGross).toEqual('0');
                    expect(testOrder.sell.amountLastPartialGross).toEqual('0');
                });
                it("fee virtual properties are set", function () {
                    expect(testOrder.fee.currency).toEqual('AUD');
                    // 0.1 * (1- 0.002)
                    expect(testOrder.fee.amount).toEqual('1');
                    // 0.5 * (1- 0.002)
                    expect(testOrder.fee.amountRemaining).toEqual('1');
                    expect(testOrder.fee.amountTraded).toEqual('0');
                    expect(testOrder.fee.amountLastPartial).toEqual('0');
                });
                it("fixed currency virtual properties are set", function () {
                    // 0.1 * (1- 0.002)
                    expect(testOrder.fixedAmount).toEqual('0.1');
                    // 0.5 * (1- 0.002)
                    expect(testOrder.fixedAmountRemaining).toEqual('0.1');
                    expect(testOrder.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fixedAmountLastPartial).toEqual('0');
                    expect(testOrder.fixedAmountGross).toEqual('0.1');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('0.1');
                    expect(testOrder.fixedAmountTradedGross).toEqual('0');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('0');
                    expect(testOrder.fee.fixedAmount).toEqual('0');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0');
                });
                it("variable currency virtual properties are set", function () {
                    expect(testOrder.variableAmount).toEqual('101');
                    expect(testOrder.variableAmountRemaining).toEqual('101');
                    expect(testOrder.variableAmountTraded).toEqual('0');
                    expect(testOrder.variableAmountLastPartial).toEqual('0');
                    expect(testOrder.variableAmountGross).toEqual('100');
                    expect(testOrder.variableAmountRemainingGross).toEqual('100');
                    expect(testOrder.variableAmountTradedGross).toEqual('0');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('0');
                    expect(testOrder.fee.variableAmount).toEqual('1');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('1');
                    expect(testOrder.fee.variableAmountTraded).toEqual('0');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('0');
                });
            });
            describe("a partially filled buy trade", function () {
                var testOrder = new order_1.default({
                    exchangeId: '12346',
                    exchangeName: 'variableCurrencyFeeExchange',
                    symbol: 'BTCAUD',
                    side: 'buy',
                    state: 'partiallyFilled',
                    amount: '100',
                    amountTraded: '80',
                    amountRemaining: '20',
                    amountLastPartial: '30',
                    numberPartialFills: 4,
                    price: '500'
                });
                it("properties are set", function () {
                    expect(testOrder.side).toEqual('buy');
                    expect(testOrder.state).toEqual('partiallyFilled');
                    expect(testOrder.amount).toEqual('100');
                    expect(testOrder.amountTraded).toEqual('80');
                    expect(testOrder.amountRemaining).toEqual('20');
                    expect(testOrder.amountLastPartial).toEqual('30');
                    expect(testOrder.numberPartialFills).toEqual(4);
                    expect(testOrder.price).toEqual('500');
                });
                it("properties are set", function () {
                    expect(testOrder.tradedRatioBN).toEqual(BigNumber(0.8));
                    expect(testOrder.remainingRatioBN).toEqual(BigNumber(0.2));
                    expect(testOrder.lastPartialRatioBN).toEqual(BigNumber(0.3));
                });
                it("buy virtual properties is set", function () {
                    expect(testOrder.buy.currency).toEqual('BTC');
                    expect(testOrder.buy.amount).toEqual('100');
                    expect(testOrder.buy.amountTraded).toEqual('80');
                    expect(testOrder.buy.amountRemaining).toEqual('20');
                    expect(testOrder.buy.amountLastPartial).toEqual('30');
                });
                it("gross buy virtual properties are set", function () {
                    expect(testOrder.buy.amountGross).toEqual('100');
                    expect(testOrder.buy.amountTradedGross).toEqual('80');
                    expect(testOrder.buy.amountRemainingGross).toEqual('20');
                    expect(testOrder.buy.amountLastPartialGross).toEqual('30');
                });
                it("sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('AUD');
                    // 100 * 500 * (1 + 0.01)
                    expect(testOrder.sell.amount).toEqual('50500');
                    // 80 * 500 * (1 + 0.01)
                    expect(testOrder.sell.amountTraded).toEqual('40400');
                    // 20 * 500 * (1 + 0.01)
                    expect(testOrder.sell.amountRemaining).toEqual('10100');
                    // 30 * 500 * (1 + 0.01)
                    expect(testOrder.sell.amountLastPartial).toEqual('15150');
                });
                it("gross sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('AUD');
                    // 100 * 500
                    expect(testOrder.sell.amountGross).toEqual('50000');
                    // 80 * 500
                    expect(testOrder.sell.amountTradedGross).toEqual('40000');
                    // 20 * 500
                    expect(testOrder.sell.amountRemainingGross).toEqual('10000');
                    // 30 * 500
                    expect(testOrder.sell.amountLastPartialGross).toEqual('15000');
                });
                it("fee virtual properties is set", function () {
                    expect(testOrder.fee.currency).toEqual('AUD');
                    // 100 * 500 * 0.01
                    expect(testOrder.fee.amount).toEqual('500');
                    expect(testOrder.fee.amountTraded).toEqual('400');
                    expect(testOrder.fee.amountRemaining).toEqual('100');
                    expect(testOrder.fee.amountLastPartial).toEqual('150');
                });
                it("fixed currency virtual properties are set", function () {
                    expect(testOrder.fixedAmount).toEqual('100');
                    expect(testOrder.fixedAmountTraded).toEqual('80');
                    expect(testOrder.fixedAmountRemaining).toEqual('20');
                    expect(testOrder.fixedAmountLastPartial).toEqual('30');
                    expect(testOrder.fixedAmountGross).toEqual('100');
                    expect(testOrder.fixedAmountTradedGross).toEqual('80');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('20');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('30');
                    expect(testOrder.fee.fixedAmount).toEqual('0');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0');
                });
                it("variable currency virtual properties are set", function () {
                    // 100 * 500
                    expect(testOrder.variableAmount).toEqual('50500');
                    expect(testOrder.variableAmountTraded).toEqual('40400');
                    expect(testOrder.variableAmountRemaining).toEqual('10100');
                    expect(testOrder.variableAmountLastPartial).toEqual('15150');
                    expect(testOrder.variableAmountGross).toEqual('50000');
                    expect(testOrder.variableAmountTradedGross).toEqual('40000');
                    expect(testOrder.variableAmountRemainingGross).toEqual('10000');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('15000');
                    // 2 / 500
                    expect(testOrder.fee.variableAmount).toEqual('500');
                    expect(testOrder.fee.variableAmountTraded).toEqual('400');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('100');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('150');
                });
            });
            describe("a partially filled sell trade", function () {
                var testOrder = new order_1.default({
                    exchangeId: '12346',
                    exchangeName: 'variableCurrencyFeeExchange',
                    symbol: 'BTCAUD',
                    side: 'sell',
                    state: 'partiallyFilled',
                    amount: '10.123456',
                    amountTraded: '5.555555',
                    amountRemaining: '4.567901',
                    amountLastPartial: '1.987654',
                    numberPartialFills: 5,
                    price: '444.44'
                });
                it("properties are set", function () {
                    expect(testOrder.side).toEqual('sell');
                    expect(testOrder.state).toEqual('partiallyFilled');
                    expect(testOrder.amount).toEqual('10.123456');
                    expect(testOrder.amountTraded).toEqual('5.555555');
                    expect(testOrder.amountRemaining).toEqual('4.567901');
                    expect(testOrder.amountLastPartial).toEqual('1.987654');
                    expect(testOrder.numberPartialFills).toEqual(5);
                    expect(testOrder.price).toEqual('444.44');
                });
                it("properties are set", function () {
                    // 5.555555 / 10.123456 = 0.548780475758476 = 0.548 ROUND_HALF_UP
                    expect(testOrder.tradedRatioBN.round(10)).toEqual(BigNumber(0.5487804758));
                    expect(testOrder.tradedPercentage).toEqual('54.878');
                    // 4.567901 / 10.123456 = 0.451219524241524
                    expect(testOrder.remainingRatioBN.round(10)).toEqual(BigNumber(0.45121952424).round(10));
                    expect(testOrder.remainingPercentage).toEqual('45.122');
                    // 1.987654 / 10.123456 = 0.196341447031528
                    expect(testOrder.lastPartialRatioBN.round(10)).toEqual(BigNumber(0.1963414470).round(10));
                    expect(testOrder.lastPartialPercentage).toEqual('19.634');
                });
                it("buy virtual properties is set", function () {
                    expect(testOrder.buy.currency).toEqual('AUD');
                    // all rounding is ROUND_DOWN
                    // 10.123456 * 444.44 * (1 - 0.01) = 4454.2760967936 = 4454.2760
                    expect(testOrder.buy.amount).toEqual('4454.276');
                    // 5.555555 * 444.44 * (1 - 0.01) = 2444.419755558
                    expect(testOrder.buy.amountTraded).toEqual('2444.4197');
                    // 4.567901 * 444.44 * (1 - 0.01) = 2009.8563412356
                    expect(testOrder.buy.amountRemaining).toEqual('2009.8563');
                    // 1.987654 * 444.44 * (1 - 0.01) = 874.5590143224
                    expect(testOrder.buy.amountLastPartial).toEqual('874.559');
                });
                it("gross buy virtual properties are set", function () {
                    // all rounding is ROUND_DOWN
                    // 10.123456 * 444.44 = 4499.26878464
                    expect(testOrder.buy.amountGross).toEqual('4499.2687');
                    // 5.555555 * 444.44 = 2469.1108642
                    expect(testOrder.buy.amountTradedGross).toEqual('2469.1108');
                    // 4.567901 * 444.44 = 2030.15792044
                    expect(testOrder.buy.amountRemainingGross).toEqual('2030.1579');
                    // 1.987654 * 444.44 = 883.39294376
                    expect(testOrder.buy.amountLastPartialGross).toEqual('883.3929');
                });
                it("sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('BTC');
                    expect(testOrder.sell.amount).toEqual('10.123456');
                    expect(testOrder.sell.amountTraded).toEqual('5.555555');
                    expect(testOrder.sell.amountRemaining).toEqual('4.567901');
                    expect(testOrder.sell.amountLastPartial).toEqual('1.987654');
                });
                it("sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('BTC');
                    expect(testOrder.sell.amountGross).toEqual('10.123456');
                    expect(testOrder.sell.amountTradedGross).toEqual('5.555555');
                    expect(testOrder.sell.amountRemainingGross).toEqual('4.567901');
                    expect(testOrder.sell.amountLastPartialGross).toEqual('1.987654');
                });
                it("fee virtual properties is set", function () {
                    expect(testOrder.fee.currency).toEqual('AUD');
                    // 10.123456 * 444.44 * 0.01 = 44.9926878464
                    expect(testOrder.fee.amount).toEqual('44.9926');
                    // 5.555555 * 444.44 * 0.01 = 24.691108642
                    expect(testOrder.fee.amountTraded).toEqual('24.6911');
                    // 4.567901 * 444.44 * 0.01 = 20.3015
                    expect(testOrder.fee.amountRemaining).toEqual('20.3015');
                    // 1.987654 * 444.44 * 0.01 = 8.8339
                    expect(testOrder.fee.amountLastPartial).toEqual('8.8339');
                });
                it("fixed currency virtual properties are set", function () {
                    expect(testOrder.fixedAmount).toEqual('10.123456');
                    expect(testOrder.fixedAmountTraded).toEqual('5.555555');
                    expect(testOrder.fixedAmountRemaining).toEqual('4.567901');
                    expect(testOrder.fixedAmountLastPartial).toEqual('1.987654');
                    expect(testOrder.fixedAmountGross).toEqual('10.123456');
                    expect(testOrder.fixedAmountTradedGross).toEqual('5.555555');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('4.567901');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('1.987654');
                    expect(testOrder.fee.fixedAmount).toEqual('0');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('0');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0');
                });
                it("variable currency virtual properties are set", function () {
                    // 100 * 500
                    expect(testOrder.variableAmount).toEqual('4454.276');
                    expect(testOrder.variableAmountTraded).toEqual('2444.4197');
                    expect(testOrder.variableAmountRemaining).toEqual('2009.8563');
                    expect(testOrder.variableAmountLastPartial).toEqual('874.559');
                    expect(testOrder.variableAmountGross).toEqual('4499.2687');
                    expect(testOrder.variableAmountTradedGross).toEqual('2469.1108');
                    expect(testOrder.variableAmountRemainingGross).toEqual('2030.1579');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('883.3929');
                    // 2 / 500
                    expect(testOrder.fee.variableAmount).toEqual('44.9926');
                    expect(testOrder.fee.variableAmountTraded).toEqual('24.6911');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('20.3015');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('8.8339');
                });
            });
            describe("using net sell amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'variableCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetSellAmount(BigNumber(800));
                    // TODO should be 800 but with rounding is coming out as 799.9997
                    expect(testOrder.sell.amount).toEqual('799.9997');
                    // 800 / (1000 * (1 + 0.01)) = 0.792079207920792
                    expect(testOrder.amount).toEqual('0.792079');
                    expect(testOrder.amountRemaining).toEqual('0.792079');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountLastPartial).toEqual('0');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'variableCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetSellAmount(BigNumber(1.2));
                    expect(testOrder.sell.amount).toEqual('1.2');
                    expect(testOrder.amount).toEqual('1.2');
                    expect(testOrder.amountRemaining).toEqual('1.2');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountLastPartial).toEqual('0');
                });
            });
            describe("using net buy amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'variableCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetBuyAmount(BigNumber(1));
                    expect(testOrder.buy.amount).toEqual('1');
                    expect(testOrder.amount).toEqual('1');
                    expect(testOrder.amountRemaining).toEqual('1');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountLastPartial).toEqual('0');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'variableCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetBuyAmount(BigNumber(700));
                    // should be 700 but with rounding comes out at 700.0002
                    expect(testOrder.buy.amount).toEqual('700.0002');
                    // 700 / (1000 * (1 - 0.01)) = 0.707070707070707
                    expect(testOrder.amount).toEqual('0.707071');
                    expect(testOrder.amountRemaining).toEqual('0.707071');
                    expect(testOrder.amountTraded).toEqual('0');
                    expect(testOrder.amountLastPartial).toEqual('0');
                });
            });
            describe("using net fixed amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'variableCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetFixedAmount(BigNumber(1));
                    expect(testOrder.amount).toEqual('1');
                    expect(testOrder.amountRemaining).toEqual('1');
                    expect(testOrder.fixedAmount).toEqual('1');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'variableCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetFixedAmount(BigNumber(1));
                    expect(testOrder.amount).toEqual('1');
                    expect(testOrder.amountRemaining).toEqual('1');
                    expect(testOrder.fixedAmount).toEqual('1');
                });
            });
        });
        describe("on an exchange with a fixedCurrency fee structure", function () {
            describe("a quoted sell order", function () {
                var testOrder = new order_1.default({ exchangeId: '6789',
                    exchangeName: 'fixedCurrencyFeeExchange',
                    symbol: 'BTCAUD',
                    side: 'sell',
                    state: 'quoted',
                    amount: '0.5',
                    amountRemaining: '0.2',
                    amountTraded: '0.3',
                    amountLastPartial: '0.1',
                    numberPartialFills: 2,
                    price: '200'
                });
                it("all properties are set", function () {
                    expect(testOrder.exchangeId).toEqual('6789');
                    expect(testOrder.exchangeName).toEqual('fixedCurrencyFeeExchange');
                    expect(testOrder.symbol).toEqual('BTCAUD');
                    expect(testOrder.side).toEqual('sell');
                    expect(testOrder.state).toEqual('quoted');
                    expect(testOrder.amount).toEqual('0.5');
                    expect(testOrder.amountTraded).toEqual('0.3');
                    expect(testOrder.amountRemaining).toEqual('0.2');
                    expect(testOrder.amountLastPartial).toEqual('0.1');
                    expect(testOrder.numberPartialFills).toEqual(2);
                    expect(testOrder.price).toEqual('200');
                });
                it("exchange virtual properties are set", function () {
                    expect(testOrder.exchange).toBeDefined();
                    expect(testOrder.exchange.name).toEqual('fixedCurrencyFeeExchange');
                });
                it("market virtual properties are set", function () {
                    expect(testOrder.market).toBeDefined();
                    expect(testOrder.market.symbol).toEqual('BTCAUD');
                });
                it("amount ratios are set", function () {
                    expect(testOrder.tradedRatioBN).toEqual(BigNumber(0.6));
                    expect(testOrder.remainingRatioBN).toEqual(BigNumber(0.4));
                    expect(testOrder.lastPartialRatioBN).toEqual(BigNumber(0.2));
                });
                it("currency virtual properties are set", function () {
                    expect(testOrder.fixedCurrency).toEqual('BTC');
                    expect(testOrder.variableCurrency).toEqual('AUD');
                });
                it("net buy virtual properties are set", function () {
                    expect(testOrder.buy.currency).toEqual('AUD');
                    // 0.5 * 200
                    expect(testOrder.buy.amount).toEqual('100');
                    // 0.2 * 200
                    expect(testOrder.buy.amountRemaining).toEqual('40');
                    // 0.3 * 200
                    expect(testOrder.buy.amountTraded).toEqual('60');
                    // 0.1 * 200
                    expect(testOrder.buy.amountLastPartial).toEqual('20');
                });
                it("gross buy virtual properties are set", function () {
                    // 0.5 * 200
                    expect(testOrder.buy.amountGross).toEqual('100');
                    // 0.2 * 200
                    expect(testOrder.buy.amountRemainingGross).toEqual('40');
                    // 0.3 * 200
                    expect(testOrder.buy.amountTradedGross).toEqual('60');
                    // 0.1 * 200
                    expect(testOrder.buy.amountLastPartialGross).toEqual('20');
                });
                it("net sell virtual properties are set", function () {
                    expect(testOrder.sell.currency).toEqual('BTC');
                    // 0.5 * (1 + 0.02)
                    expect(testOrder.sell.amount).toEqual('0.51');
                    // 0.2 * (1 - 0.02)
                    expect(testOrder.sell.amountRemaining).toEqual('0.204');
                    // 0.3 * (1 - 0.02)
                    expect(testOrder.sell.amountTraded).toEqual('0.306');
                    // 0.1 * (1 - 0.02)
                    expect(testOrder.sell.amountLastPartial).toEqual('0.102');
                });
                it("gross sell virtual properties are set", function () {
                    expect(testOrder.sell.amountGross).toEqual('0.5');
                    expect(testOrder.sell.amountRemainingGross).toEqual('0.2');
                    expect(testOrder.sell.amountTradedGross).toEqual('0.3');
                    expect(testOrder.sell.amountLastPartialGross).toEqual('0.1');
                });
                it("fee virtual properties are set", function () {
                    expect(testOrder.fee.currency).toEqual('BTC');
                    // 0.5 * 0.02
                    expect(testOrder.fee.amount).toEqual('0.01');
                    // 0.2 * 0.002
                    expect(testOrder.fee.amountRemaining).toEqual('0.004');
                    // 0.3 * 0.002
                    expect(testOrder.fee.amountTraded).toEqual('0.006');
                    // 0.1 * 0.002
                    expect(testOrder.fee.amountLastPartial).toEqual('0.002');
                });
                it("fixed currency virtual properties are set", function () {
                    expect(testOrder.fixedAmount).toEqual('0.51');
                    expect(testOrder.fixedAmountRemaining).toEqual('0.204');
                    expect(testOrder.fixedAmountTraded).toEqual('0.306');
                    expect(testOrder.fixedAmountLastPartial).toEqual('0.102');
                    expect(testOrder.fixedAmountGross).toEqual('0.5');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('0.2');
                    expect(testOrder.fixedAmountTradedGross).toEqual('0.3');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('0.1');
                    expect(testOrder.fee.fixedAmount).toEqual('0.01');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0.004');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('0.006');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0.002');
                });
                it("variable currency virtual properties are set", function () {
                    expect(testOrder.variableAmount).toEqual('100');
                    expect(testOrder.variableAmountRemaining).toEqual('40');
                    expect(testOrder.variableAmountTraded).toEqual('60');
                    expect(testOrder.variableAmountLastPartial).toEqual('20');
                    expect(testOrder.variableAmountGross).toEqual('100');
                    expect(testOrder.variableAmountRemainingGross).toEqual('40');
                    expect(testOrder.variableAmountTradedGross).toEqual('60');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('20');
                    expect(testOrder.fee.variableAmount).toEqual('0');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('0');
                    expect(testOrder.fee.variableAmountTraded).toEqual('0');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('0');
                });
            });
            describe("a partially filled buy order", function () {
                var testOrder = new order_1.default({
                    exchangeId: '333',
                    exchangeName: 'fixedCurrencyFeeExchange',
                    symbol: 'BTCAUD',
                    side: 'buy',
                    state: 'partiallyFilled',
                    amount: '100',
                    amountTraded: '80',
                    amountRemaining: '20',
                    amountLastPartial: '30',
                    numberPartialFills: 1,
                    price: '100'
                });
                it("properties are set", function () {
                    expect(testOrder.side).toEqual('buy');
                    expect(testOrder.state).toEqual('partiallyFilled');
                    expect(testOrder.amount).toEqual('100');
                    expect(testOrder.amountTraded).toEqual('80');
                    expect(testOrder.amountRemaining).toEqual('20');
                    expect(testOrder.amountLastPartial).toEqual('30');
                    expect(testOrder.numberPartialFills).toEqual(1);
                    expect(testOrder.price).toEqual('100');
                });
                it("properties are set", function () {
                    expect(testOrder.tradedRatioBN).toEqual(BigNumber(0.8));
                    expect(testOrder.remainingRatioBN).toEqual(BigNumber(0.2));
                    expect(testOrder.lastPartialRatioBN).toEqual(BigNumber(0.3));
                });
                it("net buy virtual properties is set", function () {
                    expect(testOrder.buy.currency).toEqual('BTC');
                    // 100 * (1 - 0.02)
                    expect(testOrder.buy.amount).toEqual('98');
                    // 80 * (1 - 0.02)
                    expect(testOrder.buy.amountTraded).toEqual('78.4');
                    // 20 * (1 - 0.02)
                    expect(testOrder.buy.amountRemaining).toEqual('19.6');
                    // 30 * (1 - 0.02)
                    expect(testOrder.buy.amountLastPartial).toEqual('29.4');
                });
                it("gross buy virtual properties are set", function () {
                    expect(testOrder.buy.amountGross).toEqual('100');
                    expect(testOrder.buy.amountTradedGross).toEqual('80');
                    expect(testOrder.buy.amountRemainingGross).toEqual('20');
                    expect(testOrder.buy.amountLastPartialGross).toEqual('30');
                });
                it("net sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('AUD');
                    // 100 * 100
                    expect(testOrder.sell.amount).toEqual('10000');
                    // 80 * 100
                    expect(testOrder.sell.amountTraded).toEqual('8000');
                    // 20 * 100
                    expect(testOrder.sell.amountRemaining).toEqual('2000');
                    // 30 * 100
                    expect(testOrder.sell.amountLastPartial).toEqual('3000');
                });
                it("gross sell virtual properties is set", function () {
                    expect(testOrder.sell.currency).toEqual('AUD');
                    expect(testOrder.sell.amount).toEqual('10000');
                    expect(testOrder.sell.amountTraded).toEqual('8000');
                    expect(testOrder.sell.amountRemaining).toEqual('2000');
                    expect(testOrder.sell.amountLastPartial).toEqual('3000');
                });
                it("fee virtual properties is set", function () {
                    expect(testOrder.fee.currency).toEqual('BTC');
                    // 100 * 0.02
                    expect(testOrder.fee.amount).toEqual('2');
                    // 80 * 0.02
                    expect(testOrder.fee.amountTraded).toEqual('1.6');
                    // 20 * 0.02
                    expect(testOrder.fee.amountRemaining).toEqual('0.4');
                    // 30 * 0.02
                    expect(testOrder.fee.amountLastPartial).toEqual('0.6');
                });
                it("fixed currency virtual properties are set", function () {
                    expect(testOrder.fixedAmount).toEqual('98');
                    expect(testOrder.fixedAmountTraded).toEqual('78.4');
                    expect(testOrder.fixedAmountRemaining).toEqual('19.6');
                    expect(testOrder.fixedAmountLastPartial).toEqual('29.4');
                    expect(testOrder.fixedAmountGross).toEqual('100');
                    expect(testOrder.fixedAmountTradedGross).toEqual('80');
                    expect(testOrder.fixedAmountRemainingGross).toEqual('20');
                    expect(testOrder.fixedAmountLastPartialGross).toEqual('30');
                    expect(testOrder.fee.fixedAmount).toEqual('2');
                    expect(testOrder.fee.fixedAmountTraded).toEqual('1.6');
                    expect(testOrder.fee.fixedAmountRemaining).toEqual('0.4');
                    expect(testOrder.fee.fixedAmountLastPartial).toEqual('0.6');
                });
                it("variable currency virtual properties are set", function () {
                    // 100 * 500
                    expect(testOrder.variableAmount).toEqual('10000');
                    expect(testOrder.variableAmountTraded).toEqual('8000');
                    expect(testOrder.variableAmountRemaining).toEqual('2000');
                    expect(testOrder.variableAmountLastPartial).toEqual('3000');
                    expect(testOrder.variableAmountGross).toEqual('10000');
                    expect(testOrder.variableAmountTradedGross).toEqual('8000');
                    expect(testOrder.variableAmountRemainingGross).toEqual('2000');
                    expect(testOrder.variableAmountLastPartialGross).toEqual('3000');
                    expect(testOrder.fee.variableAmount).toEqual('0');
                    expect(testOrder.fee.variableAmountTraded).toEqual('0');
                    expect(testOrder.fee.variableAmountRemaining).toEqual('0');
                    expect(testOrder.fee.variableAmountLastPartial).toEqual('0');
                });
            });
            describe("using net sell amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'fixedCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetSellAmount(BigNumber(800));
                    expect(testOrder.sell.amount).toEqual('800');
                    expect(testOrder.amount).toEqual('0.8');
                    expect(testOrder.amountRemaining).toEqual('0.8');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'fixedCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetSellAmount(BigNumber(1.2));
                    expect(testOrder.sell.amount).toEqual('1.2');
                    // 1.2 / (1 + 0.02) = 1.17647058823529
                    expect(testOrder.amount).toEqual('1.176471');
                    expect(testOrder.amountRemaining).toEqual('1.176471');
                });
            });
            describe("using net buy amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'fixedCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetBuyAmount(BigNumber(1));
                    // TODO should be 1 but with rounding comes out at 0.999999
                    expect(testOrder.buy.amount).toEqual('0.999999');
                    // 1 / (1 - 0.02) = 1.02040816326531
                    expect(testOrder.amount).toEqual('1.020408');
                    expect(testOrder.amountRemaining).toEqual('1.020408');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'fixedCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetBuyAmount(BigNumber(700));
                    expect(testOrder.buy.amount).toEqual('700');
                    // 700 / 1000 = 0.7
                    expect(testOrder.amount).toEqual('0.7');
                    expect(testOrder.amountRemaining).toEqual('0.7');
                });
            });
            describe("using net fixed amount", function () {
                it("a buy trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'fixedCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'buy',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetFixedAmount(BigNumber(1));
                    // 1 / (1 - 0.02) = 1.02040816326531
                    expect(testOrder.amount).toEqual('1.020408');
                    expect(testOrder.amountRemaining).toEqual('1.020408');
                    // 1.020408 * (1 - 0.02) = 0.99999984 rounded down
                    expect(testOrder.fixedAmount).toEqual('0.999999');
                });
                it("a sell trade", function () {
                    var testOrder = new order_1.default({
                        exchangeName: 'fixedCurrencyFeeExchange',
                        symbol: 'BTCAUD',
                        side: 'sell',
                        price: '1000'
                    });
                    testOrder.setAmountsFromNetFixedAmount(BigNumber(1));
                    // 1 / (1 + 0.02) = 0.980392156862745
                    expect(testOrder.amount).toEqual('0.980392');
                    expect(testOrder.amountRemaining).toEqual('0.980392');
                    // 0.980392 * (1 + 0.02) = 0.99999984 rounded down
                    expect(testOrder.fixedAmount).toEqual('0.999999');
                });
            });
        });
    });
    describe("clone order with an adjusted exchange rate", function () {
        var testSellOrder, testBuyOrder;
        beforeEach(function () {
            testSellOrder = new order_1.default({
                exchangeId: '555',
                exchangeName: 'testExchange2',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'pending',
                amount: '7.7777',
                amountTraded: '5.5555',
                amountRemaining: '2.2222',
                amountLastPartial: '1.1111',
                numberPartialFills: 3,
                price: '100'
            });
            testBuyOrder = new order_1.default({
                exchangeId: '555',
                exchangeName: 'testExchange2',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'pending',
                amount: '3.1234',
                amountTraded: '2.1234',
                amountRemaining: '1',
                amountLastPartial: '0.4321',
                numberPartialFills: 5,
                price: '666.666'
            });
        });
        describe("lower than old buy trade", function () {
            var rates;
            beforeEach(function () {
                rates = {
                    bestRate: '33.333',
                    maxRate: '44.444',
                    minRate: '22.222'
                };
            });
            it("with no min trade amount or max sell amount", function () {
                var newAdjustedOrder = testBuyOrder.cloneFromAdjustedRate(rates);
                expect(newAdjustedOrder).not.toBe(testBuyOrder);
                expect(newAdjustedOrder.price).toEqual('33.333');
                expect(newAdjustedOrder.minRate).toEqual('22.222');
                expect(newAdjustedOrder.maxRate).toEqual('44.444');
                expect(newAdjustedOrder.amountTraded).toEqual('2.1234');
                expect(newAdjustedOrder.amountLastPartial).toEqual('0.4321');
                expect(newAdjustedOrder.numberPartialFills).toEqual(5);
                // 1 * 666.666 / 33.333
                expect(newAdjustedOrder.amountRemaining).toEqual('20.0001');
                // 3.1234 + (20.0001 - 1)
                expect(newAdjustedOrder.amount).toEqual('22.1235');
            });
            it("with min trade amount < adjusted remaining amount and adjusted remaining amount < max sell amount", function () {
                var newAdjustedOrder = testBuyOrder.cloneFromAdjustedRate(rates, BigNumber(0.01), BigNumber(25));
                // same as previous test
                expect(newAdjustedOrder.amountRemaining).toEqual('20.0001');
                expect(newAdjustedOrder.amount).toEqual('22.1235');
            });
            it("with max sell amount > adjusted remaining amount", function () {
                var newAdjustedOrder = testBuyOrder.cloneFromAdjustedRate(rates, BigNumber(0.01), BigNumber(2.22));
                expect(newAdjustedOrder.amountRemaining).toEqual('2.22');
                // 22.1236 - (20.0002 - 2.22)
                expect(newAdjustedOrder.amount).toEqual('4.3434');
            });
        });
        describe("lower than old sell trade", function () {
            var rates;
            beforeEach(function () {
                rates = {
                    bestRate: '10',
                    minRate: '9',
                    maxRate: '12'
                };
            });
            it("with no min trade amount or max sell amount", function () {
                var newAdjustedOrder = testSellOrder.cloneFromAdjustedRate(rates);
                expect(newAdjustedOrder).not.toBe(testSellOrder);
                expect(newAdjustedOrder.price).toEqual('10');
                expect(newAdjustedOrder.minRate).toEqual('9');
                expect(newAdjustedOrder.maxRate).toEqual('12');
                expect(newAdjustedOrder.amountTraded).toEqual('5.5555');
                expect(newAdjustedOrder.amountLastPartial).toEqual('1.1111');
                expect(newAdjustedOrder.numberPartialFills).toEqual(3);
                expect(newAdjustedOrder.amountRemaining).toEqual('2.2222');
                expect(newAdjustedOrder.amount).toEqual('7.7777');
                // 2.2222 * 10 * (1 - 0.04)
                expect(newAdjustedOrder.buy.amountRemaining).toEqual('21.333');
                // 2.2222
                expect(newAdjustedOrder.sell.amountRemaining).toEqual('2.2222');
            });
            it("with min trade amount < adjusted remaining amount and adjusted remaining amount < max sell amount", function () {
                var newAdjustedOrder = testSellOrder.cloneFromAdjustedRate(rates, BigNumber(0.01), BigNumber(3));
                // same as previous test
                expect(newAdjustedOrder.amountRemaining).toEqual('2.2222');
                expect(newAdjustedOrder.amount).toEqual('7.7777');
            });
        });
        it("same as old buy trade", function () {
            var rates = {
                bestRate: '666.666',
                minRate: '555.555',
                maxRate: '777.777'
            };
            var newAdjustedOrder = testBuyOrder.cloneFromAdjustedRate(rates);
            expect(newAdjustedOrder).not.toBe(testBuyOrder);
            expect(newAdjustedOrder.price).toEqual('666.666');
            expect(newAdjustedOrder.minRate).toEqual('555.555');
            expect(newAdjustedOrder.maxRate).toEqual('777.777');
            expect(newAdjustedOrder.amountTraded).toEqual('2.1234');
            expect(newAdjustedOrder.amountLastPartial).toEqual('0.4321');
            expect(newAdjustedOrder.numberPartialFills).toEqual(5);
            expect(newAdjustedOrder.amountRemaining).toEqual('1');
            expect(newAdjustedOrder.amount).toEqual('3.1234');
        });
        describe("higher than old buy trade", function () {
            var rates;
            beforeEach(function () {
                rates = {
                    bestRate: '1111.111',
                    maxRate: '1200',
                    minRate: '1000.001'
                };
            });
            it("with no min trade amount or max sell amount", function () {
                var newAdjustedOrder = testBuyOrder.cloneFromAdjustedRate(rates);
                expect(newAdjustedOrder).not.toBe(testBuyOrder);
                expect(newAdjustedOrder.price).toEqual('1111.111');
                expect(newAdjustedOrder.maxRate).toEqual('1200');
                expect(newAdjustedOrder.minRate).toEqual('1000.001');
                expect(newAdjustedOrder.amountTraded).toEqual('2.1234');
                expect(newAdjustedOrder.amountLastPartial).toEqual('0.4321');
                expect(newAdjustedOrder.numberPartialFills).toEqual(5);
                // 1 * 666.666 / 1111.111
                expect(newAdjustedOrder.amountRemaining).toEqual('0.5999');
                // 3.1234 + (0.5999 - 1)
                expect(newAdjustedOrder.amount).toEqual('2.7233');
            });
            it("with min trade amount < adjusted remaining amount and adjusted remaining amount < max sell amount", function () {
                var newAdjustedOrder = testBuyOrder.cloneFromAdjustedRate(rates, BigNumber(0.01), BigNumber(10));
                // same as previous test
                expect(newAdjustedOrder.amountRemaining).toEqual('0.5999');
                expect(newAdjustedOrder.amount).toEqual('2.7233');
            });
            it("with max sell amount > adjusted remaining amount", function () {
                var newAdjustedOrder = testBuyOrder.cloneFromAdjustedRate(rates, BigNumber(1.1), BigNumber(2.22));
                expect(newAdjustedOrder.amountRemaining).toEqual('1.1');
                // 2.7233 + (1.1 - 0.5999)
                expect(newAdjustedOrder.amount).toEqual('3.2234');
            });
        });
    });
});
