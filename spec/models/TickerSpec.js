/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
var TestExchange_1 = require('../../exchanges/TestExchange');
var exchange_1 = require('../../models/exchange');
var quantity_1 = require('../../models/quantity');
var ticker_1 = require('../../models/ticker');
describe("Ticker: ", function () {
    // Define test exhange data rather than using live exchange config that can change
    var testExchange1 = new TestExchange_1.default({
        name: 'testExchange1',
        fixedCurrencies: ["LTC", "BTC"],
        variableCurrencies: ["AUD", "EUR", "USD"],
        commissions: { LTC: 0.1, BTC: 0.02, AUD: 0.01, USD: 0.06, EUR: 0.03 },
        defaultPriceRounding: 2,
        defaultMinAmount: 0.1,
        confirmationTime: 1 //not a fixed rate exchange so zero confirmation time
    });
    var testExchange2 = new TestExchange_1.default({
        name: 'testExchange2',
        fixedCurrencies: ["BTC"],
        variableCurrencies: ["EUR", "USD"],
        commissions: { BTC: 0.01, AUD: 0.03, USD: 0.04 },
        defaultPriceRounding: 3,
        defaultMinAmount: 0.001,
        confirmationTime: 20
    });
    // override the global exchanges variable for testing purposes
    exchange_1.default.exchanges = {};
    exchange_1.default.exchanges['testExchange1'] = testExchange1;
    exchange_1.default.exchanges['testExchange2'] = testExchange2;
    var quantity1, quantity2, quantity3, quantity4, quantity5, quantity6, quantity7, quantity8, ticker1, ticker2, ticker3, ticker4, ticker5, testTicker;
    // instansiate new test object before each test
    beforeEach(function () {
        quantity1 = new quantity_1.default({ currency: 'AUD', amount: 100 });
        quantity2 = new quantity_1.default({ currency: 'BTC', amount: 1.4459 });
        quantity3 = new quantity_1.default({ currency: 'USD', amount: 444.99 });
        quantity4 = new quantity_1.default({ currency: 'EUR', amount: 2000.01 });
        quantity5 = new quantity_1.default({ currency: 'LTC', amount: 0.1001 });
        quantity6 = new quantity_1.default({ currency: 'AUD', amount: 123.45 });
        quantity7 = new quantity_1.default({ currency: 'BTC', amount: 0.1234 });
        quantity8 = new quantity_1.default({ currency: 'AUD', amount: 3.75 });
        ticker1 = new ticker_1.default({
            exchangeName: 'testExchange1',
            symbol: 'BTCAUD',
            bid: 918.9876,
            ask: 946.4898
        });
        ticker2 = new ticker_1.default({
            exchangeName: 'testExchange1',
            symbol: 'BTCUSD',
            bid: 400,
            ask: 600
        });
        ticker3 = new ticker_1.default({
            exchangeName: 'testExchange1',
            symbol: 'BTCEUR',
            bid: 250,
            ask: 300
        });
        ticker4 = new ticker_1.default({
            exchangeName: 'testExchange2',
            symbol: 'BTCUSD',
            bid: 410,
            ask: 610
        });
        ticker5 = new ticker_1.default({
            exchangeName: 'testExchange1',
            symbol: 'LTCAUD',
            bid: 90,
            ask: 110
        });
        this.addMatchers({
            toEqualTrade: function (expectedOrder) {
                var actualOrder = this.actual;
                var notText = this.isNot ? " not" : "";
                this.message = function () {
                    return "Expected buy " + actualOrder.buy.amount + " " + actualOrder.buy.currency + notText +
                        " to equal " + expectedOrder.buy.amount + " " + expectedOrder.buy.currency +
                        " and fee " + actualOrder.fee.amount + " " + actualOrder.fee.currency + notText +
                        " to equal " + expectedOrder.fee.amount + " " + expectedOrder.fee.currency +
                        " and sell " + actualOrder.sell.amount + " " + actualOrder.sell.currency + notText +
                        " to equal " + expectedOrder.sell.amount + " " + expectedOrder.sell.currency +
                        " and price " + actualOrder.price + notText +
                        " to equal " + expectedOrder.price +
                        " and state " + actualOrder.state + notText +
                        " to equal " + expectedOrder.state;
                };
                return actualOrder.sell.currency === expectedOrder.sell.currency &&
                    actualOrder.sell.amount === expectedOrder.sell.amount &&
                    actualOrder.buy.currency === expectedOrder.buy.currency &&
                    actualOrder.buy.amount === expectedOrder.buy.amount &&
                    actualOrder.fee.currency === expectedOrder.fee.currency &&
                    actualOrder.fee.amount === expectedOrder.fee.amount &&
                    actualOrder.price === expectedOrder.price &&
                    actualOrder.state === expectedOrder.state;
            }
        });
    });
    it("exchange property: ", function () {
        var testTicker = new ticker_1.default({
            exchangeName: 'testExchange1',
            symbol: 'BTCUSD',
            bid: 400,
            ask: 600
        });
        expect(testTicker.exchange).toBeDefined();
        expect(testTicker.exchange.name).toEqual('testExchange1');
    });
    it("fixed and variable properties: ", function () {
        var testTicker = new ticker_1.default({
            exchangeName: 'testExchange1',
            symbol: 'BTCUSD',
            bid: 400,
            ask: 600
        });
        expect(testTicker.fixedCurrency).toEqual('BTC');
        expect(testTicker.variableCurrency).toEqual('USD');
    });
    it("Ticker.spread() : ", function () {
        expect(ticker1.spread()).toEqual(27.5022);
        expect(ticker2.spread()).toEqual(200);
        expect(ticker3.spread()).toEqual(50);
        expect(ticker4.spread()).toEqual(200);
    });
    it("Ticker.spreadPercentage() : ", function () {
        // (946.4898 - 918.9876) / 918.9876 * 100 = ‌2.992662795450113
        expect(ticker1.spreadPercentage()).toEqual(2.99);
        expect(ticker2.spreadPercentage()).toEqual(50);
        expect(ticker3.spreadPercentage()).toEqual(20);
        expect(ticker4.spreadPercentage()).toEqual(48.78);
    });
    /* TODO get these Ticker sell tests working
    describe("Ticker.sell() functions : ", function() {
        it("Sell quantity1 at ticker1 on testExchange1", function() {
            var actualTrade = ticker1.sell(quantity1);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'BTCAUD',
                                        state: 'pending',
                                        side: 'sell',
                                        amount: '0.1036',
                                        //buy:new Quantity({currency:'BTC',amount:0.1036}),
                                        //sell:new Quantity({currency:'AUD',amount:100}),
                                        //fee:new Quantity({currency:'BTC',amount:0.0021}),
                                        price:946.4898});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
        
        it("Sell quantity2 at ticker1 on testExchange1", function() {
            var actualTrade = ticker1.sell(quantity2);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'BTCAUD',
                                        state:'pending',
                                        side: 'sell',
                                        amount:'1.4459',
                                        price:918.9876});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
        
        it("Sell quantity3 at ticker2 on testExchange1", function() {
            var actualTrade = ticker2.sell(quantity3);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'BTCUSD',
                                        state:'pending',
                                        side: 'sell',
                                        amount: '0.7269',
                                        price:600});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
        
        it("Sell quantity2 at ticker2 on testExchange1", function() {
            var actualTrade = ticker2.sell(quantity2);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'BTCUSD',
                                        state:'pending',
                                        side: 'sell',
                                        amount: '1.4459',
                                        price:400});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
        it("Sell quantity3 at ticker2 on testExchange1", function() {
            var actualTrade = ticker2.sell(quantity3);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'BTCUSD',
                                        state:'pending',
                                        side: 'sell',
                                        amount: '444.99',
                                        price:600});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
        it("Sell quantity5 at ticker5 on testExchange1", function() {
            var actualTrade = ticker5.sell(quantity5);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'LTCAUD',
                                        state:'pending',
                                        side: 'sell',
                                        amount: '0.1001',
                                        price:90});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
    });
    */
    /* get these Ticker.buy() tests working
    describe("Ticker.buy() : ", function() {
        it("Buy quantity1 at ticker1 on testExchange1", function() {
            var actualTrade = ticker1.buy(quantity1);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'BTCAUD',
                                        state:'pending',
                                        side: 'buy',
                                        amount: '0.1099',
                                        price:946.4898});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
        
        it("Buy quantity1 at ticker2 on testExchange1", function() {
            var actualTrade = ticker1.buy(quantity2);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'BTCAUD',
                                        state:'pending',
                                        side: 'buy',
                                        amount:'1.4459',
                                        price:946.4898});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
        
        it("Buy quantity3 at ticker2 on testExchange1", function() {
            var actualTrade = ticker2.buy(quantity3);
            var expectedTrade = new Trade({
                                        exchangeName:'testExchange1',
                                        symbol: 'BTCUSD',
                                        state:'pending',
                                        side: 'buy',
                                        amount: '445.00',
                                        price:400});
            expect(actualTrade).toEqualTrade(expectedTrade);
        });
    });
    */
    describe("Log ticker changes", function () {
        var previousTicker;
        beforeEach(function () {
            previousTicker = new ticker_1.default({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bid: 987.45678,
                ask: 999.24562,
                last: 996.1235
            });
            exchange_1.default.exchanges['testExchange1'].markets['BTCUSD'].addTicker(previousTicker);
        });
        it("Previous tickers are printed", function () {
            ticker_1.default.log(previousTicker);
        });
        it("Last tickers are printed", function () {
            var lastTicker = new ticker_1.default({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bid: 977.34535,
                ask: 1000.75456,
                last: 996.1234
            });
            exchange_1.default.exchanges['testExchange1'].markets['BTCUSD'].addTicker(lastTicker);
            ticker_1.default.log(lastTicker);
        });
    });
});
