/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
var quantity_1 = require('../../models/quantity');
describe("Quantity: ", function () {
    it("Initialising", function () {
        var testQuantity = new quantity_1.default({ currency: 'AUD', amount: 100 });
        expect(testQuantity.currency).toEqual('AUD');
        expect(testQuantity.amount).toEqual(100);
    });
});
