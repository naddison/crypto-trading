/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
var util_1 = require('util');
var BigNumber = require('bignumber.js');
// import my Interfaces
var market_1 = require("./market");
var order_1 = require("./order");
var return_1 = require("./return");
// My packages
var logger = require('config-logger'), VError = require('verror');
var AggregatedOrders = (function () {
    function AggregatedOrders() {
        this.sumFixedAmountLastPartialGrossBN = new BigNumber(0);
        this.sumFixedAmountLastPartialBN = new BigNumber(0);
        this.sumVariableAmountLastPartialGrossBN = new BigNumber(0);
        this.sumVariableAmountLastPartialBN = new BigNumber(0);
        this.priceGrossBN = new BigNumber(0);
        this.priceNetBN = new BigNumber(0);
    }
    return AggregatedOrders;
})();
var OrderPair = (function () {
    function OrderPair(leadingSymbol, trailingSymbol, trailing2LeadingExchangeRate) {
        this.leadingSymbol = leadingSymbol;
        this.trailingSymbol = trailingSymbol;
        this.trailing2LeadingExchangeRate = trailing2LeadingExchangeRate;
        this.leadingOrder = null;
        this.trailingOrders = [];
        this.aggregatedBuyOrders = new AggregatedOrders();
        this.aggregatedSellOrders = new AggregatedOrders();
        this.aggregatedLeadingOrders = new AggregatedOrders();
        this.aggregatedTrailingOrders = new AggregatedOrders();
        var functionName = 'OrderPair.constructor()';
        if (trailingSymbol && !trailing2LeadingExchangeRate) {
            var error = new VError('%s trailing2LeadingExchangeRate parameter not passed to constructor for %s to %s conversion', functionName, leadingSymbol, trailingSymbol);
            logger.error(error.stack);
            throw error;
        }
        else if (leadingSymbol && !trailingSymbol) {
            // assume the trailing symbol is the same as the leading symbol
            this.trailingSymbol = leadingSymbol;
            this.trailing2LeadingExchangeRate = 1;
        }
        this.leadingVariableCurrency = market_1.default.getVariableCurrency(this.leadingSymbol);
        this.trailingVariableCurrency = market_1.default.getVariableCurrency(this.trailingSymbol);
        logger.debug('%s leadingSymbol %s, trailingSymbol %s and trailing2LeadingExchangeRate %s', functionName, this.leadingSymbol, this.trailingSymbol, this.trailing2LeadingExchangeRate);
    }
    OrderPair.prototype.addTrailingOrder = function (newTrailingOrder) {
        var functionName = 'OrderPair.addTrailingOrder()';
        if (this.leadingOrder === null) {
            var error = new VError('%s can not add trailing order when leading order has not been set', functionName);
            logger.error(error.stack);
            throw error;
        }
        else if (newTrailingOrder.side == this.leadingOrder.side) {
            var error = new VError('%s trailing order side %s has to be the opposite to the leading order side %s', functionName, newTrailingOrder.side, this.leadingOrder.side);
            logger.error(error.stack);
            throw error;
        }
        else if (newTrailingOrder.amountLastPartialBN.equals(0)) {
            logger.error('%s last partial amount on trailing order is 0', functionName);
            throw new TypeError('zeroLastPartialAmount');
        }
        // add new trailing order
        this.trailingOrders.push(newTrailingOrder);
        if (newTrailingOrder.side === 'sell') {
            calculateAggregatedValues(this.aggregatedSellOrders, newTrailingOrder);
        }
        else if (newTrailingOrder.side === 'buy') {
            calculateAggregatedValues(this.aggregatedBuyOrders, newTrailingOrder);
        }
    };
    /**
     * calculates the percentage in decimal format of trailing net fixed currency traded compared to the leading net fixed currency amount
     */
    OrderPair.prototype.getFixedCurrencyReturnedDecimalBN = function () {
        var functionName = 'OrderPair.getFixedCurrencyReturnedDecimalBN()';
        var fixedCurrencyReturnedDecimalBN = this.aggregatedTrailingOrders.sumFixedAmountLastPartialBN.
            div(this.aggregatedLeadingOrders.sumFixedAmountLastPartialBN).
            round(4);
        logger.debug('%s percentage of fixed currency returned as a decimal %s = sum fixed currency amounts of trailing orders %s / sum fixed currency amounts of leading orders', functionName, fixedCurrencyReturnedDecimalBN.toString(), this.aggregatedTrailingOrders.sumFixedAmountLastPartialBN.toString(), this.aggregatedLeadingOrders.sumFixedAmountLastPartialBN.toString());
        return fixedCurrencyReturnedDecimalBN;
    };
    /**
     * calculates the percentage of trailing net fixed currency traded compared to the leading net fixed currency amount
     */
    OrderPair.prototype.getFixedCurrencyReturnedPercentageBN = function () {
        return this.getFixedCurrencyReturnedDecimalBN().
            times(100);
    };
    OrderPair.prototype.getGrossVariableReturn = function () {
        return this.createVariableReturn(this.aggregatedBuyOrders.sumVariableAmountLastPartialGrossBN, this.aggregatedSellOrders.sumVariableAmountLastPartialGrossBN);
    };
    OrderPair.prototype.getNetVariableReturn = function () {
        return this.createVariableReturn(this.aggregatedBuyOrders.sumVariableAmountLastPartialBN, this.aggregatedSellOrders.sumVariableAmountLastPartialBN);
    };
    OrderPair.prototype.createVariableReturn = function (buyAmountBN, sellAmountBN) {
        var functionName = 'OrderPair.createVariableReturn()';
        var sellCurrency, buyCurrency, sell2buyExchangeRateBN;
        if (!this.leadingOrder) {
            var error = new VError('%s can not calculate variable return before adding a leading order', functionName);
            logger.error(error.stack);
            return error;
        }
        if (this.leadingOrder.side === 'sell') {
            sellCurrency = this.leadingVariableCurrency;
            buyCurrency = this.trailingVariableCurrency;
            logger.trace('%s leading order is a sell so variable currency of sell order %s = variable currency of leading order %s; variable currency of buy order %s = variable currency of trailing orders %s', functionName, sellCurrency, this.leadingVariableCurrency, buyCurrency, this.trailingVariableCurrency);
            sell2buyExchangeRateBN = new BigNumber(1).
                div(this.trailing2LeadingExchangeRate);
        }
        else if (this.leadingOrder.side === 'buy') {
            sellCurrency = this.trailingVariableCurrency;
            buyCurrency = this.leadingVariableCurrency;
            logger.trace('%s leading order is a buy so variable currency of sell order %s = variable currency of trailing orders %s; variable currency of buy order %s = variable currency of leading order %s', functionName, sellCurrency, this.trailingVariableCurrency, buyCurrency, this.leadingVariableCurrency);
            sell2buyExchangeRateBN = new BigNumber(this.trailing2LeadingExchangeRate);
        }
        return new return_1.default({
            sellCurrency: sellCurrency,
            sellAmount: sellAmountBN.toString(),
            buyCurrency: buyCurrency,
            buyAmount: buyAmountBN.toString(),
            sell2buyExchangeRate: sell2buyExchangeRateBN
        });
    };
    OrderPair.prototype.getGrossFixedReturn = function () {
        var functionName = 'OrderPair.getGrossFixedReturn()';
        if (!this.leadingOrder || this.trailingOrders.length === 0) {
            var error = new VError('%s leading order has not been set so can not get return', functionName);
            logger.error(error.stack);
            return error;
        }
        return new return_1.default({
            sellCurrency: this.leadingOrder.fixedCurrency,
            sellAmount: this.aggregatedSellOrders.sumFixedAmountLastPartialGrossBN.toString(),
            buyCurrency: this.leadingOrder.fixedCurrency,
            buyAmount: this.aggregatedBuyOrders.sumFixedAmountLastPartialGrossBN.toString()
        });
    };
    OrderPair.prototype.getNetFixedReturn = function () {
        var functionName = 'OrderPair.getNetFixedReturn()';
        if (!this.leadingOrder || this.trailingOrders.length === 0) {
            var error = new VError('%s leading order has not been set or there are no %s trailing orders', functionName, this.trailingOrders.length);
            logger.error(error.stack);
            return error;
        }
        return new return_1.default({
            sellCurrency: this.leadingOrder.fixedCurrency,
            sellAmount: this.aggregatedSellOrders.sumFixedAmountLastPartialBN.toString(),
            buyCurrency: this.leadingOrder.fixedCurrency,
            buyAmount: this.aggregatedBuyOrders.sumFixedAmountLastPartialBN.toString()
        });
    };
    OrderPair.prototype.getGrossPriceReturn = function () {
        var functionName = 'OrderPair.getGrossPriceReturn()';
        if (!this.leadingOrder || this.trailingOrders.length === 0) {
            var error = new VError('%s leading order has not been set so can not get return', functionName);
            logger.error(error.stack);
            return error;
        }
        return this.createVariableReturn(this.aggregatedBuyOrders.priceGrossBN, this.aggregatedSellOrders.priceGrossBN);
    };
    OrderPair.prototype.getNetPriceReturn = function () {
        return this.createVariableReturn(this.aggregatedBuyOrders.priceNetBN, this.aggregatedSellOrders.priceNetBN);
    };
    OrderPair.prototype.addLeadingOrder = function (newLeadingOrder) {
        var functionName = 'OrderPair.addLeadingOrder()';
        if (!(newLeadingOrder instanceof order_1.default)) {
            var error = new VError('%s first parameter newLeadingOrder %s is not of type Order', functionName, newLeadingOrder);
            logger.error(error.stack);
            throw error;
        }
        else if (newLeadingOrder.symbol != this.leadingSymbol) {
            var error = new VError('%s symbol %s on new leading order parameter does not match this orderPairs leading symbol %s', functionName, newLeadingOrder.symbol, this.leadingSymbol);
            logger.error(error.stack);
            throw error;
        }
        else if (!(newLeadingOrder.state != 'filled' || newLeadingOrder.state != 'partiallyFilled')) {
            var error = new VError('%s leading order state %s is not filled or partiallyFilled', functionName, newLeadingOrder.state);
            logger.error(error.stack);
            throw error;
        }
        this.leadingOrder = newLeadingOrder;
        if (this.leadingOrder.side === 'sell') {
            calculateAggregatedValues(this.aggregatedSellOrders, newLeadingOrder);
            this.aggregatedLeadingOrders = this.aggregatedSellOrders;
            this.aggregatedTrailingOrders = this.aggregatedBuyOrders;
        }
        else if (this.leadingOrder.side === 'buy') {
            calculateAggregatedValues(this.aggregatedBuyOrders, newLeadingOrder);
            this.aggregatedLeadingOrders = this.aggregatedBuyOrders;
            this.aggregatedTrailingOrders = this.aggregatedSellOrders;
        }
    };
    OrderPair.prototype.summaryDescription = function () {
        var functionName = "OrderPair.summaryDescription()";
        var trailingVwapBN;
        if (!this.leadingOrder) {
            return 'return 0% as no leading or trailing orders';
        }
        else if (this.leadingOrder.side === 'sell') {
            trailingVwapBN = this.aggregatedBuyOrders.priceGrossBN.round(2);
        }
        else if (this.leadingOrder.side === 'buy') {
            trailingVwapBN = this.aggregatedSellOrders.priceGrossBN.round(2);
        }
        var descriptionOfTrailingOrders = 'no trailing orders';
        if (this.trailingOrders.length > 0) {
            descriptionOfTrailingOrders = util_1.format('%s trailing %s %s%s %s @ %s', this.trailingOrders[0].exchangeName, this.trailingOrders[0].side, this.aggregatedTrailingOrders.sumVariableAmountLastPartialBN.round(4).toString(), this.trailingVariableCurrency, this.aggregatedTrailingOrders.sumFixedAmountLastPartialGrossBN.round(4).toString(), trailingVwapBN.toString());
        }
        var descriptionOfTrailing2Leading = "";
        if (this.trailing2LeadingExchangeRate !== 1) {
            var trailing2LeadingCode = this.leadingVariableCurrency +
                this.trailingVariableCurrency;
            descriptionOfTrailing2Leading = ", " + trailing2LeadingCode + " " + this.trailing2LeadingExchangeRate;
        }
        // force
        var leadingNetVariableReturn = this.getNetVariableReturn().amountBN(this.leadingVariableCurrency), trailingNetVariableReturn = this.getNetVariableReturn().amountBN(this.trailingVariableCurrency);
        if (leadingNetVariableReturn instanceof Error) {
            leadingNetVariableReturn = BigNumber(NaN);
        }
        if (trailingNetVariableReturn instanceof Error) {
            trailingNetVariableReturn = BigNumber(NaN);
        }
        return util_1.format('return %s% (%s%s %s%s), %s leading %s filled %s% %s%s %s @ %s, %s%s', this.getNetVariableReturn().percentageBN(this.trailingVariableCurrency).toString(), leadingNetVariableReturn.round(2).toString(), this.leadingVariableCurrency, trailingNetVariableReturn.round(2).toString(), this.trailingVariableCurrency, this.leadingOrder.exchangeName, this.leadingOrder.side, this.leadingOrder.lastPartialPercentage, this.leadingOrder.variableAmountLastPartial, this.leadingVariableCurrency, this.leadingOrder.amountLastPartialBN.toFixed(4), this.leadingOrder.priceBN.toFixed(2), descriptionOfTrailingOrders, descriptionOfTrailing2Leading);
    };
    return OrderPair;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = OrderPair;
function calculateAggregatedValues(aggregatedOrders, newOrder) {
    var functionName = 'OrderPair.calculateAggregatedValues()';
    // fixed values
    aggregatedOrders.sumFixedAmountLastPartialGrossBN = aggregatedOrders.sumFixedAmountLastPartialGrossBN.
        plus(newOrder.fixedAmountLastPartialGrossBN);
    aggregatedOrders.sumFixedAmountLastPartialBN = aggregatedOrders.sumFixedAmountLastPartialBN.
        plus(newOrder.fixedAmountLastPartialBN);
    // variable values
    aggregatedOrders.sumVariableAmountLastPartialGrossBN = aggregatedOrders.sumVariableAmountLastPartialGrossBN.
        plus(newOrder.variableAmountLastPartialGrossBN);
    aggregatedOrders.sumVariableAmountLastPartialBN = aggregatedOrders.sumVariableAmountLastPartialBN.
        plus(newOrder.variableAmountLastPartialBN);
    // price using aggregated gross values
    aggregatedOrders.priceGrossBN = aggregatedOrders.sumVariableAmountLastPartialGrossBN.
        div(aggregatedOrders.sumFixedAmountLastPartialGrossBN).
        round(newOrder.exchange.priceRounding[newOrder.symbol]);
    logger.trace('%s gross price %s = aggregated gross variable amount %s / aggregated gross fixed amount %s', functionName, aggregatedOrders.priceGrossBN.toString(), aggregatedOrders.sumVariableAmountLastPartialGrossBN.toString(), aggregatedOrders.sumFixedAmountLastPartialGrossBN.toString());
    // price using aggregated net values
    aggregatedOrders.priceNetBN = aggregatedOrders.sumVariableAmountLastPartialBN.
        div(aggregatedOrders.sumFixedAmountLastPartialBN).
        round(newOrder.exchange.priceRounding[newOrder.symbol]);
    logger.trace('%s net price %s = aggregated net variable amount %s / aggregated net fixed amount %s', functionName, aggregatedOrders.priceNetBN.toString(), aggregatedOrders.sumVariableAmountLastPartialBN.toString(), aggregatedOrders.sumFixedAmountLastPartialBN.toString());
}
