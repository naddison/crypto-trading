/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

import * as _ from 'underscore';
import {format} from "util";
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

// My packages
const logger = require('config-logger'),
    VError = require('verror');

interface IReturnSettings {
    sellCurrency: string;
    sellAmount?: string;

    buyCurrency: string;
    buyAmount?: string;

    amountRounding?: number;
    decimalRounding?: number;
    percentageRounding?: number;

    sell2buyExchangeRate?: string;
}

export default class Return
{
    sellCurrency: string;
    sellAmountBN = BigNumber(0);

    buyCurrency: string;
    buyAmountBN = BigNumber(0);

    amountRounding: number = 8;
    decimalRounding: number = 6;
    percentageRounding: number = 2;

    sell2buyExchangeRateBN = BigNumber(1);

    constructor(settings: IReturnSettings)
    {
        // override the default settings with what's passed to the constructor
        _.extend(this, settings);
    }

    get sellAmount() { return this.sellAmountBN.toString(); }
    set sellAmount(sellAmount) { this.sellAmountBN = new BigNumber(sellAmount); }

    get buyAmount() { return this.buyAmountBN.toString(); }
    set buyAmount(buyAmount) { this.buyAmountBN = new BigNumber(buyAmount); }

    get sell2buyExchangeRate() { return this.sell2buyExchangeRateBN.toString(); }
    set sell2buyExchangeRate(sell2buyExchangeRate) { this.sell2buyExchangeRateBN = new BigNumber(sell2buyExchangeRate); }

    get clone(): Return {
        return new Return({
            sellCurrency:           _.clone(this.sellCurrency),
            sellAmount:             _.clone(this.sellAmount),

            buyCurrency:            _.clone(this.buyCurrency),
            buyAmount:              _.clone(this.buyAmount),

            amountRounding:         _.clone(this.amountRounding),
            decimalRounding:        _.clone(this.decimalRounding),
            percentageRounding:     _.clone(this.percentageRounding),

            sell2buyExchangeRate:   _.clone(this.sell2buyExchangeRate)
        });
    }

    log(): string
    {
        const msg = format('buy %s %s, sell %s %s, sell to buy exchange rate %s',
            this.buyAmount, this.buyCurrency,
            this.sellAmount, this.sellCurrency,
            this.sell2buyExchangeRate);
        return msg;
    }

    amountBN(currency?: string): Error| BigNumber
    {
        const functionName = 'Return.amountBN()';

        let returnAmountBN;

        if (!currency && this.sellCurrency === this.buyCurrency)
        {
            // default to sell currency which is the same as the buy currency
            currency = this.sellCurrency;
        }
        else if (!currency || !(currency === this.buyCurrency || currency === this.sellCurrency))
        {
            const error = VError('%s first parameter currency %s must either be the "buy" currency %s or "sell" currency %s', functionName,
                currency, this.buyCurrency, this.sellCurrency);

            logger.error(error.stack);
            return error;
        }

        if (currency === this.buyCurrency)
        {
            returnAmountBN = this.sellAmountBN.
            times(this.sell2buyExchangeRate).
            minus(this.buyAmount).
            round(this.amountRounding);

            logger.trace('%s return amount %s %s = sell amount %s * sell to buy currency exchange rate %s - buy amount %s', functionName,
                returnAmountBN.toString(), this.buyCurrency,
                this.sellAmount,
                this.sell2buyExchangeRate,
                this.buyAmount);
        }
        else if (currency === this.sellCurrency)
        {
            returnAmountBN = this.sellAmountBN.
            minus(this.buyAmountBN.
                div(this.sell2buyExchangeRate)
            ).
            round(this.amountRounding);

            logger.trace('%s return amount %s %s = sell amount %s - buy amount %s / sell to buy currency exchange rate %s', functionName,
                returnAmountBN.toString(), this.sellCurrency,
                this.sellAmount,
                this.buyAmount,
                this.sell2buyExchangeRate);
        }

        return returnAmountBN;
    }

    decimalBN(currency?: string) : Error | BigNumber
    {
        const functionName = 'Return.decimalBN()';

        let returnDecimalBN;

        if (!currency && this.sellCurrency === this.buyCurrency)
        {
            // default to sell currency which is the same as the buy currency
            currency = this.sellCurrency;
        }
        else if (!currency || !(currency === this.buyCurrency || currency === this.sellCurrency))
        {
            const error = VError('%s first parameter currency %s must either be the buy currency "%s" or sell currency "%s"', functionName,
                currency, this.buyCurrency, this.sellCurrency);

            logger.error(error.stack);
            return error;
        }

        if (currency === this.buyCurrency)
        {
            returnDecimalBN = this.sellAmountBN.
            times(this.sell2buyExchangeRate).
            minus(this.buyAmount).
            div(this.buyAmount).
            round(this.decimalRounding);

            logger.trace('%s return amount %s %s = (sell amount %s * sell to buy currency exchange rate %s - buy amount %s) / buy amount %s', functionName,
                returnDecimalBN.toString(), this.buyCurrency,
                this.sellAmount,
                this.sell2buyExchangeRate,
                this.buyAmount,
                this.buyAmount);
        }
        else if (currency === this.sellCurrency)
        {
            const buyAmountInSellCurrencyBN = this.buyAmountBN.
            div(this.sell2buyExchangeRate);

            logger.trace('%s buy amount in sell currency %s %s = buy amount %s %s / sell to buy exchange rate %s', functionName,
                buyAmountInSellCurrencyBN.toString(), this.sellCurrency,
                this.buyAmount, this.buyCurrency,
                this.sell2buyExchangeRate);

            returnDecimalBN = this.sellAmountBN.
            minus(buyAmountInSellCurrencyBN).
            div(buyAmountInSellCurrencyBN).
            round(this.decimalRounding);

            logger.trace('%s return amount %s %s = (sell amount in variable currency %s - buy amount in variable currency %s / buy amount in variable currency %s', functionName,
                returnDecimalBN.toString(), this.sellCurrency,
                this.sellAmount,
                buyAmountInSellCurrencyBN.toString(),
                buyAmountInSellCurrencyBN.toString() );
        }

        return returnDecimalBN;
    }

    percentageBN(currency?: string): Error | BigNumber
    {
        const functionName = 'Return.percentageBN()';

        if (!currency && this.sellCurrency === this.buyCurrency)
        {
            // default to sell currency which is the same as the buy currency
            currency = this.sellCurrency;
        }
        else if (!currency || !(currency === this.buyCurrency || currency === this.sellCurrency))
        {
            const error = VError('%s first parameter currency %s must either be the "buy" currency %s or "sell" currency %s', functionName,
                currency, this.buyCurrency, this.sellCurrency);

            logger.error(error.stack);
            return error;
        }

        const decimalBN: BigNumber = this.decimalBN(currency) as BigNumber;

        return decimalBN.
            times(100).
            round(Number(this.percentageRounding));
    }
}

