/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
var moment = require('moment');
var _ = require('underscore');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
// import my Interfaces
var exchange_1 = require("./exchange");
var market_1 = require("./market");
// My packages
var logger = require('config-logger'), VError = require('verror');
(function (OrderSide) {
    OrderSide[OrderSide["sell"] = 0] = "sell";
    OrderSide[OrderSide["buy"] = 1] = "buy";
    OrderSide[OrderSide["unknown"] = 2] = "unknown";
})(exports.OrderSide || (exports.OrderSide = {}));
var OrderSide = exports.OrderSide;
(function (OrderType) {
    OrderType[OrderType["limit"] = 0] = "limit";
    OrderType[OrderType["market"] = 1] = "market";
    OrderType[OrderType["stop"] = 2] = "stop";
    OrderType[OrderType["trailingStop"] = 3] = "trailingStop";
    OrderType[OrderType["fillOrKill"] = 4] = "fillOrKill";
})(exports.OrderType || (exports.OrderType = {}));
var OrderType = exports.OrderType;
(function (OrderState) {
    OrderState[OrderState["quoted"] = 0] = "quoted";
    OrderState[OrderState["pending"] = 1] = "pending";
    OrderState[OrderState["pendingPartial"] = 2] = "pendingPartial";
    OrderState[OrderState["partiallyFilled"] = 3] = "partiallyFilled";
    OrderState[OrderState["filled"] = 4] = "filled";
    OrderState[OrderState["cancelled"] = 5] = "cancelled";
})(exports.OrderState || (exports.OrderState = {}));
var OrderState = exports.OrderState;
var Order = (function () {
    function Order(settings) {
        // limit or market
        this.type = "limit";
        // 'quoted', 'pending', 'pendingPartial', 'partiallyFilled', 'filled', 'cancelled', 'expired'
        this.state = "quoted";
        this.numberPartialFills = 0;
        this.minRate = "0";
        this.maxRate = "Infinity";
        // TODO add timestamp for each state
        this.timestamp = new Date();
        this.exchangeId = settings.exchangeId;
        this.originalExchangeId = settings.originalExchangeId || this.exchangeId;
        this.exchangeName = settings.exchangeName;
        this.symbol = settings.symbol;
        this.side = settings.side;
        this.type = settings.type || this.type;
        this.state = settings.state || this.state;
        this.amountBN = (settings.amount ? new BigNumber(settings.amount) : BigNumber(0)); // the fixed currency amount before fess are deducted
        this.amountTradedBN = (settings.amountTraded ? new BigNumber(settings.amountTraded) : BigNumber(0));
        // if not set the use amount
        this.amountRemainingBN = (settings.amountRemaining ? new BigNumber(settings.amountRemaining) : this.amountBN);
        this.amountLastPartialBN = (settings.amountLastPartial ? new BigNumber(settings.amountLastPartial) : BigNumber(0));
        this.numberPartialFills = settings.numberPartialFills || 0;
        if (settings.price) {
            this.priceBN = new BigNumber(settings.price);
        }
        this.tag = settings.tag;
        this.minRate = settings.minRate || this.minRate;
        this.maxRate = settings.maxRate || this.maxRate;
        // validate string values
        if (!_.contains(['buy', 'sell'], this.side)) {
            return null;
        }
        if (!_.contains(['limit', 'market', 'stop', 'trailingStop', 'fillOrKill'], this.type)) {
            return null;
        }
        if (!_.contains(['quoted', 'pending', 'pendingPartial', 'partiallyFilled', 'filled', 'cancelled'], this.state)) {
            return null;
        }
    }
    Object.defineProperty(Order.prototype, "price", {
        get: function () {
            if (this.priceBN) {
                return this.priceBN.toString();
            }
        },
        set: function (price) {
            if (price) {
                this.priceBN = new BigNumber(price);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "amount", {
        get: function () { return this.amountBN.toString(); },
        set: function (amount) { this.amountBN = new BigNumber(amount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "amountTraded", {
        get: function () { return this.amountTradedBN.toString(); },
        set: function (amountTraded) { this.amountTradedBN = new BigNumber(amountTraded); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "amountRemaining", {
        get: function () {
            if (this.amountRemainingBN) {
                return this.amountRemainingBN.toString();
            }
        },
        set: function (amountRemaining) {
            if (amountRemaining) {
                this.amountRemainingBN = new BigNumber(amountRemaining);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "amountLastPartial", {
        get: function () { return this.amountLastPartialBN.toString(); },
        set: function (amountLastPartial) { this.amountLastPartialBN = new BigNumber(amountLastPartial); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buyAmountBN", {
        get: function () { return new BigNumber(this.buy.amount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buyAmountRemainingBN", {
        get: function () { return new BigNumber(this.buy.amountRemaining); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buyAmountTradedBN", {
        get: function () { return new BigNumber(this.buy.amountTraded); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buyAmountLastPartialBN", {
        get: function () { return new BigNumber(this.buy.amountLastPartial); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buyAmountGrossBN", {
        get: function () { return new BigNumber(this.buy.amountGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buyAmountRemainingGrossBN", {
        get: function () { return new BigNumber(this.buy.amountRemainingGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buyAmountTradedGrossBN", {
        get: function () { return new BigNumber(this.buy.amountTradedGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buyAmountLastPartialGrossBN", {
        get: function () { return new BigNumber(this.buy.amountLastPartialGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sellAmountBN", {
        get: function () { return new BigNumber(this.sell.amount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sellAmountRemainingBN", {
        get: function () { return new BigNumber(this.sell.amountRemaining); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sellAmountTradedBN", {
        get: function () { return new BigNumber(this.sell.amountTraded); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sellAmountLastPartialBN", {
        get: function () { return new BigNumber(this.sell.amountLastPartial); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sellAmountGrossBN", {
        get: function () { return new BigNumber(this.sell.amountGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sellAmountRemainingGrossBN", {
        get: function () { return new BigNumber(this.sell.amountRemainingGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sellAmountTradedGrossBN", {
        get: function () { return new BigNumber(this.sell.amountTradedGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sellAmountLastPartialGrossBN", {
        get: function () { return new BigNumber(this.sell.amountLastPartialGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "feeAmountBN", {
        get: function () { return new BigNumber(this.fee.amount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "feeAmountRemainingBN", {
        get: function () { return new BigNumber(this.fee.amountRemaining); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "feeAmountTradedBN", {
        get: function () { return new BigNumber(this.fee.amountTraded); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "feeAmountLastPartialBN", {
        get: function () { return new BigNumber(this.fee.amountLastPartial); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountBN", {
        get: function () { return new BigNumber(this.fixedAmount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountRemainingBN", {
        get: function () { return new BigNumber(this.fixedAmountRemaining); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountTradedBN", {
        get: function () { return new BigNumber(this.fixedAmountTraded); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountLastPartialBN", {
        get: function () { return new BigNumber(this.fixedAmountLastPartial); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountGrossBN", {
        get: function () { return new BigNumber(this.fixedAmountGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountRemainingGrossBN", {
        get: function () { return new BigNumber(this.fixedAmountRemainingGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountTradedGrossBN", {
        get: function () { return new BigNumber(this.fixedAmountTradedGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountLastPartialGrossBN", {
        get: function () { return new BigNumber(this.fixedAmountLastPartialGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedFeeAmountBN", {
        get: function () { return new BigNumber(this.fee.fixedAmount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedFeeAmountRemainingBN", {
        get: function () { return new BigNumber(this.fee.fixedAmountRemaining); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedFeeAmountTradedBN", {
        get: function () { return new BigNumber(this.fee.fixedAmountTraded); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedFeeAmountLastPartialBN", {
        get: function () { return new BigNumber(this.fee.fixedAmountLastPartial); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountBN", {
        get: function () { return new BigNumber(this.variableAmount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountRemainingBN", {
        get: function () { return new BigNumber(this.variableAmountRemaining); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountTradedBN", {
        get: function () { return new BigNumber(this.variableAmountTraded); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountLastPartialBN", {
        get: function () { return new BigNumber(this.variableAmountLastPartial); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountGrossBN", {
        get: function () { return new BigNumber(this.variableAmountGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountRemainingGrossBN", {
        get: function () { return new BigNumber(this.variableAmountRemainingGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountTradedGrossBN", {
        get: function () { return new BigNumber(this.variableAmountTradedGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountLastPartialGrossBN", {
        get: function () { return new BigNumber(this.variableAmountLastPartialGross); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "feeVariableAmountBN", {
        get: function () { return new BigNumber(this.fee.variableAmount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "minRateBN", {
        get: function () { return new BigNumber(this.minRate); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "maxRateBN", {
        get: function () { return new BigNumber(this.maxRate); },
        enumerable: true,
        configurable: true
    });
    Order.prototype.clone = function () {
        var clonedOrder = new Order({
            exchangeId: _.clone(this.exchangeId),
            originalExchangeId: _.clone(this.originalExchangeId),
            exchangeName: _.clone(this.exchangeName),
            symbol: _.clone(this.symbol),
            side: _.clone(this.side),
            type: _.clone(this.type),
            state: _.clone(this.state),
            amount: _.clone(this.amount),
            amountTraded: _.clone(this.amountTraded),
            amountRemaining: _.clone(this.amountRemaining),
            price: _.clone(this.price),
            amountLastPartial: _.clone(this.amountLastPartial),
            numberPartialFills: _.clone(this.numberPartialFills),
            timestamp: new Date(this.timestamp.getTime()),
            tag: _.clone(this.tag),
            minRate: _.clone(this.minRate),
            maxRate: _.clone(this.maxRate)
        });
        return clonedOrder;
    };
    Object.defineProperty(Order.prototype, "jsObject", {
        get: function () {
            return {
                exchangeId: _.clone(this.exchangeId),
                originalExchangeId: _.clone(this.originalExchangeId),
                exchangeName: _.clone(this.exchangeName),
                symbol: _.clone(this.symbol),
                side: _.clone(this.side),
                state: _.clone(this.state),
                price: _.clone(this.price),
                timestamp: new Date(this.timestamp.getTime()),
                tag: _.clone(this.tag),
                minRate: _.clone(this.minRate),
                maxRate: _.clone(this.maxRate),
                amount: _.clone(this.amount),
                amountTraded: _.clone(this.amountTraded),
                amountRemaining: _.clone(this.amountRemaining),
                buyAmount: _.clone(this.buy.amount),
                buyCurrency: _.clone(this.buy.currency),
                sellAmount: _.clone(this.sell.amount),
                sellCurrency: _.clone(this.sell.currency),
                feeAmount: _.clone(this.fee.amount),
                feeCurrency: _.clone(this.fee.currency),
                amountLastPartial: _.clone(this.amountLastPartial),
                numberPartialFills: _.clone(this.numberPartialFills)
            };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "exchange", {
        // Create reference to the exchange which is not saved into the database
        get: function () {
            var functionName = 'Order.exchange()', returnedExchange = exchange_1.default.exchanges[this.exchangeName];
            if (!returnedExchange) {
                var error = new VError('%s exchange %s can not be found in the static Exchange.exchanges list for %s %s order with id %s, tag %s, amount %s, remaining amount %s, last partial amount %s', functionName, this.exchangeName, this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);
                logger.debug(error);
                throw error;
            }
            return returnedExchange;
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Order.prototype, "market", {
        // Create reference to the market which is not saved into the database
        get: function () {
            var functionName = 'Order.market()', returnedMarket = this.exchange.markets[this.symbol];
            if (!returnedMarket) {
                var error = new VError('%s market %s can not be found on the %s exchange for %s %s order with id %s, tag %s, amount %s, remaining amount %s, last partial amount %s', functionName, this.symbol, this.exchange.name, this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);
                logger.debug(error);
                throw error;
            }
            return returnedMarket;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedCurrency", {
        get: function () { return market_1.default.getFixedCurrency(this.symbol); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableCurrency", {
        get: function () { return market_1.default.getVariableCurrency(this.symbol); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "pending", {
        get: function () { return this.state === "pending"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "filled", {
        get: function () { return this.state === "filled" || this.state === "partiallyFilled"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "tradedRatioBN", {
        // traded amount compared to trade amount
        get: function () {
            // percentage of the order filled = traded amount / trade amount * 100
            return this.amountTradedBN.
                div(this.amountBN);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "tradedPercentageBN", {
        get: function () {
            return this.tradedRatioBN.
                times(100);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "tradedPercentage", {
        get: function () {
            return this.tradedPercentageBN.
                round(3).
                toString();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "remainingRatioBN", {
        // remaining amount compared to trade amount
        get: function () {
            return this.amountRemainingBN.
                div(this.amountBN);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "remainingPercentageBN", {
        get: function () {
            return this.remainingRatioBN.
                times(100);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "remainingPercentage", {
        get: function () {
            return this.remainingPercentageBN.
                round(3).toString();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "lastPartialRatioBN", {
        // last partial amount compared to trade amount
        get: function () {
            return this.amountLastPartialBN.
                div(this.amountBN);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "lastPartialPercentageBN", {
        get: function () {
            return this.lastPartialRatioBN.
                times(100);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "lastPartialPercentage", {
        get: function () {
            return this.lastPartialPercentageBN.
                round(3).toString();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "buy", {
        get: function () {
            var functionName = 'Order.buy()';
            var order = this;
            var BuyTradeAmounts = (function () {
                function BuyTradeAmounts() {
                }
                Object.defineProperty(BuyTradeAmounts.prototype, "currency", {
                    get: function () {
                        // set currency
                        if (order.side === 'buy') {
                            return order.fixedCurrency;
                        }
                        else if (order.side === 'sell') {
                            return order.variableCurrency;
                        }
                        else {
                            var error = new VError("%s can't get buy.currency on order with id %s as side %s is not buy or sell", functionName, order.exchangeId, order.side);
                            logger.error(error.stack);
                            return error;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BuyTradeAmounts.prototype, "amount", {
                    get: function () { return order.calculateBuyAmount(order.amount, 'amount'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BuyTradeAmounts.prototype, "amountRemaining", {
                    get: function () { return order.calculateBuyAmount(order.amountRemaining, 'amountRemaining'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BuyTradeAmounts.prototype, "amountTraded", {
                    get: function () { return order.calculateBuyAmount(order.amountTraded, 'amountTraded'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BuyTradeAmounts.prototype, "amountLastPartial", {
                    get: function () { return order.calculateBuyAmount(order.amountLastPartial, 'amountLastPartial'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BuyTradeAmounts.prototype, "amountGross", {
                    get: function () { return order.calculateBuyAmount(order.amount, 'amountGross', true); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BuyTradeAmounts.prototype, "amountRemainingGross", {
                    get: function () { return order.calculateBuyAmount(order.amountRemaining, 'amountRemainingGross', true); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BuyTradeAmounts.prototype, "amountTradedGross", {
                    get: function () { return order.calculateBuyAmount(order.amountTraded, 'amountTradedGross', true); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BuyTradeAmounts.prototype, "amountLastPartialGross", {
                    get: function () { return order.calculateBuyAmount(order.amountLastPartial, 'amountLastPartialGross', true); },
                    enumerable: true,
                    configurable: true
                });
                return BuyTradeAmounts;
            })();
            var tradeAmounts = new BuyTradeAmounts();
            return tradeAmounts;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "sell", {
        get: function () {
            var functionName = 'Order.sell()';
            var order = this;
            var SellTradeAmounts = (function () {
                function SellTradeAmounts() {
                }
                Object.defineProperty(SellTradeAmounts.prototype, "currency", {
                    get: function () {
                        // set currency
                        if (order.side === 'sell') {
                            return order.fixedCurrency;
                        }
                        else if (order.side === 'buy') {
                            return order.variableCurrency;
                        }
                        else {
                            var error = new VError("%s can't get sell.currency on order with id %s as side %s is not buy or sell", functionName, order.exchangeId, order.side);
                            logger.error(error.stack);
                            return error;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SellTradeAmounts.prototype, "amount", {
                    get: function () { return order.calculateSellAmount(order.amount, 'amount'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SellTradeAmounts.prototype, "amountRemaining", {
                    get: function () { return order.calculateSellAmount(order.amountRemaining, 'amountRemaining'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SellTradeAmounts.prototype, "amountTraded", {
                    get: function () { return order.calculateSellAmount(order.amountTraded, 'amountTraded'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SellTradeAmounts.prototype, "amountLastPartial", {
                    get: function () { return order.calculateSellAmount(order.amountLastPartial, 'amountLastPartial'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SellTradeAmounts.prototype, "amountGross", {
                    get: function () { return order.calculateSellAmount(order.amount, 'amountGross', true); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SellTradeAmounts.prototype, "amountRemainingGross", {
                    get: function () { return order.calculateSellAmount(order.amountRemaining, 'amountRemainingGross', true); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SellTradeAmounts.prototype, "amountTradedGross", {
                    get: function () { return order.calculateSellAmount(order.amountTraded, 'amountTradedGross', true); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SellTradeAmounts.prototype, "amountLastPartialGross", {
                    get: function () { return order.calculateSellAmount(order.amountLastPartial, 'amountLastPartialGross', true); },
                    enumerable: true,
                    configurable: true
                });
                return SellTradeAmounts;
            })();
            var tradeAmounts = new SellTradeAmounts();
            return tradeAmounts;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fee", {
        get: function () {
            var functionName = 'Order.fee()';
            var order = this;
            var FeeFixVarTradeAmounts = (function () {
                function FeeFixVarTradeAmounts() {
                }
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "currency", {
                    get: function () {
                        if (order.exchange.feeStructure === 'buyCurrency') {
                            return order.buy.currency;
                        }
                        else if (order.exchange.feeStructure === 'variableCurrency') {
                            if (order.side === 'sell')
                                return order.buy.currency;
                            else if (order.side === 'buy')
                                return order.sell.currency;
                        }
                        else if (order.exchange.feeStructure === 'fixedCurrency') {
                            if (order.side === 'sell')
                                return order.sell.currency;
                            else if (order.side === 'buy')
                                return order.buy.currency;
                        }
                        else if (order.exchange.feeStructure === 'sellCurrency') {
                            return order.sell.currency;
                        }
                        else {
                            var error = new VError('%s could not return fee currency. The fee structure %s on the %s exchange is invalid.', functionName, order.exchange.feeStructure, order.exchange.name);
                            logger.error(error.stack);
                            return error;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "amount", {
                    get: function () { return order.calculateFeeAmount(order.amount, 'amount'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "amountRemaining", {
                    get: function () { return order.calculateFeeAmount(order.amountRemaining, 'amountRemaining'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "amountTraded", {
                    get: function () { return order.calculateFeeAmount(order.amountTraded, 'amountTraded'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "amountLastPartial", {
                    get: function () { return order.calculateFeeAmount(order.amountLastPartial, 'amountLastPartial'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "fixedAmount", {
                    get: function () { return order.calculateFeeFixedAmount(this.amount, 'fee.amount'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "fixedAmountRemaining", {
                    get: function () { return order.calculateFeeFixedAmount(this.amountRemaining, 'fee.amountTraded'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "fixedAmountTraded", {
                    get: function () { return order.calculateFeeFixedAmount(this.amountTraded, 'fee.amountTraded'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "fixedAmountLastPartial", {
                    get: function () { return order.calculateFeeFixedAmount(this.amountLastPartial, 'fee.amountLastPartial'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "variableAmount", {
                    get: function () { return order.calculateFeeVariableAmount(this.amount, 'fee.amount'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "variableAmountRemaining", {
                    get: function () { return order.calculateFeeVariableAmount(this.amountRemaining, 'fee.amountTraded'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "variableAmountTraded", {
                    get: function () { return order.calculateFeeVariableAmount(this.amountTraded, 'fee.amountTraded'); },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FeeFixVarTradeAmounts.prototype, "variableAmountLastPartial", {
                    get: function () { return order.calculateFeeVariableAmount(this.amountLastPartial, 'fee.amountLastPartial'); },
                    enumerable: true,
                    configurable: true
                });
                return FeeFixVarTradeAmounts;
            })();
            var tradeAmounts = new FeeFixVarTradeAmounts();
            return tradeAmounts;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmount", {
        get: function () {
            return this.calculateFixedAmount('amount');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountRemaining", {
        get: function () {
            return this.calculateFixedAmount('amountRemaining');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountTraded", {
        get: function () {
            return this.calculateFixedAmount('amountTraded');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountLastPartial", {
        get: function () {
            return this.calculateFixedAmount('amountLastPartial');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmount", {
        get: function () {
            return this.calculateVariableAmount('amount');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountRemaining", {
        get: function () {
            return this.calculateVariableAmount('amountRemaining');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountTraded", {
        get: function () {
            return this.calculateVariableAmount('amountTraded');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountLastPartial", {
        get: function () {
            return this.calculateVariableAmount('amountLastPartial');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountGross", {
        get: function () {
            return this.calculateFixedAmount('amountGross');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountRemainingGross", {
        get: function () {
            return this.calculateFixedAmount('amountRemainingGross');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountTradedGross", {
        get: function () {
            return this.calculateFixedAmount('amountTradedGross');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "fixedAmountLastPartialGross", {
        get: function () {
            return this.calculateFixedAmount('amountLastPartialGross');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountGross", {
        get: function () {
            return this.calculateVariableAmount('amountGross');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountRemainingGross", {
        get: function () {
            return this.calculateVariableAmount('amountRemainingGross');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountTradedGross", {
        get: function () {
            return this.calculateVariableAmount('amountTradedGross');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Order.prototype, "variableAmountLastPartialGross", {
        get: function () {
            return this.calculateVariableAmount('amountLastPartialGross');
        },
        enumerable: true,
        configurable: true
    });
    Order.prototype.getCurrencyAmountBN = function (currency) {
        var functionName = util_1.format('Order.getCurrencyAmountBN');
        if (this.fixedCurrency === currency) {
            return this.fixedAmountBN;
        }
        else if (this.variableCurrency === currency) {
            return this.variableAmountBN;
        }
        else {
            var error = new VError('%s currency %s is not fixed currency %s or variable currency %s', functionName, currency, this.fixedCurrency, this.variableCurrency);
            logger.error(error.stack);
            return error;
        }
    };
    Order.prototype.getCurrencyAmountRemainingBN = function (currency) {
        var functionName = util_1.format('Order.getCurrencyAmountRemainingBN');
        if (this.fixedCurrency === currency) {
            return this.fixedAmountRemainingBN;
        }
        else if (this.variableCurrency === currency) {
            return this.variableAmountRemainingBN;
        }
        else {
            var error = new VError('%s currency %s is not fixed currency %s or variable currency %s', functionName, currency, this.fixedCurrency, this.variableCurrency);
            logger.error(error.stack);
            return error;
        }
    };
    // TODO set return type to string and change return of NaN
    // used to convert various amounts like amountRemaining, amountLastPartial and amountTraded into the buy amount
    Order.prototype.calculateBuyAmount = function (amount, amountName, gross) {
        if (gross === void 0) { gross = false; }
        var functionName = util_1.format('Order.calculateBuyAmount');
        var buyAmount, commissionRate = 0;
        if (this.side === 'buy') {
            // if net buy amount and fee amount is in fixed currency
            if (!gross && (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'fixedCurrency')) {
                commissionRate = this.getCommission(this.fixedCurrency);
                buyAmount = new BigNumber(amount).
                    times(BigNumber(1).
                    minus(commissionRate)).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.trace('%s buy.%s of buy order %s = amount %s * (1 - %s commission %s)', functionName, amountName, buyAmount, amount, this.fixedCurrency, commissionRate);
            }
            else {
                buyAmount = new BigNumber(amount).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.trace('%s buy.%s of buy order %s = amount %s', functionName, amountName, buyAmount, amount);
            }
        }
        else if (this.side === 'sell') {
            // TODO move the validation of the price into a separate function
            // if no price was set, which will be the case for pending market orders.
            // Filled market orders should have an price
            if (!this.price) {
                var error = new VError('%s price not set for %s %s order with id %s, tag %s, trade amount %s, remaining amount %s, last partial amount %s', functionName, this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);
                logger.debug(error.stack);
                return NaN;
            }
            // if net buy amount and fee is charged in variable currency
            if (!gross && (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'variableCurrency')) {
                commissionRate = this.getCommission(this.variableCurrency);
                // calculate the nett amount
                buyAmount = new BigNumber(amount).
                    times(this.price).
                    times(BigNumber(1).
                    minus(commissionRate)).
                    round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.trace('%s buy.%s of sell order %s = amount %s * price %s * (1 - %s commission %s)', functionName, amountName, buyAmount, amount, this.price, this.variableCurrency, commissionRate);
            }
            else {
                // calculate the gross amount
                buyAmount = new BigNumber(amount).
                    times(this.price).
                    round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.trace('%s buy.%s of sell order %s = trade amount %s * price %s', functionName, amountName, buyAmount, amount, this.price);
            }
        }
        else {
            var error = new VError("%s can't get buy.%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName, amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            buyAmount = error;
        }
        return buyAmount;
    };
    // TODO set return type to string and change return of NaN
    // used to convert various amounts like amountRemaining, amountLastPartial and amountTraded into the sell amount
    Order.prototype.calculateSellAmount = function (tradeAmount, amountName, gross) {
        if (gross === void 0) { gross = false; }
        var functionName = util_1.format('Order.calculateSellAmount');
        var sellAmount, commissionRate = 0;
        if (this.side === 'buy') {
            // TODO move the validation of the price into a separate function
            // if no price was set, which will be the case for pending market orders.
            // Filled market trades should have an price
            if (!this.price) {
                var error = new VError('%s price not set for %s %s order with id %s, tag %s, amount %s, remaining amount %s, last partial amount %s', functionName, this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);
                logger.debug(error);
                return NaN;
            }
            // if net amount and fee is in variable currency
            if (!gross && (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'sellCurrency')) {
                commissionRate = this.getCommission(this.variableCurrency);
                sellAmount = new BigNumber(tradeAmount).
                    times(this.price).
                    times(BigNumber(1).
                    plus(commissionRate)).
                    round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.trace('%s sell.%s of buy order %s %s = trade amount %s * price %s * (1 + %s commission %s)', functionName, amountName, sellAmount, this.variableCurrency, tradeAmount, this.price, this.variableCurrency, commissionRate);
            }
            else {
                sellAmount = new BigNumber(tradeAmount).
                    times(this.price).
                    round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.trace('%s sell.%s of buy order %s %s = trade amount %s * price %s', functionName, amountName, sellAmount, this.variableCurrency, tradeAmount, this.price);
            }
        }
        else if (this.side === 'sell') {
            // if net amount and fee is in fixed currency
            if (!gross && (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'sellCurrency')) {
                commissionRate = this.getCommission(this.fixedCurrency);
                sellAmount = new BigNumber(tradeAmount).
                    times(BigNumber(1).
                    plus(commissionRate)).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.trace('%s sell.%s of sell order %s %s = trade amount %s * (1 + %s commission %s)', functionName, amountName, sellAmount, this.fixedCurrency, tradeAmount, this.fixedCurrency, commissionRate);
            }
            else {
                sellAmount = BigNumber(tradeAmount).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.trace('%s sell.%s of sell order %s = trade amount %s', functionName, amountName, sellAmount, tradeAmount);
            }
        }
        else {
            var error = new VError("%s can't get sell.%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName, amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            sellAmount = error;
        }
        return sellAmount;
    };
    // TODO set return type to string and change return of NaN
    // used to convert various amounts like amountRemaining, amountLastPartial and amountTraded into the fee amount
    Order.prototype.calculateFeeAmount = function (tradeAmount, amountName) {
        var functionName = util_1.format('Order.calculateFeeAmount');
        var feeAmount, commissionRate = 0;
        // if fee is in fixed currency
        if (this.exchange.feeStructure === 'fixedCurrency' ||
            (this.side === 'buy' && this.exchange.feeStructure === 'buyCurrency') ||
            (this.side === 'sell' && this.exchange.feeStructure === 'sellCurrency')) {
            commissionRate = this.getCommission(this.fixedCurrency);
            feeAmount = new BigNumber(tradeAmount).
                times(commissionRate).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.trace('%s fee.%s of buy order %s %s = trade amount %s * fixed currency %s commission rate %s', functionName, amountName, feeAmount, this.fixedCurrency, tradeAmount, this.fixedCurrency, commissionRate);
        }
        else if (this.exchange.feeStructure === 'variableCurrency' ||
            (this.side === 'sell' && this.exchange.feeStructure === 'buyCurrency') ||
            (this.side === 'buy' && this.exchange.feeStructure === 'sellCurrency')) {
            // if no price was set, which will be the case for pending market orders.
            // Filled market trades should have an price
            if (!this.price) {
                var error = new VError('%s price not set for %s %s order with id %s, tag %s, amount %s, remaining amount %s, last partial amount %s', functionName, this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);
                logger.debug(error);
                return NaN;
            }
            commissionRate = this.getCommission(this.variableCurrency);
            feeAmount = new BigNumber(tradeAmount).
                times(this.price).
                times(commissionRate).
                round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.trace('%s fee.%s of buy order %s %s = trade amount %s * price %s * variable currency %s commission rate %s', functionName, amountName, feeAmount, this.variableCurrency, tradeAmount, this.price, this.variableCurrency, commissionRate);
        }
        else {
            var error = new VError("%s can't get sell.%s on order with id %s as side '%s' is not 'buy' or 'sell'; or feeStructure %s is not 'buyCurrency', 'sellCurrency', 'fixedCurrency' or 'variableCurrency'", functionName, amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            feeAmount = error;
        }
        return feeAmount;
    };
    // used to convert various trade amounts like amountRemaining, amountLastPartial and amountTraded into the fixed currency amount
    Order.prototype.calculateFixedAmount = function (amountName) {
        var functionName = util_1.format('Order.calculateFixedAmount');
        var fixedAmount;
        if (this.side === 'buy') {
            fixedAmount = this.buy[amountName];
            logger.trace('%s fixed.%s of buy order %s = buy.%s %s', functionName, amountName, fixedAmount, amountName, this.buy[amountName]);
        }
        else if (this.side === 'sell') {
            fixedAmount = this.sell[amountName];
            logger.trace('%s fixed.%s of sell order %s = sell.%s %s', functionName, amountName, fixedAmount, amountName, this.sell[amountName]);
        }
        else {
            var error = new VError("%s can't get fixed.%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName, amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            fixedAmount = error;
        }
        return fixedAmount;
    };
    /**
     * Converts an amount into a variable currency
     * If fees are in variable currency, '0' is returned
     * @param tradeAmount trade amounts like amountRemaining, amountLastPartial and amountTraded
     * @param amountName
     * @returns amount in variable as a string
     */
    Order.prototype.calculateVariableAmount = function (amountName) {
        var functionName = util_1.format('Order.calculateVariableAmount');
        var variableAmount;
        if (this.side === 'buy') {
            variableAmount = this.sell[amountName];
            logger.trace('%s variable.%s of buy order %s = sell.%s %s', functionName, amountName, variableAmount, amountName, this.sell[amountName]);
        }
        else if (this.side === 'sell') {
            variableAmount = this.buy[amountName];
            logger.trace('%s variable.%s of sell order %s = buy.%s %s', functionName, amountName, variableAmount, amountName, this.buy[amountName]);
        }
        else {
            var error = new VError("%s can't get variable.%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName, amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            variableAmount = error;
        }
        return variableAmount;
    };
    /**
     * Returns any fees that are charged in fixed currency.
     * If fees are in variable currency for this order then '0' is returned
     * @param feeAmount fee amounts like fee.amountRemaining, fee.amountLastPartial and fee.amountTraded
     * @param amountName only used for logging. eg 'fee.amountRemaining', 'fee.amountLastPartial' and 'fee.amountTraded'
     * @returns fee in string format
     */
    Order.prototype.calculateFeeFixedAmount = function (feeAmount, amountName) {
        var functionName = util_1.format('Order.calculateFeeFixed%s', amountName);
        var feeFixedAmount;
        if (this.fee.currency === this.fixedCurrency) {
            feeFixedAmount = new BigNumber(feeAmount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP).
                toString();
            logger.trace('%s fixed currency %s fee %s = fee amount %s', functionName, this.fixedCurrency, feeFixedAmount, feeAmount);
        }
        else if (this.fee.currency === this.variableCurrency) {
            feeFixedAmount = '0';
        }
        else {
            var error = new VError("%s can't get fee.fixed%s on order with id %s as fee currency %s does not match this trades fixed %s or variable %s currencies", functionName, amountName, this.exchangeId, this.fee.currency, this.fixedCurrency, this.variableCurrency);
            logger.error(error.stack);
            feeFixedAmount = error;
        }
        return feeFixedAmount;
    };
    // used to convert various fee amounts like fee.amountRemaining, fee.amountLastPartial and fee.amountTraded into the variable currency amount
    /**
     * Returns any fees that are charged in variable currency.
     * If fees are in fixed currency for this order then '0' is returned
     * @param feeAmount fee amounts like fee.amountRemaining, fee.amountLastPartial and fee.amountTraded
     * @param amountName only used for logging. eg 'fee.amountRemaining', 'fee.amountLastPartial' and 'fee.amountTraded'
     * @returns fee in string format
     */
    Order.prototype.calculateFeeVariableAmount = function (tradeAmount, amountName) {
        var functionName = util_1.format('Order.calculateFeeVariable%s', amountName);
        var feeVariableAmount;
        if (this.fee.currency === this.fixedCurrency) {
            feeVariableAmount = '0';
        }
        else if (this.fee.currency === this.variableCurrency) {
            feeVariableAmount = new BigNumber(tradeAmount).
                round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                toString();
        }
        else {
            var error = new VError("%s can't get fee.variable%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName, amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            feeVariableAmount = error;
        }
        return feeVariableAmount;
    };
    /**
     * Gets the maker or taker commission for from the exchange the order is against
     * @param currency the currency the fee will be charged in. This depends on the order side and exchange fee structure
     * @returns commission in decimal format. eg 0.001 which is 0.1%
     */
    Order.prototype.getCommission = function (currency) {
        var functionName = util_1.format('Order.getCommission');
        if (!currency) {
            var error = new VError('%s currency parameter %s has not been passed', functionName, currency);
            logger.error(error.stack);
            return error;
        }
        if (this.type === 'limit') {
            return this.exchange.commissions[currency].maker;
        }
        else if (this.type === 'market') {
            return this.exchange.commissions[currency].taker;
        }
    };
    // TODO add unit tests
    Order.prototype.getVariableCommissionBN = function () {
        var functionName = util_1.format('Order.getVariableCommissionBN');
        var variableCommissionBN = new BigNumber(0);
        if (this.side === 'sell' &&
            (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'variableCurrency')) {
            // negative amount
            variableCommissionBN = new BigNumber(this.getCommission(this.variableCurrency)).negated();
        }
        else if (this.side === 'buy' &&
            (this.exchange.feeStructure === 'sellCurrency' || this.exchange.feeStructure === 'variableCurrency')) {
            // positive amount
            variableCommissionBN = new BigNumber(this.getCommission(this.variableCurrency));
        }
        logger.trace('%s variable commission = %s', functionName, variableCommissionBN.toString());
        return variableCommissionBN;
    };
    // TODO add unit tests
    Order.prototype.getFixedCommissionBN = function () {
        var functionName = util_1.format('Order.getFixedCommissionBN');
        var fixedCommissionBN = new BigNumber(0);
        if (this.side === 'buy' &&
            (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'fixedCurrency')) {
            // negative amount
            fixedCommissionBN = new BigNumber(this.getCommission(this.fixedCurrency)).negated();
        }
        else if (this.side === 'sell' &&
            (this.exchange.feeStructure === 'sellCurrency' || this.exchange.feeStructure === 'fixedCurrency')) {
            // positive amount
            fixedCommissionBN = new BigNumber(this.getCommission(this.fixedCurrency));
        }
        logger.trace('%s fixed commission = %s', functionName, fixedCommissionBN.toString());
        return fixedCommissionBN;
    };
    Order.prototype.setAmountsFromNetSellAmount = function (sellAmountBN) {
        var functionName = 'Order.setAmountsFromNetSellAmount()';
        var tradeAmountBN, commissionRate;
        if (!sellAmountBN || !(sellAmountBN instanceof BigNumber)) {
            var error = new VError('%s parameter sellAmountBN %s needs to be passed and be of type BigNumber', functionName, sellAmountBN);
            logger.error(error.stack);
            return error;
        }
        if (this.amount || this.amountRemaining || this.amountTraded !== '0' || this.amountLastPartial !== '0') {
            logger.debug('%s existing trade amounts %s, remaining %s, traded %s and last partial %s are about to be overridden to get a sell amount of %s', functionName, this.amount, this.amountRemaining, this.amountTraded, this.amountLastPartial, sellAmountBN.toString());
        }
        // if sell order then sell amount is in fixed currency
        if (this.side === 'sell') {
            // if fees are charged in sell amount
            if (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'sellCurrency') {
                commissionRate = this.getCommission(this.fixedCurrency);
                tradeAmountBN = sellAmountBN.
                    div(BigNumber(1).
                    plus(commissionRate)).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
                logger.trace('%s new trade amount %s = net sell amount %s / (1 + fixed currency %s commission %s)', functionName, tradeAmountBN.toString(), sellAmountBN.toString(), this.fixedCurrency, commissionRate);
            }
            else if (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'buyCurrency') {
                tradeAmountBN = sellAmountBN.
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
            }
        }
        else if (this.side === 'buy') {
            if (!this.price) {
                var error = new VError('%s price is not defined. This is needed to convert the variable currency net sell amount to fixed currency trade amounts', functionName);
                logger.error(error.stack);
                return error;
            }
            if (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'sellCurrency') {
                commissionRate = this.getCommission(this.variableCurrency);
                tradeAmountBN = sellAmountBN.
                    div(this.price).
                    div(BigNumber(1).
                    plus(commissionRate)).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
                logger.trace('%s new trade amount %s = net sell amount %s / (price %s * (1 + variable currency %s commission %s))', functionName, tradeAmountBN.toString(), sellAmountBN.toString(), this.price, this.variableCurrency, commissionRate);
            }
            else if (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'buyCurrency') {
                tradeAmountBN = sellAmountBN.
                    div(this.price).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN);
                logger.trace('%s new trade amount %s = net sell amount %s / price %s', functionName, tradeAmountBN.toString(), sellAmountBN.toString(), this.price);
            }
        }
        this.amount = tradeAmountBN.toString();
        this.amountRemaining = this.amount;
        this.amountTraded = '0';
        this.amountLastPartial = '0';
        this.numberPartialFills = 0;
    };
    Order.prototype.setAmountsFromNetBuyAmount = function (buyAmountBN) {
        var functionName = 'Order.setAmountsFromNetBuyAmount()';
        var tradeAmountBN, commissionRate;
        if (!buyAmountBN || !(buyAmountBN instanceof BigNumber)) {
            var error = new VError('%s parameter buyAmountBN %s needs to be passed and be of type BigNumber', functionName, buyAmountBN);
            logger.error(error.stack);
            return error;
        }
        if (this.amount || this.amountRemaining || this.amountTraded || this.amountLastPartial) {
            logger.debug('%s existing trade amounts %s, remaining %s, traded %s and last partial %s are about to be overridden to get a buy amount of %s', functionName, buyAmountBN.toString());
        }
        // if sell order then net buy amount is in variable currency so will need converting to fixed currency trade amounts
        if (this.side === 'sell') {
            if (!this.price) {
                var error = new VError('%s price is not defined. This is needed to convert the variable currency net buy amount to fixed currency trade amounts', functionName);
                logger.error(error.stack);
                return error;
            }
            // if fees are NOT changed in buy amount
            if (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'sellCurrency') {
                tradeAmountBN = buyAmountBN.
                    div(this.price).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN);
                logger.trace('%s new trade amount %s = net buy amount %s / price %s', functionName, tradeAmountBN.toString(), buyAmountBN.toString(), this.price);
            }
            else if (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'buyCurrency') {
                commissionRate = this.getCommission(this.variableCurrency);
                tradeAmountBN = buyAmountBN.
                    div(this.price).
                    div(BigNumber(1).
                    minus(commissionRate)).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
                logger.trace('%s new trade amount %s = net buy amount %s / (price %s * (1 - variable currency %s commission %s))', functionName, tradeAmountBN.toString(), buyAmountBN.toString(), this.price, this.variableCurrency, commissionRate);
            }
        }
        else if (this.side === 'buy') {
            // if fees are NOT changed in net buy amount
            if (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'sellCurrency') {
                tradeAmountBN = buyAmountBN.
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
            }
            else if (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'buyCurrency') {
                commissionRate = this.getCommission(this.fixedCurrency);
                tradeAmountBN = buyAmountBN.
                    div(BigNumber(1).
                    minus(commissionRate)).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
                logger.trace('%s new trade amount %s = net buy amount %s / (1 - fixed currency %s commission %s)', functionName, tradeAmountBN.toString(), buyAmountBN.toString(), this.fixedCurrency, commissionRate);
            }
        }
        this.amount = tradeAmountBN.toString();
        this.amountRemaining = this.amount;
        this.amountTraded = '0';
        this.amountLastPartial = '0';
        this.numberPartialFills = 0;
    };
    Order.prototype.setAmountsFromNetFixedAmount = function (fixedAmountBN) {
        var functionName = 'Order.setAmountsFromNetFixedAmount()';
        var tradeAmountBN, commissionRate = 0; // default the commission to zero
        if (!fixedAmountBN || !(fixedAmountBN instanceof BigNumber)) {
            var error = new VError('%s parameter fixedAmountBN %s needs to be passed and be of type BigNumber', functionName, fixedAmountBN);
            logger.error(error.stack);
            return error;
        }
        if (this.amount || this.amountRemaining || this.amountTraded !== "0" || this.amountLastPartial !== "0") {
            logger.debug('%s existing trade amounts %s, remaining %s, traded %s and last partial %s are about to be overridden to get a fixed amount of %s', functionName, fixedAmountBN.toString());
        }
        // if sell order and fee charged in fixed currency
        if (this.side === 'sell' &&
            (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'sellCurrency')) {
            // fee is added to trade amount to get net fixed amount
            commissionRate = this.getCommission(this.fixedCurrency);
        }
        else if (this.side === 'buy' &&
            (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'buyCurrency')) {
            // fee is taken from trade amount to get fixed amount
            commissionRate = -1 * this.getCommission(this.fixedCurrency);
        }
        tradeAmountBN = fixedAmountBN.
            div(BigNumber(1).
            plus(commissionRate)).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
        logger.trace('%s new trade amount %s = net fixed amount %s / (1 + fixed currency %s commission %s))', functionName, tradeAmountBN.toString(), fixedAmountBN.toString(), this.fixedCurrency, commissionRate);
        this.amount = tradeAmountBN.toString();
        this.amountRemaining = this.amount;
        this.amountTraded = '0';
        this.amountLastPartial = '0';
        this.numberPartialFills = 0;
    };
    Order.prototype.fill = function () {
        var functionName = 'Order.fill()';
        logger.trace('%s about to fill order with state %s, id %s, tag %s, amount %s, amountRemaining %s and amountTraded %s', functionName, this.state, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountTraded);
        this.state = 'filled';
        var oldAmountTraded = this.amountTraded;
        this.amountTraded = new BigNumber(this.amountRemaining).
            plus(this.amountTraded).
            round(this.exchange.currencyRounding[this.market.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();
        logger.debug('%s amount trade = old amount traded %s + amount remaining %s = %s', functionName, oldAmountTraded, this.amountRemaining, this.amountTraded);
        this.amountLastPartial = this.amountRemaining;
        this.numberPartialFills++;
        this.amountRemaining = '0';
        logger.debug('%s filled order with id %s, tag %s, amount %s, amountRemaining %s, amountTraded %s, amountLastPartial %s and numberPartialFills %s', functionName, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountTraded, this.amountLastPartial, this.numberPartialFills);
    };
    Order.prototype.partialFill = function (fillAmount) {
        var functionName = 'Order.partialFill()';
        logger.trace('%s about to process partial fill amount %s for order with id %s, tag %s, amount %s, amountRemaining %s and amountTraded %s', functionName, fillAmount, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountTraded);
        // if the fill amount >= trade amount remaining
        if (BigNumber(fillAmount).greaterThanOrEqualTo(this.amountRemainingBN)) {
            logger.debug('%s fill amount %s >= trade amount remaining %s so will mark the %s order with id %s and tag %s as filled instead of partially filled', functionName, fillAmount, this.amountRemaining, this.side, this.exchangeId, this.tag);
            this.fill();
            return;
        }
        this.state = 'partiallyFilled';
        var oldAmountTraded = this.amountTraded;
        this.amountTraded = new BigNumber(this.amountTraded).
            plus(fillAmount).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();
        logger.debug('%s new amount traded = old amount traded %s + fill amount %s = %s', functionName, oldAmountTraded, fillAmount, this.amountTraded);
        var oldAmountRemaining = this.amountRemaining;
        this.amountRemaining = new BigNumber(this.amountRemaining).
            minus(fillAmount).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();
        logger.debug('%s new amount remaining = old amount remaining %s - fill amount %s = %s', functionName, oldAmountRemaining, fillAmount, this.amountRemaining);
        this.amountLastPartial = fillAmount;
        this.numberPartialFills++;
        logger.debug('%s amount last partial = %s and number of partial fills = %s', functionName, this.amountLastPartial, this.numberPartialFills);
    };
    // returns a cloned order for the remaining pending amount of a partially filled order
    Order.prototype.remainingPartial = function () {
        var functionName = 'Order.remainingPartial()';
        // create new order for the remaining pending amount
        var newRemainingOrder = this.clone();
        newRemainingOrder.state = 'pending';
        logger.debug('%s cloned new pending %s order from partially filled %s order with id %s and amount %s', functionName, newRemainingOrder.tag, newRemainingOrder.side, newRemainingOrder.exchangeId, newRemainingOrder.amount);
        return newRemainingOrder;
    };
    // returns a cloned order with a new price with the sell amount preserved
    Order.prototype.cloneFromAdjustedRate = function (rates, minTradeAmountBN, maxTradeAmountBN) {
        var functionName = 'Order.cloneFromAdjustedRate()';
        if (!minTradeAmountBN)
            minTradeAmountBN = new BigNumber(0);
        if (!maxTradeAmountBN)
            maxTradeAmountBN = new BigNumber(Infinity);
        // create new order for the new price
        var newAdjustedOrder = this.clone();
        newAdjustedOrder.price = rates.bestRate;
        newAdjustedOrder.minRate = rates.minRate;
        newAdjustedOrder.maxRate = rates.maxRate;
        if (this.side === 'buy' && this.price != rates.bestRate) {
            // new remaining amount = old remaining amount * old price / new price
            newAdjustedOrder.amountRemaining = this.amountRemainingBN.
                times(this.priceBN).
                div(rates.bestRate).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s new remaining amount %s = old remaining amount %s * old rate %s / new rate %s', functionName, newAdjustedOrder.amountRemaining, this.amountRemaining, this.price, rates.bestRate);
            // amount = old amount + (new remaining amount - old remaining amount)
            newAdjustedOrder.amount = newAdjustedOrder.amountBN.
                plus(newAdjustedOrder.amountRemainingBN).
                minus(this.amountRemainingBN).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s new amount %s = old amount %s + (new remaining amount %s - old remaining amount %s)', functionName, newAdjustedOrder.amount, this.amount, newAdjustedOrder.amountRemaining, this.amountRemaining);
            // if new amount remaining < min trade amount
            if (newAdjustedOrder.amountRemainingBN.lessThan(minTradeAmountBN)) {
                var oldAmountBN = newAdjustedOrder.amountBN;
                // new trade amount = old trade amount + (min trade amount - old remaining amount)
                newAdjustedOrder.amount = newAdjustedOrder.amountBN.
                    plus(minTradeAmountBN).
                    minus(newAdjustedOrder.amountRemaining).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s new trade amount %s = old trade amount %s + (min trade amount %s - old remaining amount %s)', functionName, newAdjustedOrder.amount, oldAmountBN.toString(), minTradeAmountBN.toString(), newAdjustedOrder.amountRemaining);
                newAdjustedOrder.amountRemaining = minTradeAmountBN.toString();
            }
            else if (newAdjustedOrder.amountRemainingBN.greaterThan(maxTradeAmountBN)) {
                var oldAmountBN = newAdjustedOrder.amountBN;
                // new trade amount = old trade amount - (old remaining amount - max trade amount)
                newAdjustedOrder.amount = newAdjustedOrder.amountBN.
                    minus(newAdjustedOrder.amountRemaining).
                    plus(maxTradeAmountBN).
                    round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s new trade amount %s = old trade amount %s - (old remaining amount %s - max trade amount %s)', functionName, newAdjustedOrder.amount, oldAmountBN.toString(), newAdjustedOrder.amountRemaining, maxTradeAmountBN.toString());
                newAdjustedOrder.amountRemaining = maxTradeAmountBN.toString();
            }
        }
        logger.debug('%s cloned new %s order for id %s, tag %s with rate %s->%s, amount %s->%s, remaining amount %s->%s, sell amount remaining %s->%s', functionName, newAdjustedOrder.side, newAdjustedOrder.exchangeId, newAdjustedOrder.tag, this.price, newAdjustedOrder.price, this.amount, newAdjustedOrder.amount, this.amountRemaining, newAdjustedOrder.amountRemaining, this.sell.amountRemaining, newAdjustedOrder.sell.amountRemaining);
        return newAdjustedOrder;
    };
    // used to append a partial fill to a previous partial fill
    // this is used by the Simulator to merge multiple partial fills of the same order
    Order.prototype.appendPartialFill = function (fillAmount) {
        var functionName = 'Order.appendPartialFill()';
        var oldAmountTraded, oldAmountRemaining, oldAmountLastPartial;
        // if fill amount >= remaining amount
        if (BigNumber(fillAmount).greaterThanOrEqualTo(this.amountRemaining)) {
            logger.debug('%s fill amount >= amount remaining so will fill the order', functionName, fillAmount, this.amountRemaining);
            this.state = 'filled';
            // increment the last partial amount by the remaining amount as it's a filled order
            // note this does not equal the amountRemaining as there could have been another partial fill earlier in the request
            oldAmountLastPartial = this.amountLastPartial;
            this.amountLastPartial = this.amountLastPartialBN.
                plus(this.amountRemaining).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s new amount last partial = old amount last partial %s + amount remaining %s = %s', functionName, oldAmountLastPartial, this.amountRemaining, this.amountLastPartial);
            // increment the traded amount
            oldAmountTraded = this.amountLastPartial;
            this.amountTraded = this.amountTradedBN.
                plus(this.amountRemaining).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s new amount traded = old amount traded %s + amount remaining %s = %s', functionName, oldAmountTraded, this.amountRemaining, this.amountTraded);
            this.amountRemaining = '0';
            logger.debug('%s filled %s with id %s, amount %s, amountRemaining %s, amountTraded %s, amountLastPartial %s and numberPartialFills %s', functionName, this.tag, this.exchangeId, this.amount, this.amountRemaining, this.amountTraded, this.amountLastPartial, this.numberPartialFills);
        }
        else {
            logger.debug('%s fill amount < amount remaining so will partially fill the order', functionName, fillAmount, this.amountRemaining);
            this.state = 'partiallyFilled';
            oldAmountTraded = this.amountTraded;
            this.amountTraded = new BigNumber(this.amountTraded).
                plus(fillAmount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s new amount traded = old amount traded %s + fill amount %s = %s', functionName, oldAmountTraded, fillAmount, this.amountTraded);
            oldAmountRemaining = this.amountRemaining;
            this.amountRemaining = new BigNumber(this.amountRemaining).
                minus(fillAmount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s new amount remaining = old amount remaining %s - fill amount %s = %s', functionName, oldAmountRemaining, fillAmount, this.amountRemaining);
            oldAmountLastPartial = this.amountLastPartial;
            this.amountLastPartial = this.amountLastPartialBN.
                plus(fillAmount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s new amount last partial = old amount last partial %s + fill amount %s = %s', functionName, oldAmountLastPartial, fillAmount, this.amountLastPartial);
        }
    };
    Order.prototype.cancel = function () {
        this.state = 'cancelled';
    };
    Order.prototype.log = function (level) {
        if (!level) {
            level = 'debug';
        }
        logger.log(level, "%s[%s] id %s, tag %s, state %s, side %s, amount %s %s, price %s, remaining %s, traded %s, filled %s%, Buy %s %s, Sell %s %s, Fee %s %s, num partials %s last partial %s, max buy rate %s, min sell rate %s, %s", this.exchangeName, this.symbol, this.exchangeId, this.tag, this.state, this.side, this.amount, this.fixedCurrency, this.price, this.amountRemaining, this.amountTraded, this.tradedPercentage, this.buy.amount, this.buy.currency, this.sell.amount, this.sell.currency, this.fee.amount, this.fee.currency, this.numberPartialFills, this.amountLastPartial, this.maxRate, this.minRate, moment(this.timestamp).format('D-MMM-YY H:mm:ss'));
    };
    return Order;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Order;
