/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
var _ = require('underscore');
var BigNumber = require('bignumber.js');
var market_1 = require("./market");
// My packages
var logger = require('config-logger'), VError = require('verror');
var HistoricalAccountTrade = (function () {
    function HistoricalAccountTrade(settings) {
        this.feeAmountBN = BigNumber(NaN);
        _.extend(this, settings);
    }
    Object.defineProperty(HistoricalAccountTrade.prototype, "fixedCurrency", {
        get: function () {
            return market_1.default.getFixedCurrency(this.symbol);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HistoricalAccountTrade.prototype, "variableCurrency", {
        get: function () {
            return market_1.default.getVariableCurrency(this.symbol);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HistoricalAccountTrade.prototype, "netFixedAmountBN", {
        // calculate the net fixed currency amount after a trade. That is, fixed currency amount after fees. eg BTC for BTCUSD
        get: function () {
            if (this.fixedCurrency == this.feeCurrency) {
                if (this.side == 'sell') {
                    return this.quantityBN.
                        plus(this.feeAmountBN);
                }
                else if (this.side == 'buy') {
                    return this.quantityBN.
                        minus(this.feeAmountBN);
                }
            }
            else {
                // just return the trade quantity
                return this.quantityBN;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HistoricalAccountTrade.prototype, "netVariableAmountBN", {
        // calculate the net variable currency amount after a trade. That is, variable currency amount after fees. eg USD for BTCUSD
        get: function () {
            if (this.variableCurrency == this.feeCurrency) {
                if (this.side == 'sell') {
                    return this.quantityBN.
                        times(this.priceBN).
                        minus(this.feeAmountBN);
                }
                else if (this.side == 'buy') {
                    return this.quantityBN.
                        times(this.priceBN).
                        plus(this.feeAmountBN);
                }
            }
            else {
                // just return the variable amount
                return this.quantityBN.
                    times(this.priceBN);
            }
        },
        enumerable: true,
        configurable: true
    });
    return HistoricalAccountTrade;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HistoricalAccountTrade;
