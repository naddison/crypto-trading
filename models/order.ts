/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />

import * as moment from 'moment';
import * as _ from 'underscore';
import {format} from 'util';
import BigNumber = require('bignumber.js');
type BigNumber = bignumber.BigNumber;

// import my Interfaces
import Exchange from "./exchange";
import Market from "./market";
import {ITradeAmounts, IGrossTradeAmounts, IFixVarTradeAmounts} from "./tradeAmounts";

// My packages
const logger = require('config-logger'),
    VError = require('verror');

export interface IOrderSettings
{
    exchangeId?: string,
    originalExchangeId?: string,
    exchangeName: string,
    symbol: string,
    side: string,
    type?: string,
    state?: string,
    amount?: string | number | BigNumber,        // the fixed currency amount before fess are deducted
    amountTraded?: string | number | BigNumber,
    amountRemaining?: string | number | BigNumber,
    amountLastPartial?: string | number | BigNumber,
    numberPartialFills?: number,
    price?: string | number | BigNumber,
    tag?: string,
    minRate?: string,
    maxRate?: string,

    // TODO add timestamp for each state
    timestamp?: Date
}

export interface IRates
{
    bestRate: string,
    minRate: string,
    maxRate: string,
    expectedLiquidPrice: string
}

export enum OrderSide {
    sell,
    buy,
    unknown
}

export enum OrderType {
    limit,
    market,
    stop,
    trailingStop,
    fillOrKill
}

export enum OrderState {
    quoted,
    pending,
    pendingPartial,
    partiallyFilled,
    filled,
    cancelled
}

export default class Order
{
    exchangeId: string;
    originalExchangeId: string;
    exchangeName: string;
    symbol: string;
    // sell or buy
    side: string;
    // limit or market
    type: string = "limit";
    // 'quoted', 'pending', 'pendingPartial', 'partiallyFilled', 'filled', 'cancelled', 'expired'
    state: string = "quoted";

    amountBN: BigNumber;        // the fixed currency amount before fess are deducted
    amountTradedBN: BigNumber;
    amountRemainingBN: BigNumber;

    amountLastPartialBN: BigNumber;
    numberPartialFills: number = 0;

    priceBN: BigNumber;

    tag: string;
    minRate: string = "0";
    maxRate: string = "Infinity";

    // TODO add timestamp for each state
    timestamp: Date = new Date();

    constructor(settings: IOrderSettings)
    {
        this.exchangeId = settings.exchangeId;
        this.originalExchangeId = settings.originalExchangeId || this.exchangeId;

        this.exchangeName = settings.exchangeName;
        this.symbol = settings.symbol;
        this.side = settings.side;
        this.type = settings.type || this.type;
        this.state = settings.state || this.state;

        this.amountBN = (settings.amount ? new BigNumber(settings.amount) : BigNumber(0) );        // the fixed currency amount before fess are deducted
        this.amountTradedBN = (settings.amountTraded ? new BigNumber(settings.amountTraded) : BigNumber(0) );
        // if not set the use amount
        this.amountRemainingBN = (settings.amountRemaining ? new BigNumber(settings.amountRemaining) : this.amountBN);
        this.amountLastPartialBN = (settings.amountLastPartial ? new BigNumber(settings.amountLastPartial) : BigNumber(0) );
        this.numberPartialFills = settings.numberPartialFills || 0;

        if (settings.price) { this.priceBN = new BigNumber(settings.price); }

        this.tag = settings.tag;
        this.minRate = settings.minRate || this.minRate;
        this.maxRate = settings.maxRate || this.maxRate;

        // validate string values
        if ( !_.contains(['buy','sell'], this.side) ) { return null; }
        if ( !_.contains(['limit', 'market', 'stop', 'trailingStop', 'fillOrKill'],this.type) ) { return null;}
        if ( !_.contains(['quoted', 'pending', 'pendingPartial', 'partiallyFilled', 'filled', 'cancelled'],this.state) ) { return null;}
    }

    get price(): string {
        if (this.priceBN) {
            return this.priceBN.toString();
        }
    }
    set price(price) {
        if(price) {
            this.priceBN = new BigNumber(price);
        }
    }

    get amount(): string {return this.amountBN.toString(); }
    set amount(amount) {this.amountBN = new BigNumber(amount); }

    get amountTraded(): string {return this.amountTradedBN.toString(); }
    set amountTraded(amountTraded) {this.amountTradedBN = new BigNumber(amountTraded); }

    get amountRemaining(): string {
        if(this.amountRemainingBN) {
            return this.amountRemainingBN.toString();
        }
    }
    set amountRemaining(amountRemaining) {
        if (amountRemaining) {
            this.amountRemainingBN = new BigNumber(amountRemaining);
        }
    }

    get amountLastPartial(): string {return this.amountLastPartialBN.toString(); }
    set amountLastPartial(amountLastPartial) {this.amountLastPartialBN = new BigNumber(amountLastPartial); }

    get buyAmountBN(): BigNumber {return new BigNumber(this.buy.amount); }
    get buyAmountRemainingBN(): BigNumber {return new BigNumber(this.buy.amountRemaining); }
    get buyAmountTradedBN(): BigNumber {return new BigNumber(this.buy.amountTraded); }
    get buyAmountLastPartialBN(): BigNumber {return new BigNumber(this.buy.amountLastPartial); }

    get buyAmountGrossBN(): BigNumber {return new BigNumber(this.buy.amountGross); }
    get buyAmountRemainingGrossBN(): BigNumber {return new BigNumber(this.buy.amountRemainingGross); }
    get buyAmountTradedGrossBN(): BigNumber {return new BigNumber(this.buy.amountTradedGross); }
    get buyAmountLastPartialGrossBN(): BigNumber {return new BigNumber(this.buy.amountLastPartialGross); }

    get sellAmountBN(): BigNumber {return new BigNumber(this.sell.amount); }
    get sellAmountRemainingBN(): BigNumber {return new BigNumber(this.sell.amountRemaining); }
    get sellAmountTradedBN(): BigNumber {return new BigNumber(this.sell.amountTraded); }
    get sellAmountLastPartialBN(): BigNumber {return new BigNumber(this.sell.amountLastPartial); }

    get sellAmountGrossBN(): BigNumber {return new BigNumber(this.sell.amountGross); }
    get sellAmountRemainingGrossBN(): BigNumber {return new BigNumber(this.sell.amountRemainingGross); }
    get sellAmountTradedGrossBN(): BigNumber {return new BigNumber(this.sell.amountTradedGross); }
    get sellAmountLastPartialGrossBN(): BigNumber {return new BigNumber(this.sell.amountLastPartialGross); }

    get feeAmountBN(): BigNumber {return new BigNumber(this.fee.amount); }
    get feeAmountRemainingBN(): BigNumber {return new BigNumber(this.fee.amountRemaining); }
    get feeAmountTradedBN(): BigNumber {return new BigNumber(this.fee.amountTraded); }
    get feeAmountLastPartialBN(): BigNumber {return new BigNumber(this.fee.amountLastPartial); }

    get fixedAmountBN(): BigNumber {return new BigNumber(this.fixedAmount); }
    get fixedAmountRemainingBN(): BigNumber {return new BigNumber(this.fixedAmountRemaining); }
    get fixedAmountTradedBN(): BigNumber {return new BigNumber(this.fixedAmountTraded); }
    get fixedAmountLastPartialBN(): BigNumber {return new BigNumber(this.fixedAmountLastPartial); }
    get fixedAmountGrossBN(): BigNumber {return new BigNumber(this.fixedAmountGross); }
    get fixedAmountRemainingGrossBN(): BigNumber {return new BigNumber(this.fixedAmountRemainingGross); }
    get fixedAmountTradedGrossBN(): BigNumber {return new BigNumber(this.fixedAmountTradedGross); }
    get fixedAmountLastPartialGrossBN(): BigNumber {return new BigNumber(this.fixedAmountLastPartialGross); }
    get fixedFeeAmountBN(): BigNumber {return new BigNumber(this.fee.fixedAmount); }
    get fixedFeeAmountRemainingBN(): BigNumber {return new BigNumber(this.fee.fixedAmountRemaining); }
    get fixedFeeAmountTradedBN(): BigNumber {return new BigNumber(this.fee.fixedAmountTraded); }
    get fixedFeeAmountLastPartialBN(): BigNumber {return new BigNumber(this.fee.fixedAmountLastPartial); }

    get variableAmountBN(): BigNumber {return new BigNumber(this.variableAmount); }
    get variableAmountRemainingBN(): BigNumber {return new BigNumber(this.variableAmountRemaining); }
    get variableAmountTradedBN(): BigNumber {return new BigNumber(this.variableAmountTraded); }
    get variableAmountLastPartialBN(): BigNumber {return new BigNumber(this.variableAmountLastPartial); }
    get variableAmountGrossBN(): BigNumber {return new BigNumber(this.variableAmountGross); }
    get variableAmountRemainingGrossBN(): BigNumber {return new BigNumber(this.variableAmountRemainingGross); }
    get variableAmountTradedGrossBN(): BigNumber {return new BigNumber(this.variableAmountTradedGross); }
    get variableAmountLastPartialGrossBN(): BigNumber {return new BigNumber(this.variableAmountLastPartialGross); }
    get feeVariableAmountBN(): BigNumber {return new BigNumber(this.fee.variableAmount); }

    get minRateBN(): BigNumber {return new BigNumber(this.minRate); }
    get maxRateBN(): BigNumber {return new BigNumber(this.maxRate); }


    clone(): Order
    {
        const clonedOrder = new Order({
            exchangeId:     _.clone(this.exchangeId),
            originalExchangeId:     _.clone(this.originalExchangeId),
            exchangeName:   _.clone(this.exchangeName),
            symbol:     _.clone(this.symbol),
            side:           _.clone(this.side),
            type:           _.clone(this.type),
            state:          _.clone(this.state),
            amount:         _.clone(this.amount),
            amountTraded:   _.clone(this.amountTraded),
            amountRemaining:_.clone(this.amountRemaining),
            price:   _.clone(this.price),
            amountLastPartial:  _.clone(this.amountLastPartial),
            numberPartialFills: _.clone(this.numberPartialFills),
            timestamp:      new Date(this.timestamp.getTime() ),
            tag:            _.clone(this.tag),
            minRate:        _.clone(this.minRate),
            maxRate:        _.clone(this.maxRate)
        });

        return clonedOrder;
    }

    get jsObject(): Object
    {
        return {
            exchangeId:     _.clone(this.exchangeId),
            originalExchangeId:     _.clone(this.originalExchangeId),
            exchangeName:   _.clone(this.exchangeName),
            symbol:         _.clone(this.symbol),
            side:           _.clone(this.side),
            state:          _.clone(this.state),
            price:          _.clone(this.price),
            timestamp:      new Date(this.timestamp.getTime()),
            tag:            _.clone(this.tag),

            minRate:        _.clone(this.minRate),
            maxRate:        _.clone(this.maxRate),

            amount:         _.clone(this.amount),
            amountTraded:     _.clone(this.amountTraded),
            amountRemaining:  _.clone(this.amountRemaining),
            buyAmount:      _.clone(this.buy.amount),
            buyCurrency:    _.clone(this.buy.currency),
            sellAmount:     _.clone(this.sell.amount),
            sellCurrency:   _.clone(this.sell.currency),
            feeAmount:      _.clone(this.fee.amount),
            feeCurrency:    _.clone(this.fee.currency),
            amountLastPartial:  _.clone(this.amountLastPartial),
            numberPartialFills: _.clone(this.numberPartialFills)
        };
    }

    // Create reference to the exchange which is not saved into the database
    get exchange(): Exchange
    {
        const functionName = 'Order.exchange()',
            returnedExchange: Exchange = Exchange.exchanges[this.exchangeName];

        if (!returnedExchange)
        {
            const error = new VError('%s exchange %s can not be found in the static Exchange.exchanges list for %s %s order with id %s, tag %s, amount %s, remaining amount %s, last partial amount %s', functionName,
                this.exchangeName, this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);

            logger.debug(error);
            throw error;
        }

        return returnedExchange;
    };

    // Create reference to the market which is not saved into the database
    get market(): Market
    {
        const functionName = 'Order.market()',
            returnedMarket: Market = this.exchange.markets[this.symbol];

        if (!returnedMarket)
        {
            const error = new VError('%s market %s can not be found on the %s exchange for %s %s order with id %s, tag %s, amount %s, remaining amount %s, last partial amount %s', functionName,
                this.symbol, this.exchange.name, this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);

            logger.debug(error);
            throw error;
        }
        return returnedMarket;
    }

    get fixedCurrency(): string { return Market.getFixedCurrency(this.symbol) }
    get variableCurrency(): string { return Market.getVariableCurrency(this.symbol) }

    get pending(): boolean { return this.state === "pending"; }

    get filled(): boolean { return this.state === "filled" || this.state === "partiallyFilled"; }

    // traded amount compared to trade amount
    get tradedRatioBN(): BigNumber
    {
        // percentage of the order filled = traded amount / trade amount * 100
        return this.amountTradedBN.
        div(this.amountBN);
    }

    get tradedPercentageBN(): BigNumber
    {
        return this.tradedRatioBN.
        times(100);
    }

    get tradedPercentage(): string
    {
        return this.tradedPercentageBN.
        round(3).
        toString();
    }

    // remaining amount compared to trade amount
    get remainingRatioBN(): BigNumber
    {
        return this.amountRemainingBN.
        div(this.amountBN);
    }

    get remainingPercentageBN(): BigNumber
    {
        return this.remainingRatioBN.
        times(100);
    }

    get remainingPercentage(): string
    {
        return this.remainingPercentageBN.
        round(3).toString();
    }

    // last partial amount compared to trade amount
    get lastPartialRatioBN(): BigNumber
    {
        return this.amountLastPartialBN.
        div(this.amountBN);
    }

    get lastPartialPercentageBN(): BigNumber
    {
        return this.lastPartialRatioBN.
        times(100);
    }

    get lastPartialPercentage(): string
    {
        return this.lastPartialPercentageBN.
        round(3).toString();
    }

    get buy(): IGrossTradeAmounts
    {
        const functionName = 'Order.buy()';

        const order = this;

        class BuyTradeAmounts implements ITradeAmounts
        {
            get currency():string
            {
                // set currency
                if (order.side === 'buy')
                {
                    return order.fixedCurrency;
                }
                else if (order.side === 'sell')
                {
                    return order.variableCurrency;
                }
                else
                {
                    const error = new VError("%s can't get buy.currency on order with id %s as side %s is not buy or sell", functionName,
                        order.exchangeId, order.side);

                    logger.error(error.stack);
                    return error;
                }
            }

            get amount(): string { return order.calculateBuyAmount(order.amount, 'amount') as string; }
            get amountRemaining(): string { return order.calculateBuyAmount(order.amountRemaining, 'amountRemaining') as string; }
            get amountTraded(): string { return order.calculateBuyAmount(order.amountTraded, 'amountTraded') as string; }
            get amountLastPartial(): string { return order.calculateBuyAmount(order.amountLastPartial, 'amountLastPartial') as string; }

            get amountGross(): string { return order.calculateBuyAmount(order.amount, 'amountGross', true) as string; }
            get amountRemainingGross(): string { return order.calculateBuyAmount(order.amountRemaining, 'amountRemainingGross', true) as string; }
            get amountTradedGross(): string { return order.calculateBuyAmount(order.amountTraded, 'amountTradedGross', true) as string; }
            get amountLastPartialGross(): string { return order.calculateBuyAmount(order.amountLastPartial, 'amountLastPartialGross', true) as string; }
        }

        const tradeAmounts = new BuyTradeAmounts();

        return tradeAmounts;
    }

    get sell(): IGrossTradeAmounts
    {
        const functionName = 'Order.sell()';

        const order = this;

        class SellTradeAmounts implements ITradeAmounts
        {
            get currency():string
            {
                // set currency
                if (order.side === 'sell')
                {
                    return order.fixedCurrency;
                }
                else if (order.side === 'buy')
                {
                    return order.variableCurrency;
                }
                else
                {
                    const error = new VError("%s can't get sell.currency on order with id %s as side %s is not buy or sell", functionName,
                        order.exchangeId, order.side);

                    logger.error(error.stack);
                    return error;
                }
            }

            get amount(): string { return order.calculateSellAmount(order.amount, 'amount') as string; }
            get amountRemaining(): string { return order.calculateSellAmount(order.amountRemaining, 'amountRemaining') as string; }
            get amountTraded(): string { return order.calculateSellAmount(order.amountTraded, 'amountTraded') as string; }
            get amountLastPartial(): string { return order.calculateSellAmount(order.amountLastPartial, 'amountLastPartial') as string; }

            get amountGross(): string { return order.calculateSellAmount(order.amount, 'amountGross', true) as string; }
            get amountRemainingGross(): string { return order.calculateSellAmount(order.amountRemaining, 'amountRemainingGross', true) as string; }
            get amountTradedGross(): string { return order.calculateSellAmount(order.amountTraded, 'amountTradedGross', true) as string; }
            get amountLastPartialGross(): string { return order.calculateSellAmount(order.amountLastPartial, 'amountLastPartialGross', true) as string; }
        }

        const tradeAmounts = new SellTradeAmounts();

        return tradeAmounts;
    }

    get fee(): IFixVarTradeAmounts
    {
        const functionName = 'Order.fee()';

        const order = this;

        class FeeFixVarTradeAmounts implements IFixVarTradeAmounts
        {
            get currency(): string
            {
                if (order.exchange.feeStructure === 'buyCurrency')
                {
                    return order.buy.currency;
                }
                else if (order.exchange.feeStructure === 'variableCurrency')
                {
                    if (order.side === 'sell')
                        return order.buy.currency;

                    else if (order.side === 'buy')
                        return order.sell.currency;
                }

                else if (order.exchange.feeStructure === 'fixedCurrency')
                {
                    if (order.side === 'sell')
                        return  order.sell.currency;

                    else if (order.side === 'buy')
                        return order.buy.currency;
                }

                else if (order.exchange.feeStructure === 'sellCurrency')
                {
                    return order.sell.currency;
                }
                else
                {
                    const error = new VError('%s could not return fee currency. The fee structure %s on the %s exchange is invalid.', functionName,
                        order.exchange.feeStructure, order.exchange.name);

                    logger.error(error.stack);
                    return error;
                }
            }

            get amount(): string { return order.calculateFeeAmount(order.amount, 'amount') as string; }
            get amountRemaining(): string { return order.calculateFeeAmount(order.amountRemaining, 'amountRemaining') as string; }
            get amountTraded(): string { return order.calculateFeeAmount(order.amountTraded, 'amountTraded') as string; }
            get amountLastPartial(): string { return order.calculateFeeAmount(order.amountLastPartial, 'amountLastPartial') as string; }

            get fixedAmount(): string { return order.calculateFeeFixedAmount(this.amount, 'fee.amount') as string; }
            get fixedAmountRemaining(): string { return order.calculateFeeFixedAmount(this.amountRemaining, 'fee.amountTraded') as string; }
            get fixedAmountTraded(): string { return order.calculateFeeFixedAmount(this.amountTraded, 'fee.amountTraded') as string; }
            get fixedAmountLastPartial(): string { return order.calculateFeeFixedAmount(this.amountLastPartial, 'fee.amountLastPartial') as string; }

            get variableAmount(): string { return order.calculateFeeVariableAmount(this.amount, 'fee.amount') as string; }
            get variableAmountRemaining(): string { return order.calculateFeeVariableAmount(this.amountRemaining, 'fee.amountTraded') as string; }
            get variableAmountTraded(): string { return order.calculateFeeVariableAmount(this.amountTraded, 'fee.amountTraded') as string; }
            get variableAmountLastPartial(): string { return order.calculateFeeVariableAmount(this.amountLastPartial, 'fee.amountLastPartial') as string; }
        }

        const tradeAmounts = new FeeFixVarTradeAmounts();

        return tradeAmounts;
    }

    get fixedAmount(): string
    {
        return this.calculateFixedAmount('amount') as string;
    }

    get fixedAmountRemaining(): string
    {
        return this.calculateFixedAmount('amountRemaining') as string;
    }

    get fixedAmountTraded(): string
    {
        return this.calculateFixedAmount('amountTraded') as string;
    }

    get fixedAmountLastPartial(): string
    {
        return this.calculateFixedAmount('amountLastPartial') as string;
    }

    get variableAmount(): string
    {
        return this.calculateVariableAmount('amount') as string;
    }

    get variableAmountRemaining(): string
    {
        return this.calculateVariableAmount('amountRemaining') as string;
    }

    get variableAmountTraded(): string
    {
        return this.calculateVariableAmount('amountTraded') as string;
    }

    get variableAmountLastPartial(): string
    {
        return this.calculateVariableAmount('amountLastPartial') as string;
    }

    get fixedAmountGross(): string
    {
        return this.calculateFixedAmount('amountGross') as string;
    }

    get fixedAmountRemainingGross(): string
    {
        return this.calculateFixedAmount('amountRemainingGross') as string;
    }

    get fixedAmountTradedGross(): string
    {
        return this.calculateFixedAmount('amountTradedGross') as string;
    }

    get fixedAmountLastPartialGross(): string
    {
        return this.calculateFixedAmount('amountLastPartialGross') as string;
    }

    get variableAmountGross(): string
    {
        return this.calculateVariableAmount('amountGross') as string;
    }

    get variableAmountRemainingGross(): string
    {
        return this.calculateVariableAmount('amountRemainingGross') as string;
    }

    get variableAmountTradedGross(): string
    {
        return this.calculateVariableAmount('amountTradedGross') as string;
    }

    get variableAmountLastPartialGross(): string
    {
        return this.calculateVariableAmount('amountLastPartialGross') as string;
    }

    getCurrencyAmountBN(currency: string): BigNumber
    {
        const functionName = format('Order.getCurrencyAmountBN');

        if (this.fixedCurrency === currency)
        {
            return this.fixedAmountBN;
        }
        else if (this.variableCurrency === currency)
        {
            return this.variableAmountBN;
        }
        else
        {
            const error = new VError('%s currency %s is not fixed currency %s or variable currency %s', functionName,
                currency, this.fixedCurrency, this.variableCurrency);
            logger.error(error.stack);
            return error;
        }
    }

    getCurrencyAmountRemainingBN(currency: string): BigNumber
    {
        const functionName = format('Order.getCurrencyAmountRemainingBN');

        if (this.fixedCurrency === currency)
        {
            return this.fixedAmountRemainingBN;
        }
        else if (this.variableCurrency === currency)
        {
            return this.variableAmountRemainingBN;
        }
        else
        {
            const error = new VError('%s currency %s is not fixed currency %s or variable currency %s', functionName,
                currency, this.fixedCurrency, this.variableCurrency);
            logger.error(error.stack);
            return error;
        }
    }

    // TODO set return type to string and change return of NaN
    // used to convert various amounts like amountRemaining, amountLastPartial and amountTraded into the buy amount
    calculateBuyAmount(amount: string, amountName: string, gross: boolean = false): string | Error | number
    {
        const functionName = format('Order.calculateBuyAmount');

        let buyAmount: string,
            commissionRate = 0;

        if (this.side === 'buy')
        {
            // if net buy amount and fee amount is in fixed currency
            if (!gross && (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'fixedCurrency'))
            {
                commissionRate = this.getCommission(this.fixedCurrency);

                buyAmount = new BigNumber(amount).
                times(BigNumber(1).
                    minus(commissionRate)
                ).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.trace('%s buy.%s of buy order %s = amount %s * (1 - %s commission %s)', functionName,
                    amountName, buyAmount, amount, this.fixedCurrency, commissionRate );
            }
            // if gross buy amount or fee amount is in variable currency
            else
            {
                buyAmount = new BigNumber(amount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.trace('%s buy.%s of buy order %s = amount %s', functionName,
                    amountName, buyAmount, amount );
            }
        }
        else if (this.side === 'sell')
        {
            // TODO move the validation of the price into a separate function
            // if no price was set, which will be the case for pending market orders.
            // Filled market orders should have an price
            if (!this.price)
            {
                const error = new VError('%s price not set for %s %s order with id %s, tag %s, trade amount %s, remaining amount %s, last partial amount %s', functionName,
                    this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);

                logger.debug(error.stack);
                return NaN;
            }

            // if net buy amount and fee is charged in variable currency
            if (!gross && (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'variableCurrency'))
            {
                commissionRate = this.getCommission(this.variableCurrency);

                // calculate the nett amount
                buyAmount = new BigNumber(amount).
                times(this.price).
                times(BigNumber(1).
                    minus(commissionRate)
                ).
                round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.trace('%s buy.%s of sell order %s = amount %s * price %s * (1 - %s commission %s)', functionName,
                    amountName, buyAmount, amount, this.price, this.variableCurrency, commissionRate );
            }
            // if gross buy amount or fee is charged in fixed currency
            else
            {
                // calculate the gross amount
                buyAmount = new BigNumber(amount).
                times(this.price).
                round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.trace('%s buy.%s of sell order %s = trade amount %s * price %s', functionName,
                    amountName, buyAmount, amount, this.price);
            }
        }
        else
        {
            const error = new VError("%s can't get buy.%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName,
                amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            buyAmount = error;
        }

        return buyAmount;
    }

    // TODO set return type to string and change return of NaN
    // used to convert various amounts like amountRemaining, amountLastPartial and amountTraded into the sell amount
    calculateSellAmount(tradeAmount: string, amountName: string, gross: boolean = false): string | Error | number
    {
        const functionName = format('Order.calculateSellAmount');

        let sellAmount: string,
            commissionRate = 0;

        if (this.side === 'buy')
        {
            // TODO move the validation of the price into a separate function
            // if no price was set, which will be the case for pending market orders.
            // Filled market trades should have an price
            if (!this.price)
            {
                const error = new VError('%s price not set for %s %s order with id %s, tag %s, amount %s, remaining amount %s, last partial amount %s', functionName,
                    this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);

                logger.debug(error);
                return NaN;
            }

            // if net amount and fee is in variable currency
            if (!gross && (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'sellCurrency'))
            {
                commissionRate = this.getCommission(this.variableCurrency);

                sellAmount = new BigNumber(tradeAmount).
                times(this.price).
                times(BigNumber(1).
                    plus(commissionRate)
                ).
                round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.trace('%s sell.%s of buy order %s %s = trade amount %s * price %s * (1 + %s commission %s)', functionName,
                    amountName, sellAmount, this.variableCurrency,
                    tradeAmount, this.price,
                    this.variableCurrency, commissionRate );
            }
            // else gross amount or fee is in fixed currency
            else
            {
                sellAmount = new BigNumber(tradeAmount).
                times(this.price).
                round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.trace('%s sell.%s of buy order %s %s = trade amount %s * price %s', functionName,
                    amountName, sellAmount, this.variableCurrency,
                    tradeAmount, this.price);
            }
        }
        else if (this.side === 'sell')
        {
            // if net amount and fee is in fixed currency
            if (!gross && (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'sellCurrency'))
            {
                commissionRate = this.getCommission(this.fixedCurrency);

                sellAmount = new BigNumber(tradeAmount).
                times(BigNumber(1).
                    plus(commissionRate)
                ).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.trace('%s sell.%s of sell order %s %s = trade amount %s * (1 + %s commission %s)', functionName,
                    amountName, sellAmount, this.fixedCurrency,
                    tradeAmount,
                    this.fixedCurrency, commissionRate );
            }
            // if gross amount or fee is in variable currency
            else
            {
                sellAmount =  BigNumber(tradeAmount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.trace('%s sell.%s of sell order %s = trade amount %s', functionName,
                    amountName, sellAmount, tradeAmount );
            }
        }
        else
        {
            const error = new VError("%s can't get sell.%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName,
                amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            sellAmount = error;
        }

        return sellAmount;
    }

    // TODO set return type to string and change return of NaN
    // used to convert various amounts like amountRemaining, amountLastPartial and amountTraded into the fee amount
    calculateFeeAmount(tradeAmount: string, amountName: string): string | Error | number
    {
        const functionName = format('Order.calculateFeeAmount');

        let feeAmount: string,
            commissionRate = 0;

        // if fee is in fixed currency
        if (this.exchange.feeStructure === 'fixedCurrency' ||
            (this.side === 'buy' && this.exchange.feeStructure === 'buyCurrency') ||
            (this.side === 'sell' && this.exchange.feeStructure === 'sellCurrency'))
        {
            commissionRate = this.getCommission(this.fixedCurrency);

            feeAmount = new BigNumber(tradeAmount).
            times(commissionRate).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();

            logger.trace('%s fee.%s of buy order %s %s = trade amount %s * fixed currency %s commission rate %s', functionName,
                amountName, feeAmount, this.fixedCurrency,
                tradeAmount,
                this.fixedCurrency, commissionRate );
        }
        // if fee is in variable currency
        else if (this.exchange.feeStructure === 'variableCurrency' ||
            (this.side === 'sell' && this.exchange.feeStructure === 'buyCurrency') ||
            (this.side === 'buy' && this.exchange.feeStructure === 'sellCurrency'))
        {
            // if no price was set, which will be the case for pending market orders.
            // Filled market trades should have an price
            if (!this.price)
            {
                const error = new VError('%s price not set for %s %s order with id %s, tag %s, amount %s, remaining amount %s, last partial amount %s', functionName,
                    this.side, this.type, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountLastPartial);

                logger.debug(error);
                return NaN;
            }

            commissionRate = this.getCommission(this.variableCurrency);

            feeAmount = new BigNumber(tradeAmount).
            times(this.price).
            times(commissionRate).
            round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
            toString();

            logger.trace('%s fee.%s of buy order %s %s = trade amount %s * price %s * variable currency %s commission rate %s', functionName,
                amountName, feeAmount, this.variableCurrency,
                tradeAmount,
                this.price,
                this.variableCurrency, commissionRate );
        }
        else
        {
            const error = new VError("%s can't get sell.%s on order with id %s as side '%s' is not 'buy' or 'sell'; or feeStructure %s is not 'buyCurrency', 'sellCurrency', 'fixedCurrency' or 'variableCurrency'", functionName,
                amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            feeAmount = error;
        }

        return feeAmount;
    }

    // used to convert various trade amounts like amountRemaining, amountLastPartial and amountTraded into the fixed currency amount
    calculateFixedAmount(amountName: string): string | Error
    {
        const functionName = format('Order.calculateFixedAmount');

        let fixedAmount: string;

        if (this.side === 'buy')
        {
            fixedAmount = this.buy[amountName];

            logger.trace('%s fixed.%s of buy order %s = buy.%s %s', functionName,
                amountName, fixedAmount, amountName, this.buy[amountName] );
        }
        else if (this.side === 'sell')
        {
            fixedAmount = this.sell[amountName];

            logger.trace('%s fixed.%s of sell order %s = sell.%s %s', functionName,
                amountName, fixedAmount, amountName, this.sell[amountName] );
        }
        else
        {
            const error = new VError("%s can't get fixed.%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName,
                amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            fixedAmount = error;
        }

        return fixedAmount;
    }

    /**
     * Converts an amount into a variable currency
     * If fees are in variable currency, '0' is returned
     * @param tradeAmount trade amounts like amountRemaining, amountLastPartial and amountTraded
     * @param amountName
     * @returns amount in variable as a string
     */
    calculateVariableAmount(amountName: string): string | Error
    {
        const functionName = format('Order.calculateVariableAmount');

        let variableAmount: string;

        if (this.side === 'buy')
        {
            variableAmount = this.sell[amountName];

            logger.trace('%s variable.%s of buy order %s = sell.%s %s', functionName,
                amountName, variableAmount, amountName, this.sell[amountName] );
        }
        else if (this.side === 'sell')
        {
            variableAmount = this.buy[amountName];

            logger.trace('%s variable.%s of sell order %s = buy.%s %s', functionName,
                amountName, variableAmount, amountName, this.buy[amountName] );
        }
        else
        {
            const error = new VError("%s can't get variable.%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName,
                amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            variableAmount = error;
        }

        return variableAmount;
    }

    /**
     * Returns any fees that are charged in fixed currency.
     * If fees are in variable currency for this order then '0' is returned
     * @param feeAmount fee amounts like fee.amountRemaining, fee.amountLastPartial and fee.amountTraded
     * @param amountName only used for logging. eg 'fee.amountRemaining', 'fee.amountLastPartial' and 'fee.amountTraded'
     * @returns fee in string format
     */
    calculateFeeFixedAmount(feeAmount: string, amountName: string): string | Error
    {
        const functionName = format('Order.calculateFeeFixed%s', amountName);

        let feeFixedAmount: string;

        if (this.fee.currency === this.fixedCurrency)
        {
            feeFixedAmount = new BigNumber(feeAmount).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP).
            toString();

            logger.trace('%s fixed currency %s fee %s = fee amount %s', functionName,
                this.fixedCurrency, feeFixedAmount,
                feeAmount );
        }
        else if (this.fee.currency === this.variableCurrency)
        {
            feeFixedAmount = '0';
        }
        else
        {
            const error = new VError("%s can't get fee.fixed%s on order with id %s as fee currency %s does not match this trades fixed %s or variable %s currencies", functionName,
                amountName, this.exchangeId, this.fee.currency,
                this.fixedCurrency, this.variableCurrency);
            logger.error(error.stack);
            feeFixedAmount = error;
        }

        return feeFixedAmount;
    }

// used to convert various fee amounts like fee.amountRemaining, fee.amountLastPartial and fee.amountTraded into the variable currency amount
    /**
     * Returns any fees that are charged in variable currency.
     * If fees are in fixed currency for this order then '0' is returned
     * @param feeAmount fee amounts like fee.amountRemaining, fee.amountLastPartial and fee.amountTraded
     * @param amountName only used for logging. eg 'fee.amountRemaining', 'fee.amountLastPartial' and 'fee.amountTraded'
     * @returns fee in string format
     */
    calculateFeeVariableAmount(tradeAmount: string, amountName: string): string | Error
    {
        const functionName = format('Order.calculateFeeVariable%s', amountName);

        let feeVariableAmount: string;

        if (this.fee.currency === this.fixedCurrency)
        {
            feeVariableAmount = '0';
        }
        else if (this.fee.currency === this.variableCurrency)
        {
            feeVariableAmount = new BigNumber(tradeAmount).
            round(this.exchange.currencyRounding[this.variableCurrency], BigNumber.ROUND_DOWN).
            toString();
        }
        else
        {
            const error = new VError("%s can't get fee.variable%s on order with id %s as side '%s' is not 'buy' or 'sell'", functionName,
                amountName, this.exchangeId, this.side);
            logger.error(error.stack);
            feeVariableAmount = error;
        }

        return feeVariableAmount;
    }


    /**
     * Gets the maker or taker commission for from the exchange the order is against
     * @param currency the currency the fee will be charged in. This depends on the order side and exchange fee structure
     * @returns commission in decimal format. eg 0.001 which is 0.1%
     */
    getCommission(currency: string): number
    {
        const functionName = format('Order.getCommission');

        if (!currency)
        {
            const error = new VError('%s currency parameter %s has not been passed', functionName, currency);
            logger.error(error.stack);
            return error;
        }

        if (this.type === 'limit')
        {
            return this.exchange.commissions[currency].maker;
        }
        else if (this.type === 'market')
        {
            return this.exchange.commissions[currency].taker;
        }
    }

    // TODO add unit tests
    getVariableCommissionBN(): BigNumber
    {
        const functionName = format('Order.getVariableCommissionBN');

        let variableCommissionBN = new BigNumber(0);

        if (this.side === 'sell' &&
            (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'variableCurrency') )
        {
            // negative amount
            variableCommissionBN = new BigNumber(this.getCommission(this.variableCurrency)).negated();
        }
        else if (this.side === 'buy' &&
            (this.exchange.feeStructure === 'sellCurrency' || this.exchange.feeStructure === 'variableCurrency') )
        {
            // positive amount
            variableCommissionBN = new BigNumber(this.getCommission(this.variableCurrency));
        }

        logger.trace('%s variable commission = %s', functionName,
            variableCommissionBN.toString() );

        return variableCommissionBN;
    }

    // TODO add unit tests
    getFixedCommissionBN(): BigNumber
    {
        const functionName = format('Order.getFixedCommissionBN');

        let fixedCommissionBN = new BigNumber(0);

        if (this.side === 'buy' &&
            (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'fixedCurrency') )
        {
            // negative amount
            fixedCommissionBN = new BigNumber(this.getCommission(this.fixedCurrency)).negated();
        }
        else if (this.side === 'sell' &&
            (this.exchange.feeStructure === 'sellCurrency' || this.exchange.feeStructure === 'fixedCurrency') )
        {
            // positive amount
            fixedCommissionBN = new BigNumber(this.getCommission(this.fixedCurrency));
        }

        logger.trace('%s fixed commission = %s', functionName,
            fixedCommissionBN.toString() );

        return fixedCommissionBN;
    }

    setAmountsFromNetSellAmount(sellAmountBN: BigNumber): void | Error
    {
        const functionName = 'Order.setAmountsFromNetSellAmount()';

        let tradeAmountBN: BigNumber,
            commissionRate: number;

        if (!sellAmountBN || !(sellAmountBN instanceof BigNumber))
        {
            const error = new VError('%s parameter sellAmountBN %s needs to be passed and be of type BigNumber', functionName,
                sellAmountBN);
            logger.error(error.stack);
            return error;
        }

        if (this.amount || this.amountRemaining || this.amountTraded !== '0' || this.amountLastPartial !== '0')
        {
            logger.debug('%s existing trade amounts %s, remaining %s, traded %s and last partial %s are about to be overridden to get a sell amount of %s', functionName,
                this.amount, this.amountRemaining, this.amountTraded, this.amountLastPartial,
                sellAmountBN.toString() );
        }

        // if sell order then sell amount is in fixed currency
        if (this.side === 'sell')
        {
            // if fees are charged in sell amount
            if (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'sellCurrency')
            {
                commissionRate = this.getCommission(this.fixedCurrency);

                tradeAmountBN = sellAmountBN.
                div(BigNumber(1).
                plus(commissionRate) ).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);

                logger.trace('%s new trade amount %s = net sell amount %s / (1 + fixed currency %s commission %s)', functionName,
                    tradeAmountBN.toString(),
                    sellAmountBN.toString(),
                    this.fixedCurrency, commissionRate);
            }
            // else fees are NOT charged in sell amount
            else if (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'buyCurrency')
            {
                tradeAmountBN = sellAmountBN.
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
            }
        }
        // if buy order then net sell amount is in variable currency so will need converting to fixed currency trade amounts
        else if (this.side === 'buy')
        {
            if (!this.price)
            {
                const error = new VError('%s price is not defined. This is needed to convert the variable currency net sell amount to fixed currency trade amounts', functionName);
                logger.error(error.stack);
                return error;
            }

            if (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'sellCurrency')
            {
                commissionRate = this.getCommission(this.variableCurrency);

                tradeAmountBN = sellAmountBN.
                div(this.price).
                div(BigNumber(1).
                plus(commissionRate) ).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);

                logger.trace('%s new trade amount %s = net sell amount %s / (price %s * (1 + variable currency %s commission %s))', functionName,
                    tradeAmountBN.toString(),
                    sellAmountBN.toString(),
                    this.price,
                    this.variableCurrency, commissionRate);
            }
            else if (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'buyCurrency')
            {
                tradeAmountBN = sellAmountBN.
                div(this.price).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN);

                logger.trace('%s new trade amount %s = net sell amount %s / price %s', functionName,
                    tradeAmountBN.toString(),
                    sellAmountBN.toString(),
                    this.price);
            }
        }

        this.amount = tradeAmountBN.toString();
        this.amountRemaining = this.amount;
        this.amountTraded = '0';
        this.amountLastPartial = '0';
        this.numberPartialFills = 0;
    }

    setAmountsFromNetBuyAmount(buyAmountBN: BigNumber): Error | void
    {
        const functionName = 'Order.setAmountsFromNetBuyAmount()';

        let tradeAmountBN: BigNumber,
            commissionRate: number;

        if (!buyAmountBN || !(buyAmountBN instanceof BigNumber))
        {
            const error = new VError('%s parameter buyAmountBN %s needs to be passed and be of type BigNumber', functionName,
                buyAmountBN);
            logger.error(error.stack);
            return error;
        }

        if (this.amount || this.amountRemaining || this.amountTraded || this.amountLastPartial)
        {
            logger.debug('%s existing trade amounts %s, remaining %s, traded %s and last partial %s are about to be overridden to get a buy amount of %s', functionName,
                buyAmountBN.toString() );
        }

        // if sell order then net buy amount is in variable currency so will need converting to fixed currency trade amounts
        if (this.side === 'sell')
        {
            if (!this.price)
            {
                const error = new VError('%s price is not defined. This is needed to convert the variable currency net buy amount to fixed currency trade amounts', functionName);
                logger.error(error.stack);
                return error;
            }

            // if fees are NOT changed in buy amount
            if (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'sellCurrency')
            {
                tradeAmountBN = buyAmountBN.
                div(this.price).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN);

                logger.trace('%s new trade amount %s = net buy amount %s / price %s', functionName,
                    tradeAmountBN.toString(),
                    buyAmountBN.toString(),
                    this.price);
            }
            // if fees are changed in buy amount
            else if (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'buyCurrency')
            {
                commissionRate = this.getCommission(this.variableCurrency);

                tradeAmountBN = buyAmountBN.
                div(this.price).
                div(BigNumber(1).
                minus(commissionRate) ).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);

                logger.trace('%s new trade amount %s = net buy amount %s / (price %s * (1 - variable currency %s commission %s))', functionName,
                    tradeAmountBN.toString(),
                    buyAmountBN.toString(),
                    this.price,
                    this.variableCurrency, commissionRate);
            }
        }
        // if buy order then net buy amount is in fixed currency
        else if (this.side === 'buy')
        {
            // if fees are NOT changed in net buy amount
            if (this.exchange.feeStructure === 'variableCurrency' || this.exchange.feeStructure === 'sellCurrency')
            {
                tradeAmountBN = buyAmountBN.
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);
            }
            // if fees are changed in net buy amount
            else if (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'buyCurrency')
            {
                commissionRate = this.getCommission(this.fixedCurrency);

                tradeAmountBN = buyAmountBN.
                div(BigNumber(1).
                minus(commissionRate) ).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);

                logger.trace('%s new trade amount %s = net buy amount %s / (1 - fixed currency %s commission %s)', functionName,
                    tradeAmountBN.toString(),
                    buyAmountBN.toString(),
                    this.fixedCurrency, commissionRate);
            }
        }

        this.amount = tradeAmountBN.toString();
        this.amountRemaining = this.amount;
        this.amountTraded = '0';
        this.amountLastPartial = '0';
        this.numberPartialFills = 0;
    }

    setAmountsFromNetFixedAmount(fixedAmountBN: BigNumber): Error | void
    {
        const functionName = 'Order.setAmountsFromNetFixedAmount()';

        let tradeAmountBN: BigNumber,
            commissionRate = 0;     // default the commission to zero

        if (!fixedAmountBN || !(fixedAmountBN instanceof BigNumber))
        {
            const error = new VError('%s parameter fixedAmountBN %s needs to be passed and be of type BigNumber', functionName,
                fixedAmountBN);
            logger.error(error.stack);
            return error;
        }

        if (this.amount || this.amountRemaining || this.amountTraded !== "0" || this.amountLastPartial !== "0" )
        {
            logger.debug('%s existing trade amounts %s, remaining %s, traded %s and last partial %s are about to be overridden to get a fixed amount of %s', functionName,
                fixedAmountBN.toString());
        }

        // if sell order and fee charged in fixed currency
        if (this.side === 'sell' &&
            (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'sellCurrency'))
        {
            // fee is added to trade amount to get net fixed amount
            commissionRate = this.getCommission(this.fixedCurrency);
        }
        // if buy order and fee charged in fixed currency
        else if (this.side === 'buy' &&
            (this.exchange.feeStructure === 'fixedCurrency' || this.exchange.feeStructure === 'buyCurrency'))
        {
            // fee is taken from trade amount to get fixed amount
            commissionRate = -1 * this.getCommission(this.fixedCurrency);
        }

        tradeAmountBN = fixedAmountBN.
        div(BigNumber(1).
        plus(commissionRate) ).
        round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_HALF_UP);

        logger.trace('%s new trade amount %s = net fixed amount %s / (1 + fixed currency %s commission %s))', functionName,
            tradeAmountBN.toString(),
            fixedAmountBN.toString(),
            this.fixedCurrency, commissionRate);

        this.amount = tradeAmountBN.toString();
        this.amountRemaining = this.amount;
        this.amountTraded = '0';
        this.amountLastPartial = '0';
        this.numberPartialFills = 0;
    }

    fill(): void
    {
        const functionName = 'Order.fill()';

        logger.trace('%s about to fill order with state %s, id %s, tag %s, amount %s, amountRemaining %s and amountTraded %s', functionName,
            this.state, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountTraded);

        this.state = 'filled';

        const oldAmountTraded = this.amountTraded;
        this.amountTraded = new BigNumber(this.amountRemaining).
            plus(this.amountTraded).
            round(this.exchange.currencyRounding[this.market.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();

        logger.debug('%s amount trade = old amount traded %s + amount remaining %s = %s', functionName,
            oldAmountTraded, this.amountRemaining, this.amountTraded);

        this.amountLastPartial = this.amountRemaining;
        this.numberPartialFills++;
        this.amountRemaining = '0';

        logger.debug('%s filled order with id %s, tag %s, amount %s, amountRemaining %s, amountTraded %s, amountLastPartial %s and numberPartialFills %s', functionName,
            this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountTraded, this.amountLastPartial, this.numberPartialFills);
    }

    partialFill(fillAmount: string): void
    {
        const functionName = 'Order.partialFill()';

        logger.trace('%s about to process partial fill amount %s for order with id %s, tag %s, amount %s, amountRemaining %s and amountTraded %s', functionName,
            fillAmount, this.exchangeId, this.tag, this.amount, this.amountRemaining, this.amountTraded);

        // if the fill amount >= trade amount remaining
        if (BigNumber(fillAmount).greaterThanOrEqualTo(this.amountRemainingBN) ) {
            logger.debug('%s fill amount %s >= trade amount remaining %s so will mark the %s order with id %s and tag %s as filled instead of partially filled', functionName,
                fillAmount, this.amountRemaining, this.side, this.exchangeId, this.tag);

            this.fill();

            return;
        }

        this.state = 'partiallyFilled';

        const oldAmountTraded = this.amountTraded;
        this.amountTraded = new BigNumber(this.amountTraded).
            plus(fillAmount).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();

        logger.debug('%s new amount traded = old amount traded %s + fill amount %s = %s', functionName,
            oldAmountTraded, fillAmount, this.amountTraded);

        const oldAmountRemaining = this.amountRemaining;
        this.amountRemaining = new BigNumber(this.amountRemaining).
            minus(fillAmount).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();

        logger.debug('%s new amount remaining = old amount remaining %s - fill amount %s = %s', functionName,
            oldAmountRemaining, fillAmount, this.amountRemaining);

        this.amountLastPartial = fillAmount;
        this.numberPartialFills++;

        logger.debug('%s amount last partial = %s and number of partial fills = %s', functionName,
            this.amountLastPartial, this.numberPartialFills);
    }

    // returns a cloned order for the remaining pending amount of a partially filled order
    remainingPartial(): Order
    {
        const functionName = 'Order.remainingPartial()';

        // create new order for the remaining pending amount
        const newRemainingOrder = this.clone();

        newRemainingOrder.state = 'pending';

        logger.debug('%s cloned new pending %s order from partially filled %s order with id %s and amount %s', functionName,
            newRemainingOrder.tag, newRemainingOrder.side, newRemainingOrder.exchangeId, newRemainingOrder.amount);

        return newRemainingOrder;
    }

    // returns a cloned order with a new price with the sell amount preserved
    cloneFromAdjustedRate(rates: IRates, minTradeAmountBN?: BigNumber, maxTradeAmountBN?: BigNumber): Order
    {
        const functionName = 'Order.cloneFromAdjustedRate()';

        if (!minTradeAmountBN) minTradeAmountBN = new BigNumber(0);
        if (!maxTradeAmountBN) maxTradeAmountBN = new BigNumber(Infinity);

        // create new order for the new price
        const newAdjustedOrder = this.clone();
        newAdjustedOrder.price = rates.bestRate;
        newAdjustedOrder.minRate = rates.minRate;
        newAdjustedOrder.maxRate = rates.maxRate;

        if (this.side === 'buy' && this.price != rates.bestRate)
        {
            // new remaining amount = old remaining amount * old price / new price
            newAdjustedOrder.amountRemaining = this.amountRemainingBN.
            times(this.priceBN).
            div(rates.bestRate).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();

            logger.debug('%s new remaining amount %s = old remaining amount %s * old rate %s / new rate %s', functionName,
                newAdjustedOrder.amountRemaining, this.amountRemaining, this.price, rates.bestRate);

            // amount = old amount + (new remaining amount - old remaining amount)
            newAdjustedOrder.amount = newAdjustedOrder.amountBN.
            plus(newAdjustedOrder.amountRemainingBN).
            minus(this.amountRemainingBN).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();

            logger.debug('%s new amount %s = old amount %s + (new remaining amount %s - old remaining amount %s)', functionName,
                newAdjustedOrder.amount, this.amount, newAdjustedOrder.amountRemaining, this.amountRemaining);

            // if new amount remaining < min trade amount
            if (newAdjustedOrder.amountRemainingBN.lessThan(minTradeAmountBN) )
            {
                const oldAmountBN = newAdjustedOrder.amountBN;

                // new trade amount = old trade amount + (min trade amount - old remaining amount)
                newAdjustedOrder.amount = newAdjustedOrder.amountBN.
                plus(minTradeAmountBN).
                minus(newAdjustedOrder.amountRemaining).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.debug('%s new trade amount %s = old trade amount %s + (min trade amount %s - old remaining amount %s)', functionName,
                    newAdjustedOrder.amount, oldAmountBN.toString(),
                    minTradeAmountBN.toString(),
                    newAdjustedOrder.amountRemaining);

                newAdjustedOrder.amountRemaining = minTradeAmountBN.toString();
            }
            // else if sell amount remaining > max sell amount
            else if (newAdjustedOrder.amountRemainingBN.greaterThan(maxTradeAmountBN) )
            {
                const oldAmountBN = newAdjustedOrder.amountBN;

                // new trade amount = old trade amount - (old remaining amount - max trade amount)
                newAdjustedOrder.amount = newAdjustedOrder.amountBN.
                minus(newAdjustedOrder.amountRemaining).
                plus(maxTradeAmountBN).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

                logger.debug('%s new trade amount %s = old trade amount %s - (old remaining amount %s - max trade amount %s)', functionName,
                    newAdjustedOrder.amount, oldAmountBN.toString(),
                    newAdjustedOrder.amountRemaining,
                    maxTradeAmountBN.toString() );

                newAdjustedOrder.amountRemaining = maxTradeAmountBN.toString();
            }
        }

        logger.debug('%s cloned new %s order for id %s, tag %s with rate %s->%s, amount %s->%s, remaining amount %s->%s, sell amount remaining %s->%s', functionName,
            newAdjustedOrder.side, newAdjustedOrder.exchangeId, newAdjustedOrder.tag,
            this.price, newAdjustedOrder.price,
            this.amount, newAdjustedOrder.amount,
            this.amountRemaining, newAdjustedOrder.amountRemaining,
            this.sell.amountRemaining, newAdjustedOrder.sell.amountRemaining);

        return newAdjustedOrder;
    }

    // used to append a partial fill to a previous partial fill
    // this is used by the Simulator to merge multiple partial fills of the same order
    appendPartialFill(fillAmount: string): void
    {
        const functionName = 'Order.appendPartialFill()';

        let oldAmountTraded: string,
            oldAmountRemaining: string,
            oldAmountLastPartial: string;

        // if fill amount >= remaining amount
        if (BigNumber(fillAmount).greaterThanOrEqualTo(this.amountRemaining) )
        {
            logger.debug('%s fill amount >= amount remaining so will fill the order', functionName,
                fillAmount, this.amountRemaining);

            this.state = 'filled';

            // increment the last partial amount by the remaining amount as it's a filled order
            // note this does not equal the amountRemaining as there could have been another partial fill earlier in the request
            oldAmountLastPartial = this.amountLastPartial;
            this.amountLastPartial = this.amountLastPartialBN.
            plus(this.amountRemaining).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();

            logger.debug('%s new amount last partial = old amount last partial %s + amount remaining %s = %s', functionName,
                oldAmountLastPartial, this.amountRemaining, this.amountLastPartial);

            // increment the traded amount
            oldAmountTraded = this.amountLastPartial;
            this.amountTraded = this.amountTradedBN.
            plus(this.amountRemaining).
            round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
            toString();

            logger.debug('%s new amount traded = old amount traded %s + amount remaining %s = %s', functionName,
                oldAmountTraded, this.amountRemaining, this.amountTraded);

            this.amountRemaining = '0';

            logger.debug('%s filled %s with id %s, amount %s, amountRemaining %s, amountTraded %s, amountLastPartial %s and numberPartialFills %s', functionName,
                this.tag, this.exchangeId, this.amount, this.amountRemaining, this.amountTraded, this.amountLastPartial, this.numberPartialFills);
        }
        else
        {
            logger.debug('%s fill amount < amount remaining so will partially fill the order', functionName,
                fillAmount, this.amountRemaining);

            this.state = 'partiallyFilled';

            oldAmountTraded = this.amountTraded;
            this.amountTraded = new BigNumber(this.amountTraded).
                plus(fillAmount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

            logger.debug('%s new amount traded = old amount traded %s + fill amount %s = %s', functionName,
                oldAmountTraded, fillAmount, this.amountTraded);

            oldAmountRemaining = this.amountRemaining;
            this.amountRemaining = new BigNumber(this.amountRemaining).
                minus(fillAmount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

            logger.debug('%s new amount remaining = old amount remaining %s - fill amount %s = %s', functionName,
                oldAmountRemaining, fillAmount, this.amountRemaining);

            oldAmountLastPartial = this.amountLastPartial;
            this.amountLastPartial = this.amountLastPartialBN.
                plus(fillAmount).
                round(this.exchange.currencyRounding[this.fixedCurrency], BigNumber.ROUND_DOWN).
                toString();

            logger.debug('%s new amount last partial = old amount last partial %s + fill amount %s = %s', functionName,
                oldAmountLastPartial, fillAmount, this.amountLastPartial);
        }
    }

    cancel(): void {
        this.state = 'cancelled';
    }

    log(level: string): void
    {
        if (!level) {
            level = 'debug';
        }

        logger.log(level, "%s[%s] id %s, tag %s, state %s, side %s, amount %s %s, price %s, remaining %s, traded %s, filled %s%, Buy %s %s, Sell %s %s, Fee %s %s, num partials %s last partial %s, max buy rate %s, min sell rate %s, %s",
            this.exchangeName, this.symbol, this.exchangeId, this.tag,
            this.state, this.side,
            this.amount, this.fixedCurrency,
            this.price, this.amountRemaining, this.amountTraded, this.tradedPercentage,
            this.buy.amount, this.buy.currency,
            this.sell.amount, this.sell.currency,
            this.fee.amount, this.fee.currency,
            this.numberPartialFills, this.amountLastPartial,
            this.maxRate, this.minRate,
            moment(this.timestamp).format('D-MMM-YY H:mm:ss')
        );
    }
}