/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
// load third party packages
var _ = require('underscore');
var util_1 = require('util');
var balance_1 = require("./balance");
var logger = require('config-logger'), VError = require('verror');
/**
 * Stores an array of balance objects for each currency in the account
 * @param balances an array of balance objects
 * @param exchangeName the name of the exchange used in the Exchanges object
 * @param rounding an associative array of currencies referencing the number of decimal places for each currency. eg {BTC:8, LTC:8, AUD:2},
 */
var Account = (function () {
    function Account(balances, exchangeName, rounding) {
        this.exchangeName = exchangeName;
        this.rounding = rounding;
        this.balances = {};
        var functionName = 'Account.constructor()', self = this;
        balances.forEach(function (balance) {
            logger.debug('%s setting balance of %s to %s total and %s available', functionName, balance.currency, balance.totalBalance, balance.availableBalance);
            self.balances[balance.currency] = balance.clone;
        });
    }
    Account.prototype.setBalance = function (newBalance) {
        var functionName = util_1.format('%s Account.setBalance()', this.exchangeName);
        this.balances[newBalance.currency] = newBalance.clone;
        logger.debug('%s set %s balance with total = %s and available = %s', functionName, newBalance.currency, newBalance.totalBalance, newBalance.availableBalance);
    };
    ;
    Account.prototype.addOrder = function (newOrder) {
        var functionName = util_1.format('%s Account.addOrder()', this.exchangeName);
        this.initCurrencies(newOrder);
        var previousSellAvailableBalanceBN = this.balances[newOrder.sell.currency].availableBalanceBN;
        this.balances[newOrder.sell.currency].availableBalance = previousSellAvailableBalanceBN.
            minus(newOrder.sell.amountRemaining).
            round(this.rounding[newOrder.sell.currency]).
            toString();
        logger.debug('%s %s available balance %s = old balance %s - sell amount remaining %s for order id %s and tag %s', functionName, newOrder.sell.currency, this.balances[newOrder.sell.currency].availableBalance, previousSellAvailableBalanceBN.toString(), newOrder.sell.amountRemaining, newOrder.exchangeId, newOrder.tag);
    };
    ;
    Account.prototype.addMarketTrade = function (order) {
        var functionName = util_1.format('%s Account.addMarketTrade()', this.exchangeName);
        if (order.state != 'filled') {
            var error = new VError('%s market order state %s is not filled', functionName, order.state);
            logger.error(error.stack);
            throw error;
        }
        else if (order.amountTradedBN.lessThanOrEqualTo(0)) {
            var error = new VError('%s market traded amount %s is not > 0', functionName, order.amountTraded);
            logger.error(error.stack);
            throw error;
        }
        this.initCurrencies(order);
        var previousSellAvailableBalanceBN = this.balances[order.sell.currency].availableBalanceBN, previousSellTotalBalanceBN = this.balances[order.sell.currency].totalBalanceBN, previousBuyAvailableBalanceBN = this.balances[order.buy.currency].availableBalanceBN, previousBuyTotalBalanceBN = this.balances[order.buy.currency].totalBalanceBN;
        this.balances[order.sell.currency].availableBalance = previousSellAvailableBalanceBN.
            minus(order.sell.amountTraded).
            round(this.rounding[order.sell.currency]).
            toString();
        logger.debug('%s available %s balance %s = old balance %s - sell amount traded %s for order with id %s and tag %s', functionName, order.sell.currency, this.balances[order.sell.currency].availableBalance, previousSellAvailableBalanceBN.toString(), order.sell.amountTraded, order.exchangeId, order.tag);
        this.balances[order.sell.currency].totalBalance = previousSellTotalBalanceBN.
            minus(order.sell.amountTraded).
            round(this.rounding[order.sell.currency]).
            toString();
        logger.debug('%s total %s balance %s = old balance %s - sell amount traded %s for order with id %s and tag %s', functionName, order.sell.currency, this.balances[order.sell.currency].totalBalance, previousSellTotalBalanceBN.toString(), order.sell.amountTraded, order.exchangeId, order.tag);
        this.balances[order.buy.currency].availableBalance = previousBuyAvailableBalanceBN.
            plus(order.buy.amountTraded).
            round(this.rounding[order.buy.currency]).
            toString();
        logger.debug('%s available %s balance %s = old balance %s + buy amount traded %s for order with id %s and tag %s', functionName, order.buy.currency, this.balances[order.buy.currency].availableBalance, previousBuyAvailableBalanceBN.toString(), order.buy.amountTraded, order.exchangeId, order.tag);
        this.balances[order.buy.currency].totalBalance = previousBuyTotalBalanceBN.
            plus(order.buy.amountTraded).
            round(this.rounding[order.buy.currency]).
            toString();
        logger.debug('%s total %s balance %s = old balance %s + buy amount traded %s for order with id %s and tag %s', functionName, order.buy.currency, this.balances[order.buy.currency].totalBalance, previousBuyTotalBalanceBN.toString(), order.buy.amountTraded, order.exchangeId, order.tag);
    };
    ;
    Account.prototype.cancelOrder = function (cancelledOrder) {
        var functionName = util_1.format('%s Account.cancelOrder()', this.exchangeName);
        this.initCurrencies(cancelledOrder);
        var previousSellAvailableBalanceBN = this.balances[cancelledOrder.sell.currency].availableBalanceBN;
        this.balances[cancelledOrder.sell.currency].availableBalance = previousSellAvailableBalanceBN.
            plus(cancelledOrder.sell.amountRemaining).
            round(this.rounding[cancelledOrder.sell.currency]).
            toString();
        logger.debug('%s available %s balance %s = old balance %s + sell amount remaining %s for order with id %s and tag %s', functionName, cancelledOrder.sell.currency, this.balances[cancelledOrder.sell.currency].availableBalance, previousSellAvailableBalanceBN.toString(), cancelledOrder.sell.amountRemaining, cancelledOrder.exchangeId, cancelledOrder.tag);
    };
    ;
    /**
     * Updates balances for an Order that is about to be filled. Uses amountRemaining
     * @param filledOrder
     */
    Account.prototype.fillOrder = function (filledOrder) {
        var functionName = util_1.format('%s Account.fillOrder()', this.exchangeName);
        this.initCurrencies(filledOrder);
        var previousBuyAvailableBalanceBN = this.balances[filledOrder.buy.currency].availableBalanceBN, previousBuyTotalBalanceBN = this.balances[filledOrder.buy.currency].totalBalanceBN, previousSellTotalBalanceBN = this.balances[filledOrder.sell.currency].totalBalanceBN;
        this.balances[filledOrder.buy.currency].totalBalance = previousBuyTotalBalanceBN.
            plus(filledOrder.buy.amountRemaining).
            round(this.rounding[filledOrder.buy.currency]).
            toString();
        logger.debug('%s total %s balance %s = old balance %s + buy amount remaining %s for order with id %s and tag %s', functionName, filledOrder.buy.currency, this.balances[filledOrder.buy.currency].totalBalance, previousBuyTotalBalanceBN.toString(), filledOrder.buy.amountRemaining, filledOrder.exchangeId, filledOrder.tag);
        this.balances[filledOrder.buy.currency].availableBalance = previousBuyAvailableBalanceBN.
            plus(filledOrder.buy.amountRemaining).
            round(this.rounding[filledOrder.buy.currency]).
            toString();
        logger.debug('%s available %s balance %s = old balance %s + buy amount remaining %s for order with id %s and tag %s', functionName, filledOrder.buy.currency, this.balances[filledOrder.buy.currency].availableBalance, previousBuyAvailableBalanceBN.toString(), filledOrder.buy.amountRemaining, filledOrder.exchangeId, filledOrder.tag);
        this.balances[filledOrder.sell.currency].totalBalance = previousSellTotalBalanceBN.
            minus(filledOrder.sell.amountRemaining).
            round(this.rounding[filledOrder.sell.currency]).
            toString();
        logger.debug('%s total %s balance %s = old balance %s - sell amount remaining %s for order with id %s and tag %s', functionName, filledOrder.sell.currency, this.balances[filledOrder.sell.currency].totalBalance, previousSellTotalBalanceBN.toString(), filledOrder.sell.amountRemaining, filledOrder.exchangeId, filledOrder.tag);
    };
    ;
    /**
     * Updates balances for an Order that was just filled. Uses amountLastPartial
     * @param filledOrder
     */
    Account.prototype.filledOrder = function (filledOrder) {
        var functionName = util_1.format('%s Account.filledOrder()', this.exchangeName);
        this.initCurrencies(filledOrder);
        var previousBuyAvailableBalanceBN = this.balances[filledOrder.buy.currency].availableBalanceBN, previousBuyTotalBalanceBN = this.balances[filledOrder.buy.currency].totalBalanceBN, previousSellTotalBalanceBN = this.balances[filledOrder.sell.currency].totalBalanceBN;
        logger.debug("%s adjusting %s and %s currencies for filled order with id %s, rate %s, amount %s, amount remaining %s and last partial amount %s", functionName, filledOrder.buy.currency, filledOrder.sell.currency, filledOrder.exchangeId, filledOrder.price, filledOrder.amount, filledOrder.amountRemaining, filledOrder.amountLastPartial);
        this.balances[filledOrder.buy.currency].totalBalance = previousBuyTotalBalanceBN.
            plus(filledOrder.buy.amountLastPartial).
            round(this.rounding[filledOrder.buy.currency]).
            toString();
        logger.debug('%s total %s balance %s = old balance %s + buy amount last partial %s', functionName, filledOrder.buy.currency, this.balances[filledOrder.buy.currency].totalBalance, previousBuyTotalBalanceBN.toString(), filledOrder.buy.amountLastPartial);
        this.balances[filledOrder.buy.currency].availableBalance = previousBuyAvailableBalanceBN.
            plus(filledOrder.buy.amountLastPartial).
            round(this.rounding[filledOrder.buy.currency]).
            toString();
        logger.debug('%s available %s balance %s = old balance %s + buy amount last partial %s', functionName, filledOrder.buy.currency, this.balances[filledOrder.buy.currency].availableBalance, previousBuyAvailableBalanceBN.toString(), filledOrder.buy.amountLastPartial);
        this.balances[filledOrder.sell.currency].totalBalance = previousSellTotalBalanceBN.
            minus(filledOrder.sell.amountLastPartial).
            round(this.rounding[filledOrder.sell.currency]).
            toString();
        logger.debug('%s total %s balance %s = old balance %s - sell amount last partial %s', functionName, filledOrder.sell.currency, this.balances[filledOrder.sell.currency].totalBalance, previousSellTotalBalanceBN.toString(), filledOrder.sell.amountLastPartial);
    };
    ;
    Account.prototype.initCurrencies = function (order) {
        var functionName = util_1.format('%s Account.initCurrencies()', this.exchangeName);
        logger.trace('%s initialising currency balances to zero if not already set', functionName);
        if (!this.balances[order.sell.currency]) {
            logger.debug('%s sell currency %s of order has not been initialised. Setting to zero balance', functionName, order.sell.currency);
            this.balances[order.sell.currency] = new balance_1.default({
                exchangeName: this.exchangeName,
                currency: order.sell.currency,
                totalBalance: '0',
                availableBalance: '0'
            });
        }
        if (!this.balances[order.buy.currency]) {
            logger.debug('%s buy currency %s of order has not been initialised. Setting to zero balance', functionName, order.buy.currency);
            this.balances[order.buy.currency] = new balance_1.default({
                exchangeName: this.exchangeName,
                currency: order.buy.currency,
                totalBalance: '0',
                availableBalance: '0'
            });
        }
    };
    ;
    Account.prototype.clone = function () {
        var clonedCurrencies = [];
        _.values(this.balances).forEach(function (currency) {
            clonedCurrencies.push(currency.clone);
        });
        return new Account(clonedCurrencies, this.exchangeName, this.rounding);
    };
    Account.prototype.log = function () {
        var functionName = 'Account.log()';
        var msg = util_1.format("Balances on exchange %s:", this.exchangeName);
        // for each balance in the list of currencies
        _.values(this.balances).forEach(function (balance) {
            // add the balance log line to the log message
            return msg + util_1.format('\n%s %s total balance = %s, available balance = %s', functionName, balance.currency, balance.totalBalance, balance.availableBalance);
        });
        return msg;
    };
    return Account;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Account;
