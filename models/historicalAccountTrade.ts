/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

import * as moment from 'moment';
import * as _ from 'underscore';
import {format} from 'util';
import BigNumber = require('bignumber.js');
type BigNumber = bignumber.BigNumber;

// import my Interfaces
import Exchange from "./exchange";
import Market from "./market";
import {ITradeAmounts, IGrossTradeAmounts, IFixVarTradeAmounts} from "./tradeAmounts";

// My packages
const logger = require('config-logger'),
    VError = require('verror');

export interface IHistoricalAccountTrade {
    exchangeName: string,
    symbol: string,
    side: string,
    priceBN: BigNumber,
    quantityBN: BigNumber,
    timestamp: Date,
    orderId?: string,
    tradeId?: string,
    feeAmountBN?: BigNumber,
    feeCurrency?: string,
    rebateAmountBN?: BigNumber,
    rebateCurrency?: string
}

export default class HistoricalAccountTrade
{
    exchangeName: string;
    symbol: string;
    side: string;
    priceBN: BigNumber;
    quantityBN: BigNumber;
    timestamp: Date;
    orderId: string;
    tradeId: string;
    feeAmountBN = BigNumber(NaN);
    feeCurrency: string;
    rebateAmountBN: BigNumber;
    rebateCurrency: string;

    constructor(settings: IHistoricalAccountTrade)
    {
        _.extend(this, settings);
    }

    get fixedCurrency(): string
    {
        return Market.getFixedCurrency(this.symbol);
    }

    get variableCurrency(): string
    {
        return Market.getVariableCurrency(this.symbol);
    }

    // calculate the net fixed currency amount after a trade. That is, fixed currency amount after fees. eg BTC for BTCUSD
    get netFixedAmountBN(): BigNumber
    {
        if (this.fixedCurrency == this.feeCurrency)
        {
            if (this.side == 'sell')
            {
                return this.quantityBN.
                    plus(this.feeAmountBN);
            }
            else if (this.side == 'buy')
            {
                return this.quantityBN.
                    minus(this.feeAmountBN);
            }
        }
        // fee is in variable currency
        else
        {
            // just return the trade quantity
            return this.quantityBN;
        }
    }

    // calculate the net variable currency amount after a trade. That is, variable currency amount after fees. eg USD for BTCUSD
    get netVariableAmountBN(): BigNumber
    {
        if (this.variableCurrency == this.feeCurrency)
        {
            if (this.side == 'sell')
            {
                return this.quantityBN.
                    times(this.priceBN).
                    minus(this.feeAmountBN);
            }
            else if (this.side == 'buy')
            {
                return this.quantityBN.
                    times(this.priceBN).
                    plus(this.feeAmountBN);
            }
        }
        // fee is in variable currency
        else
        {
            // just return the variable amount
            return this.quantityBN.
                times(this.priceBN);
        }
    }
}