/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var _ = require('underscore');
var events_1 = require('events');
var BigNumber = require('bignumber.js');
var exchange_1 = require("./exchange");
var logger = require('config-logger'), VError = require('verror');
var Market = (function (_super) {
    __extends(Market, _super);
    function Market(settings) {
        _super.call(this);
        this.tickers = [];
        this.orders = [];
        this.exchangeName = settings.exchangeName;
        this.symbol = settings.symbol;
        // Set helper properties
        this.fixedCurrency = Market.getFixedCurrency(this.symbol);
        this.variableCurrency = Market.getVariableCurrency(this.symbol);
    }
    Market.getFixedCurrency = function (symbol) {
        return symbol.slice(0, 3);
    };
    Market.getVariableCurrency = function (symbol) {
        return symbol.slice(3);
    };
    Object.defineProperty(Market.prototype, "exchange", {
        // Create reference to the exchange which is not saved into the database
        get: function () {
            var functionName = 'Market.exchange()', returnedExchange = exchange_1.default.exchanges[this.exchangeName];
            if (!returnedExchange) {
                var error = new VError('%s exchange %s can not be found in the static Exchange.exchanges', functionName, this.exchangeName);
                logger.debug(error);
                throw error;
            }
            return returnedExchange;
        },
        enumerable: true,
        configurable: true
    });
    ;
    // returns a matrix that can be used to multiple the trade amount to get the buy and sell fees
    Market.prototype.getCommissionMatrix = function (commissionType) {
        var functionName = 'Market.getCommissionMatrix()';
        var exchangeCommissionType;
        if (commissionType != 'maker' && commissionType != 'taker') {
            var error = new VError('%s first parameter commissionType %s needs to be either "maker" or "taker"', functionName, commissionType);
            logger.error(error.stack);
            return error;
        }
        var commissionMatrix = {
            sell: {},
            buy: {}
        };
        if (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'variableCurrency') {
            // negative as fee is on the buy currency
            commissionMatrix.sell[this.fixedCurrency] = new BigNumber(0);
            commissionMatrix.buy[this.variableCurrency] = new BigNumber(this.exchange.commissions[this.variableCurrency][commissionType]).
                negated();
        }
        else if (this.exchange.feeStructure === 'sellCurrency' || this.exchange.feeStructure === 'fixedCurrency') {
            // positive as the fee is on the sell currency
            commissionMatrix.sell[this.fixedCurrency] = new BigNumber(this.exchange.commissions[this.fixedCurrency][commissionType]);
            commissionMatrix.buy[this.variableCurrency] = new BigNumber(0);
        }
        if (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'fixedCurrency') {
            // negative as the fee is on the buy currency
            commissionMatrix.buy[this.fixedCurrency] = new BigNumber(this.exchange.commissions[this.fixedCurrency][commissionType]).
                negated();
            commissionMatrix.sell[this.variableCurrency] = new BigNumber(0);
        }
        else if (this.exchange.feeStructure === 'sellCurrency' || this.exchange.feeStructure === 'variableCurrency') {
            // positive as the fee is on the sell currency
            commissionMatrix.buy[this.fixedCurrency] = new BigNumber(0);
            commissionMatrix.sell[this.variableCurrency] = new BigNumber(this.exchange.commissions[this.variableCurrency][commissionType]);
        }
        logger.trace('%s sell %s %s; %s %s; buy %s %s, %s %s', functionName, this.fixedCurrency, commissionMatrix.sell[this.fixedCurrency].toString(), this.variableCurrency, commissionMatrix.sell[this.variableCurrency].toString(), this.fixedCurrency, commissionMatrix.buy[this.fixedCurrency].toString(), this.variableCurrency, commissionMatrix.buy[this.variableCurrency].toString());
        return commissionMatrix;
    };
    /**
     * Add's a new ticker to the market if it's different from the previous ticker
     * @Param {Ticker} new Ticker to be added to the market
     */
    Market.prototype.addTicker = function (newTicker, callback) {
        // If there has been a change in the bid, ask or last ticker
        if (!this.previousTicker ||
            this.previousTicker.bid != newTicker.bid ||
            this.previousTicker.ask != newTicker.ask ||
            this.previousTicker.last != newTicker.last) {
            this.tickers.push(newTicker);
            this.previousTicker = this.latestTicker;
            this.latestTicker = newTicker;
            this.emit('ticker', newTicker);
        }
        if (callback)
            callback(newTicker);
    };
    ;
    /**
     * Add's a new OrderBook to the market
     * @Param {OrderBook} newOrderBook to be added to the market
     */
    Market.prototype.addOrderBook = function (newOrderBook, callback) {
        this.previousOrderBook = this.latestOrderBook;
        this.latestOrderBook = newOrderBook;
        this.emit('newOrderBook', newOrderBook);
        if (callback)
            callback(newOrderBook);
    };
    /**
     * Add's a new order to a market
     * @Param {Order} newOrder new order to be added to the market
     */
    Market.prototype.addOrder = function (newOrder) {
        var functionName = 'Market.addOrder()';
        //if trade state is not pending
        if (newOrder.state !== 'pending') {
            var error = new VError('%s can not add %s order with id %s and rate %s as its state must be "pending" and not "%s"', functionName, newOrder.side, newOrder.exchangeId, newOrder.price, newOrder.state);
            logger.error(error.stack);
            throw error;
        }
        // see if newOrder is already in the orders list matching on the exchangeId
        var storedOrder = _.findWhere(this.orders, {
            exchangeId: newOrder.exchangeId,
            state: 'pending' });
        // If trade is not already in the trades list then add
        if (_.isUndefined(storedOrder)) {
            logger.debug('%s trade is not already in market.trades list so will add', functionName);
            this.orders.push(newOrder);
            // emit a new event to the listeners
            this.emit('order', newOrder);
        }
        else {
            logger.debug('%s %s trade with id %s and rate %s already exists in market.trades. Will not add it again', functionName, newOrder.side, newOrder.exchangeId, newOrder.price);
        }
    };
    Market.prototype.cancelOrder = function (cancelledOrder, callback) {
        var functionName = 'Market.cancelOrder()', self = this;
        //if trade state is not cancelled
        if (cancelledOrder.state !== 'cancelled') {
            var error = new VError('%s can not cancel %s trade with id %s and rate %s as its state must be "cancelled" and not "%s"', functionName, cancelledOrder.side, cancelledOrder.exchangeId, cancelledOrder.price, cancelledOrder.state);
            logger.error(error.stack);
            throw error;
        }
        logger.trace('%s about to replace a pending order with a cancelled trade for id %s, tag %s in list of %s market trades', functionName, cancelledOrder.exchangeId, cancelledOrder.tag, cancelledOrder.state, this.orders.length);
        // remove old pending order from market trades
        this.orders = _.reject(this.orders, function (order) {
            return order.exchangeId === cancelledOrder.exchangeId &&
                order.state === 'pending';
        });
        //emit a new filled event to the listeners
        this.emit('order', cancelledOrder);
        if (callback)
            callback(cancelledOrder);
    };
    Market.prototype.fillOrder = function (filledOrder, callback) {
        var functionName = 'Market.fillOrder()', self = this;
        //if trade state is not cancelled
        if (filledOrder.state !== 'filled') {
            var error = new VError('%s can not cancel %s trade with id %s and rate %s as its state must be "filled" and not "%s"', functionName, filledOrder.side, filledOrder.exchangeId, filledOrder.price, filledOrder.state);
            logger.error(error.stack);
            throw error;
        }
        logger.trace('%s about to replace a pending order with a filled order for id %s, tag %s in market trades list', functionName, filledOrder.exchangeId, filledOrder.tag, filledOrder.state);
        // remove old pending order from market trades
        this.orders = _.reject(this.orders, function (order) {
            return order.exchangeId === filledOrder.exchangeId &&
                order.state === 'pending';
        });
        // add new filled order
        this.orders.push(filledOrder);
        //emit a new filled order event to the listeners
        this.emit('order', filledOrder);
        if (callback)
            callback(filledOrder);
    };
    Market.prototype.partialFillOrder = function (partiallyFilledOrder, callback) {
        var functionName = 'Market.partialFillOrder()', self = this;
        if (partiallyFilledOrder.state === 'filled') {
            this.fillOrder(partiallyFilledOrder, callback);
            return;
        }
        else if (partiallyFilledOrder.state !== 'partiallyFilled') {
            var error = new VError('%s can not partially fill %s trade with id %s and rate %s as its state must be "partiallyFilled" and not "%s"', functionName, partiallyFilledOrder.side, partiallyFilledOrder.exchangeId, partiallyFilledOrder.price, partiallyFilledOrder.state);
            logger.error(error.stack);
            throw error;
        }
        logger.trace('%s about to replace a pending order with a partially filled trade for id %s, tag %s in market trades list', functionName, partiallyFilledOrder.exchangeId, partiallyFilledOrder.tag, partiallyFilledOrder.state);
        // remove old pending order from market trades
        this.orders = _.reject(this.orders, function (order) {
            return order.exchangeId === partiallyFilledOrder.exchangeId &&
                order.state === 'pending';
        });
        // add new filled order
        this.orders.push(partiallyFilledOrder);
        // add remaining pending order to market trades list
        var remainingPartialOrder = partiallyFilledOrder.remainingPartial();
        // push the remaining pending order of the partially filled trade into the market maker trades
        self.orders.push(remainingPartialOrder);
        //emit a new filled event to the listeners
        this.emit('order', partiallyFilledOrder);
        if (callback)
            callback(partiallyFilledOrder);
    };
    return Market;
})(events_1.EventEmitter);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Market;
