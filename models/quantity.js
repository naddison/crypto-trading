/// <reference path="../typings/node/node-0.10.d.ts" />
var Quantity = (function () {
    function Quantity(quantity) {
        this.currency = quantity.currency;
        this.amount = quantity.amount;
    }
    return Quantity;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Quantity;
