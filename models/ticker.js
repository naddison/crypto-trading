/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
// load third party packages
var moment = require('moment');
var _ = require('underscore');
var BigNumber = require('bignumber.js');
var exchange_1 = require("./exchange");
var market_1 = require("./market");
var order_1 = require("./order");
var quantity_1 = require("./quantity");
var VError = require('verror'), logger = require('config-logger');
var colors = require('colors');
colors.setTheme({
    up: 'green',
    down: 'red',
    same: 'white'
});
var Ticker = (function () {
    function Ticker(settings) {
        // timestamp used when services send back a timestamp
        this.timestamp = new Date;
        // override the default Ticker settings
        _.extend(this, settings);
    }
    Object.defineProperty(Ticker.prototype, "bidBN", {
        get: function () { return new BigNumber(this.bid); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ticker.prototype, "askBN", {
        get: function () { return new BigNumber(this.ask); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ticker.prototype, "lastBN", {
        get: function () { return new BigNumber(this.last); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ticker.prototype, "exchange", {
        get: function () { return exchange_1.default.exchanges[this.exchangeName]; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ticker.prototype, "market", {
        get: function () { return this.exchange.markets[this.fixedCurrency + this.variableCurrency]; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ticker.prototype, "fixedCurrency", {
        get: function () { return market_1.default.getFixedCurrency(this.symbol); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ticker.prototype, "variableCurrency", {
        get: function () { return market_1.default.getVariableCurrency(this.symbol); },
        enumerable: true,
        configurable: true
    });
    /**
     * Sell quantity of currency taking into account exchange commissions
     */
    Ticker.prototype.sell = function (sellQuantity) {
        var functionName = 'Ticker.sell()';
        // initialise variables
        var price, amount;
        // if selling exchange's fixed currency. eg Sell BTC for a BTC/AUD price
        if (sellQuantity.currency === this.fixedCurrency) {
            // the bid price is used as we are selling the exchanges fixed currency. eg Selling BTC with a BTC/AUD exchange rate
            price = this.bid;
            amount = _.clone(sellQuantity.amount);
        }
        else if (sellQuantity.currency === this.variableCurrency) {
            // the ask price is used as we are buying the exchange's fixed currency. eg Buying BTC with a BTC/AUD exchange rate
            price = this.ask;
            amount = new BigNumber(sellQuantity.amount).
                div(this.ask).
                toFixed(this.exchange.currencyRounding[this.fixedCurrency]);
            logger.debug('%s trade amount = sell quantity amount %s / ask %s = %s', functionName, sellQuantity.amount, this.ask, amount);
        }
        else {
            var error = new VError('%s could not convert quantity as currency "%s" not defined for this ticker', functionName, sellQuantity.currency);
            logger.error(error.message);
            throw error;
        }
        // return new trade object
        return new order_1.default({
            exchangeName: this.exchangeName,
            symbol: this.fixedCurrency.concat(this.variableCurrency),
            state: 'pending',
            side: 'sell',
            amount: amount,
            price: price.toString()
        });
    };
    /**
     * Buy quantity of currency taking into account exchange commissions
     * this is much harder than the sell function as we need to
     * estimate the sell amount and then tune to allow for rounding errors
     * the actual buy amount of the trade will be the closest it can be due to rounding errors.
     * The buy amount may not be exactly equal to the requested buy quantity
     * @param {Quantity} buyQuantity Currency and requested buy amount for this ticker
     * @return {Order} trade quote for this ticker
     */
    Ticker.prototype.buy = function (buyQuantity) {
        // initialise variables
        var sellQuantity = new quantity_1.default({ currency: '', amount: 0 });
        var conversionRate;
        // if buying exchange's fixed currency. eg Buy BTC for a BTC/AUD exchange rate
        if (buyQuantity.currency === this.fixedCurrency) {
            //Sell currency must be the varaible currency. eg AUD for a BTC/AUD exchange rate
            sellQuantity.currency = this.variableCurrency;
            // the ask price is used as we are buying the exchange's fixed currency. eg Buying BTC with a BTC/AUD exchange rate
            conversionRate = 1 / this.ask;
        }
        else if (buyQuantity.currency === this.variableCurrency) {
            //Sell currency must be the variable currency. eg AUD for a BTC/AUD exchange rate
            sellQuantity.currency = this.fixedCurrency;
            // the bid price is used as we are selling the exchange's fixed currency. eg Selling BTC with a BTC/AUD exchange rate
            conversionRate = 1 / this.bid;
        }
        else {
            var errMsg = 'Error: Ticker.buy() could not convert quantity as currency "' + buyQuantity.currency + '" not defined for this ticker';
            logger.error(errMsg);
            throw new Error(errMsg);
        }
        // Sell amount = known buy amount / (conversion rate * (1 - commission rate))
        // the ask price is used as we are buying the exchange's fixed currency. eg Buying BTC with a BTC/AUD exchange rate
        sellQuantity.amount = buyQuantity.amount * conversionRate / (1 - this.exchange.commissions[buyQuantity.currency].taker);
        // round the sell amount to the sell currency precision
        sellQuantity.amount = Number(sellQuantity.amount.toFixed(this.exchange.currencyRounding[sellQuantity.currency]));
        // TODO recursively adjust the sell amount until we get as close as we can to the specified buy amount
        // test the estimated sell amount will give the correct buy amount
        var trade = this.sell(sellQuantity);
        var roundingError = Number(trade.buy.amount) - buyQuantity.amount;
        roundingError = Number(roundingError.toFixed(this.exchange.currencyRounding[buyQuantity.currency]));
        // eg 2 decimal places will have an increment of 1 / 10 ^ 2 = 1 / 100 = 0.01
        var buyCurrencyIncrement = 1 / 10 ^ this.exchange.currencyRounding[buyQuantity.currency];
        if (roundingError > 0) {
            // adjust the testSellQuantity down and retest
            sellQuantity.amount -= buyCurrencyIncrement;
        }
        else if (roundingError < 0) {
            // adjust the testSellQuantity up and retest
            sellQuantity.amount += buyCurrencyIncrement;
        }
        return trade;
    };
    /**
     * @return {Number} Spread between ask and bid tickers
     */
    Ticker.prototype.spread = function () {
        // number of decimal places the fixed currency is stored in. eg 4 decimal places for BTC on CoinJar
        var fixedCurrencyPrecision = this.exchange.currencyRounding[this.fixedCurrency];
        // Calculate the spread = ask - bid
        var spread = new BigNumber(this.ask).
            minus(this.bid).
            toNumber();
        return spread;
    };
    /**
     * @return {Number} Spread between ask and bid tickers as a percentage of the bid price
     */
    Ticker.prototype.spreadPercentage = function () {
        var functionName = 'Ticker.spreadPercentage()';
        if (this.bid > 0) {
            // (ask - bid) / bid * 100
            var spreadPercentage = new BigNumber(this.ask).
                minus(this.bid).
                div(this.bid).
                times(100).
                round(2).
                toNumber();
            return spreadPercentage;
        }
        else {
            return 0;
        }
    };
    // Note the following are static functions hence are not declared on the prototype property
    /**
     * Generic static listener for new tickers
     */
    Ticker.tickerListener = function (market, callback) {
        market.on('price', function (ticker) {
            logger.debug('Ticker event fired with bid %d and ask %d tickers for the %s market on the %s exchange', ticker.bid, ticker.ask, market.symbol, market.exchangeName);
            // call callback if exists and is a function
            if (callback)
                callback(ticker);
        });
    };
    Ticker.startListeners = function (callback, exchangeNames, symbols) {
        var functionName = 'Ticker.startListeners()', self = this;
        // for each exchange
        exchangeNames.forEach(function (exchangeName) {
            var exchange = exchange_1.default.exchanges[exchangeName];
            // default to symbols passed in as an argument
            var exchangeSymbols = symbols;
            // if no symbols passed in argument then start listeners for all markets on the exchange
            if (!symbols) {
                exchangeSymbols = _.keys(exchange.markets);
            }
            // for each market
            exchangeSymbols.forEach(function (symbol) {
                var market = exchange.markets[symbol];
                self.tickerListener(market, callback);
            });
        });
    };
    /**
     * Static function to log the latest ticker
     */
    Ticker.log = function (ticker) {
        // get rounding from the exchange configuration
        var rounding = exchange_1.default.exchanges[ticker.exchangeName].currencyRounding[ticker.fixedCurrency];
        var market = ticker.exchangeName + '[' + ticker.symbol + '] ', previousTicker = ticker.market.previousTicker;
        if (!previousTicker) {
            return console.log('%s bid %s, %s ask %s, last %s', market, ticker.bid, ticker.ask, ticker.last);
        }
        // TODO change to use BigNumber
        var bidChange = Number((ticker.bid - previousTicker.bid).toFixed(rounding)), askChange = Number((ticker.ask - previousTicker.ask).toFixed(rounding)), lastChange = Number((ticker.last - previousTicker.last).toFixed(rounding)), bidChangePerc = (bidChange / previousTicker.bid * 100).toFixed(3), askChangePerc = (askChange / previousTicker.ask * 100).toFixed(3), lastChangePerc = (lastChange / previousTicker.ask * 100).toFixed(3), bid = ticker.bid + ' ' + bidChangePerc + '% ', ask = ticker.ask + ' ' + askChangePerc + '% ', last = ticker.last + ' ' + lastChangePerc + '% ', spread = new BigNumber(ticker.ask).minus(ticker.bid).div(ticker.bid).times(100).toFixed(3) + '% ';
        console.log(market +
            bid +
            ask +
            last +
            spread +
            moment(ticker.timestamp).format('hh:mm:ss'));
    };
    return Ticker;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Ticker;
