
export interface ICurrency
{
    currency: string;
}

export interface ITradeAmounts extends ICurrency
{
    amount: string;
    amountRemaining: string;
    amountTraded: string;
    amountLastPartial: string;
}

export interface IGrossTradeAmounts extends ITradeAmounts
{
    amountGross: string;
    amountRemainingGross: string;
    amountTradedGross: string;
    amountLastPartialGross: string;
}

export interface IFixVarTradeAmounts extends ITradeAmounts
{
    fixedAmount: string;
    fixedAmountRemaining: string;
    fixedAmountTraded: string;
    fixedAmountLastPartial: string;

    variableAmount: string;
    variableAmountRemaining: string;
    variableAmountTraded: string;
    variableAmountLastPartial: string;
}
