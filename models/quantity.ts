/// <reference path="../typings/node/node-0.10.d.ts" />

export interface IQuantity {
	currency: string;
	amount: number;
}

export default class Quantity implements IQuantity
{
	currency: string;
	amount: number;

	constructor(quantity: IQuantity) {
		this.currency = quantity.currency;
		this.amount = quantity.amount;
	}
}