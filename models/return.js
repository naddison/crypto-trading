/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
var _ = require('underscore');
var util_1 = require("util");
var BigNumber = require('bignumber.js');
// My packages
var logger = require('config-logger'), VError = require('verror');
var Return = (function () {
    function Return(settings) {
        this.sellAmountBN = BigNumber(0);
        this.buyAmountBN = BigNumber(0);
        this.amountRounding = 8;
        this.decimalRounding = 6;
        this.percentageRounding = 2;
        this.sell2buyExchangeRateBN = BigNumber(1);
        // override the default settings with what's passed to the constructor
        _.extend(this, settings);
    }
    Object.defineProperty(Return.prototype, "sellAmount", {
        get: function () { return this.sellAmountBN.toString(); },
        set: function (sellAmount) { this.sellAmountBN = new BigNumber(sellAmount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Return.prototype, "buyAmount", {
        get: function () { return this.buyAmountBN.toString(); },
        set: function (buyAmount) { this.buyAmountBN = new BigNumber(buyAmount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Return.prototype, "sell2buyExchangeRate", {
        get: function () { return this.sell2buyExchangeRateBN.toString(); },
        set: function (sell2buyExchangeRate) { this.sell2buyExchangeRateBN = new BigNumber(sell2buyExchangeRate); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Return.prototype, "clone", {
        get: function () {
            return new Return({
                sellCurrency: _.clone(this.sellCurrency),
                sellAmount: _.clone(this.sellAmount),
                buyCurrency: _.clone(this.buyCurrency),
                buyAmount: _.clone(this.buyAmount),
                amountRounding: _.clone(this.amountRounding),
                decimalRounding: _.clone(this.decimalRounding),
                percentageRounding: _.clone(this.percentageRounding),
                sell2buyExchangeRate: _.clone(this.sell2buyExchangeRate)
            });
        },
        enumerable: true,
        configurable: true
    });
    Return.prototype.log = function () {
        var msg = util_1.format('buy %s %s, sell %s %s, sell to buy exchange rate %s', this.buyAmount, this.buyCurrency, this.sellAmount, this.sellCurrency, this.sell2buyExchangeRate);
        return msg;
    };
    Return.prototype.amountBN = function (currency) {
        var functionName = 'Return.amountBN()';
        var returnAmountBN;
        if (!currency && this.sellCurrency === this.buyCurrency) {
            // default to sell currency which is the same as the buy currency
            currency = this.sellCurrency;
        }
        else if (!currency || !(currency === this.buyCurrency || currency === this.sellCurrency)) {
            var error = VError('%s first parameter currency %s must either be the "buy" currency %s or "sell" currency %s', functionName, currency, this.buyCurrency, this.sellCurrency);
            logger.error(error.stack);
            return error;
        }
        if (currency === this.buyCurrency) {
            returnAmountBN = this.sellAmountBN.
                times(this.sell2buyExchangeRate).
                minus(this.buyAmount).
                round(this.amountRounding);
            logger.trace('%s return amount %s %s = sell amount %s * sell to buy currency exchange rate %s - buy amount %s', functionName, returnAmountBN.toString(), this.buyCurrency, this.sellAmount, this.sell2buyExchangeRate, this.buyAmount);
        }
        else if (currency === this.sellCurrency) {
            returnAmountBN = this.sellAmountBN.
                minus(this.buyAmountBN.
                div(this.sell2buyExchangeRate)).
                round(this.amountRounding);
            logger.trace('%s return amount %s %s = sell amount %s - buy amount %s / sell to buy currency exchange rate %s', functionName, returnAmountBN.toString(), this.sellCurrency, this.sellAmount, this.buyAmount, this.sell2buyExchangeRate);
        }
        return returnAmountBN;
    };
    Return.prototype.decimalBN = function (currency) {
        var functionName = 'Return.decimalBN()';
        var returnDecimalBN;
        if (!currency && this.sellCurrency === this.buyCurrency) {
            // default to sell currency which is the same as the buy currency
            currency = this.sellCurrency;
        }
        else if (!currency || !(currency === this.buyCurrency || currency === this.sellCurrency)) {
            var error = VError('%s first parameter currency %s must either be the buy currency "%s" or sell currency "%s"', functionName, currency, this.buyCurrency, this.sellCurrency);
            logger.error(error.stack);
            return error;
        }
        if (currency === this.buyCurrency) {
            returnDecimalBN = this.sellAmountBN.
                times(this.sell2buyExchangeRate).
                minus(this.buyAmount).
                div(this.buyAmount).
                round(this.decimalRounding);
            logger.trace('%s return amount %s %s = (sell amount %s * sell to buy currency exchange rate %s - buy amount %s) / buy amount %s', functionName, returnDecimalBN.toString(), this.buyCurrency, this.sellAmount, this.sell2buyExchangeRate, this.buyAmount, this.buyAmount);
        }
        else if (currency === this.sellCurrency) {
            var buyAmountInSellCurrencyBN = this.buyAmountBN.
                div(this.sell2buyExchangeRate);
            logger.trace('%s buy amount in sell currency %s %s = buy amount %s %s / sell to buy exchange rate %s', functionName, buyAmountInSellCurrencyBN.toString(), this.sellCurrency, this.buyAmount, this.buyCurrency, this.sell2buyExchangeRate);
            returnDecimalBN = this.sellAmountBN.
                minus(buyAmountInSellCurrencyBN).
                div(buyAmountInSellCurrencyBN).
                round(this.decimalRounding);
            logger.trace('%s return amount %s %s = (sell amount in variable currency %s - buy amount in variable currency %s / buy amount in variable currency %s', functionName, returnDecimalBN.toString(), this.sellCurrency, this.sellAmount, buyAmountInSellCurrencyBN.toString(), buyAmountInSellCurrencyBN.toString());
        }
        return returnDecimalBN;
    };
    Return.prototype.percentageBN = function (currency) {
        var functionName = 'Return.percentageBN()';
        if (!currency && this.sellCurrency === this.buyCurrency) {
            // default to sell currency which is the same as the buy currency
            currency = this.sellCurrency;
        }
        else if (!currency || !(currency === this.buyCurrency || currency === this.sellCurrency)) {
            var error = VError('%s first parameter currency %s must either be the "buy" currency %s or "sell" currency %s', functionName, currency, this.buyCurrency, this.sellCurrency);
            logger.error(error.stack);
            return error;
        }
        var decimalBN = this.decimalBN(currency);
        return decimalBN.
            times(100).
            round(Number(this.percentageRounding));
    };
    return Return;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Return;
