/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
var BigNumber = require('bignumber.js');
var exchange_1 = require("../exchange");
// My packages
var logger = require('config-logger');
/**
 * Used by OrderBook.getVwap()
 */
var BuyAmountCalculator = (function () {
    function BuyAmountCalculator(market, commissionType) {
        this.market = market;
        this.exchange = exchange_1.default.exchanges[market.exchangeName];
        this.marketCommissions = market.getCommissionMatrix(commissionType);
    }
    /**
     * calculate the buy currency amount of an order in an order book.
     * @param order object with rate and amount properties of type number
     * @param side  asks or bids
     */
    BuyAmountCalculator.prototype.calcAmountBN = function (order, side) {
        var functionName = 'BuyAmountCalculator.calcAmountBN()';
        var buyAmountsBN;
        if (side === 'asks') {
            // asks is for a market buy trade so the buy currency is fixed
            buyAmountsBN = new BigNumber(order.amount).
                times(this.marketCommissions.buy[this.market.fixedCurrency].
                plus(1)).
                round(this.exchange.currencyRounding[this.market.fixedCurrency], BigNumber.ROUND_DOWN);
            logger.debug('%s buy amount on the asks side %s = order amount %s * (fixed buy commission %s + 1)', functionName, buyAmountsBN.toString(), order.amount, this.marketCommissions.buy[this.market.fixedCurrency].toString());
        }
        else if (side === 'bids') {
            // bids is a market sell trade so the buy currency is variable
            buyAmountsBN = new BigNumber(order.price).
                times(order.amount).
                times(this.marketCommissions.buy[this.market.variableCurrency].
                plus(1)).
                round(this.exchange.currencyRounding[this.market.variableCurrency], BigNumber.ROUND_DOWN);
            logger.debug('%s buy amount on the bids side %s = order price %s * order amount %s * (variable buy commission %s + 1)', functionName, buyAmountsBN.toString(), order.price, order.amount, this.marketCommissions.buy[this.market.variableCurrency].toString());
        }
        return buyAmountsBN;
    };
    return BuyAmountCalculator;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BuyAmountCalculator;
