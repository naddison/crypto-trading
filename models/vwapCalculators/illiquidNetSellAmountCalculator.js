/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../../typings/underscore/underscore.d.ts" />
var BigNumber = require('bignumber.js');
var order_1 = require("../order");
// My packages
var logger = require('config-logger'), VError = require('verror');
var BuyAmountCalculator = (function () {
    function BuyAmountCalculator(marginBN, liquid2IlliquidRateBN) {
        this.marginBN = marginBN;
        this.liquid2IlliquidRateBN = new BigNumber(1);
        if (liquid2IlliquidRateBN) {
            this.liquid2IlliquidRateBN = liquid2IlliquidRateBN;
        }
    }
    /**
     * static function to calculate the sell amount in getVwapFromCustomAmount
     * @param order an array with two items: rate and amount
     * @param side  asks or bids
     * @param orderBook  instance of the orderBook calling the calcAmountBN function
     */
    BuyAmountCalculator.prototype.calcAmountBN = function (order, side, orderBook) {
        var functionName = 'IlliquidNetSellAmountCalculator.calcAmountBN()';
        if (!(side === 'asks' || side === 'bids')) {
            var error = new VError('%s second parameter side %s must be either "asks" or "bids"', functionName, side);
            logger.error(error.stack);
            return error;
        }
        var liquidSide = (side === 'asks' ? 'buy' : 'sell'), illiquidSide = (side === 'asks' ? 'sell' : 'buy');
        // calculate net sell amount of liquid order
        var liquidOrder = new order_1.default({
            exchangeName: orderBook.exchangeName,
            symbol: orderBook.symbol,
            side: liquidSide,
            type: 'market',
            price: order.price.toString(),
            amount: order.amount.toString()
        });
        // illiquid sell amount is variable currency
        if (illiquidSide === 'buy') {
            // illiquid net variable amount = liquid variable amount * (1 + marginBN)
            var illiquidNetSellAmountBN = liquidOrder.variableAmountBN.
                times(this.liquid2IlliquidRateBN).
                times(this.marginBN.
                plus(1));
            logger.trace('%s illiquid net sell amount %s = liquid net variable amount * liquid to illiquid exchange rate %s * (1 + margin %s)', functionName, illiquidNetSellAmountBN.toString(), liquidOrder.variableAmountBN.toString(), this.liquid2IlliquidRateBN.toString(), this.marginBN.toString());
            // return as sell amount of illiquid order
            return illiquidNetSellAmountBN;
        }
        else if (illiquidSide === 'sell') {
            logger.debug('%s illiquid net sell amount %s = liquid net fixed amount', functionName, liquidOrder.fixedAmountBN.toString());
            // illiquid net fixed amount = liquid net fixed amount
            return liquidOrder.fixedAmountBN;
        }
    };
    return BuyAmountCalculator;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BuyAmountCalculator;
