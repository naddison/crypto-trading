/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
var BigNumber = require('bignumber.js');
// My packages
var logger = require('config-logger');
var TradeAmountCalculator = (function () {
    function TradeAmountCalculator() {
    }
    TradeAmountCalculator.prototype.calcAmountBN = function (order) {
        var functionName = 'TradeAmountCalculator.calcAmountBN()';
        logger.trace('%s trade amount = %s', functionName, order.amount);
        return new BigNumber(order.amount);
    };
    return TradeAmountCalculator;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TradeAmountCalculator;
