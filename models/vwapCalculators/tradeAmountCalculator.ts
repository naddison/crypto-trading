/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />

import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

import {IAmountCalculator, IOrderBookOrder} from "../orderBook";

// My packages
var logger = require('config-logger');

export default class TradeAmountCalculator implements IAmountCalculator
{
    calcAmountBN(order: IOrderBookOrder): BigNumber
    {
        var functionName = 'TradeAmountCalculator.calcAmountBN()';

        logger.trace('%s trade amount = %s', functionName,
            order.amount);

        return new BigNumber(order.amount);
    }
}
